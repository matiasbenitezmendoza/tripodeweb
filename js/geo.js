var geo = (function(){
 
    var firebaseRef;
    var geoFire;
    var geoQuery;

    var _init = function(){
        firebase.initializeApp({
               apiKey: "AIzaSyC5IcRccDo289TTRa3Y7qJIu8YPz3EnKAI",
               databaseURL: "https://geofire-9d0de.firebaseio.com"
        });
        firebaseRef = firebase.database().ref().push();
        geoFire = new GeoFire(firebaseRef);
    };
    
    var getLocation = function() {
        if (typeof navigator !== "undefined" && typeof navigator.geolocation !== "undefined") {
          log("Asking user to get their location");
          navigator.geolocation.getCurrentPosition(geolocationCallback, errorHandler);
        } else {
          log("Your browser does not support the HTML5 Geolocation API, so this demo will not work.")
        }
    };
    
    var geolocationCallback = function(location) {
        var latitude = location.coords.latitude;
        var longitude = location.coords.longitude;
        log("Retrieved user's location: [" + latitude + ", " + longitude + "]");
        var username = "wesley";
        geoFire.set(username, [latitude, longitude]).then(function() {
          log("Current user " + username + "'s location has been added to GeoFire");
          firebaseRef.child(username).onDisconnect().remove();
          log("Added handler to remove user " + username + " from GeoFire when you leave this page.");
        }).catch(function(error) {
          log("Error adding user " + username + "'s location to GeoFire");
        });
    };
    
    var errorHandler = function(error) {
        if (error.code == 1) {
          log("Error: PERMISSION_DENIED: User denied access to their location");
        } else if (error.code === 2) {
          log("Error: POSITION_UNAVAILABLE: Network is down or positioning satellites cannot be reached");
        } else if (error.code === 3) {
          log("Error: TIMEOUT: Calculating the user's location too took long");
        } else {
          log("Unexpected error code")
        }
    };
    
    var log = function (message) {
        var childDiv = document.createElement("div");
        var textNode = document.createTextNode(message);
        childDiv.appendChild(textNode);
        document.getElementById("log").appendChild(childDiv);
    };

    var accion = function() {
        var lat = 19.3936176;
        var lon = -99.1803593;
        var myID = "repartidor-" + firebaseRef.push().key;
        geoFire.set(myID, [lat, lon]).then(function() {
             log(myID + ": Repartidor en posicion [" + lat + "," + lon + "]");
        });
        return false;
    };

    var show = function() {
        var lat = 19.3929902;
        var lon = -99.1795868;
        var radius = 100;
        var operation;

        if (typeof geoQuery !== "undefined") {
            operation = "Updating";
        
            geoQuery.updateCriteria({
                  center: [lat, lon],
                  radius: radius
            });

        } else {
            operation = "Creating";
            geoQuery = geoFire.query({
                  center: [lat, lon],
                  radius: radius
            });
    
            geoQuery.on("key_entered", function(key, location, distance) {
                         log(key + " is located at [" + location + "] which is within the query (" + distance.toFixed(2) + " km from center)");
            });

            geoQuery.on("key_exited", function(key, location, distance) {
                     console.log(key, location, distance);
                     log(key + " is located at [" + location + "] which is no longer within the query (" + distance.toFixed(2) + " km from center)");
            });
        }

        log(operation + " the query: centered at [" + lat + "," + lon + "] with radius of " + radius + "km"); 
        return false;
    };

    return{
           "init" : _init,
           "getLocation": getLocation,
           "add_user": accion,
           "show": show
    };
            
})();