// SOFT SCROLL
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

  // MENU MOVIL
  if( window.innerWidth <= 850 ){
    $(".cont_burger").click(function(){
    	$(".burger").toggleClass('active');
    	$("#mainNav").slideToggle();
    })
    $("#mainNav a").click(function(){
      $(".burger").toggleClass('active');
    	$("#mainNav").slideToggle();
    });
  }


// HOME
  var swiper = new Swiper('.swiper-container1', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });


$('[data-fancybox]' ).fancybox({
  media : {
        youtube : {
            params : {
                autoplay : 1
            }
        }
    },
  buttons : [
      'slideShow',
      'fullScreen',
      'close'
  ]
});

  $('[data-fancybox="imgs-gpAbuDhabi"], [data-fancybox="imgs-gpBrasil"], [data-fancybox="imgs-gpMexico"], [data-fancybox="imgs-gpEua"], [data-fancybox="imgs-gpJapon"], [data-fancybox="imgs-gpMalasia"], [data-fancybox="imgs-gpMalasia"]' ).fancybox({
  buttons : [
        'slideShow',
        'fullScreen',
        'close'
    ],
    thumbs : {
		autoStart : true,
    axis        : 'y'
	}
});

$('[data-type="iframe"]').fancybox({
buttons : [
      'fullScreen',
      'close'
  ],
  iframe : {
    tpl : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
		css : {
        width: '60%'
		}
	}
});
