<?php
namespace MercadoPago;
use MercadoPago\Annotation\RestMethod;
use MercadoPago\Annotation\RequestParam;
use MercadoPago\Annotation\Attribute;

/**
 * @RestMethod(resource="/v1/payments", method="create")
 * @RestMethod(resource="/v1/payments/:id", method="read")
 * @RestMethod(resource="/v1/payments/search", method="search")
 * @RestMethod(resource="/v1/payments/:id", method="update")
 * @RestMethod(resource="/v1/payments/:id/refunds", method="refund")
 * @RequestParam(param="access_token")
 */
class Payment extends Entity
{
    /**
     * @Attribute(primaryKey = true)
     */
    protected $id;
    /**
     * @Attribute()
     */
    protected $acquirer;
    /**
     * @Attribute()
     */
    protected $acquirer_reconciliation;
    /**
     * @Attribute(idempotency = true)
     */
    protected $site_id;
    /**
     * @Attribute()
     */
    protected $sponsor_id;
    /**
     * @Attribute()
     */
    protected $operation_type;
    /**
     * @Attribute(idempotency = true)
     */
    protected $order_id;
    /**
     * @Attribute()
     */
    protected $order;
    /**
     * @Attribute()
     */
    protected $binary_mode;
    /**
     * @Attribute()
     */
    protected $external_reference;
    /**
     * @Attribute()
     */
    protected $status;
    /**
     * @Attribute()
     */
    protected $status_detail;
    /**
     * @Attribute()
     */
    protected $store_id;
    /**
     * @Attribute()
     */
    protected $taxes_amount;
    /**
     * @Attribute(type = "string")
     */
    protected $payment_type;
    /**
     * @Attribute()
     */
    protected $date_created;
    /**
     * @Attribute()
     */
    protected $last_modified;
    /**
     * @Attribute()
     */
    protected $live_mode;
    /**
     * @Attribute()
     */
    protected $date_last_updated;
    /**
     * @Attribute()
     */
    protected $date_of_expiration;
    /**
     * @Attribute()
     */
    protected $deduction_schema;
    /**
     * @Attribute()
     */
    protected $date_approved;
    /**
     * @Attribute()
     */
    protected $money_release_date;
    /**
     * @Attribute()
     */
    protected $money_release_schema;
    /**
     * @Attribute()
     */
    protected $currency_id;
    /**
     * @Attribute(type = "float")
     */
    protected $transaction_amount;
    /**
     * @Attribute(type = "float")
     */
    protected $transaction_amount_refunded;
    /**
     * @Attribute()
     */
    protected $shipping_cost;
    /**
     * @Attribute(idempotency = true)
     */
    protected $total_paid_amount;
    /**
     * @Attribute(type = "float")
     */
    protected $finance_charge;
    /**
     * @Attribute()
     */
    protected $net_received_amount;
    /**
     * @Attribute()
     */
    protected $marketplace;
    /**
     * @Attribute(type = "float")
     */
    protected $marketplace_fee;
    /**
     * @Attribute()
     */
    protected $reason;
    /**
     * @Attribute()
     */
    protected $payer;
    /**
     * @Attribute()
     */
    protected $collector;
    /**
     * @Attribute()
     */
    protected $collector_id;
    /**
     * @Attribute()
     */
    protected $counter_currency;
    /**
     * @Attribute()
     */
    protected $payment_method_id;
    // For flavor 1
    /**
     * @Attribute()
     */
    protected $payment_type_id;
    /**
     * @Attribute()
     */
    protected $pos_id;
    /**
     * @Attribute()
     */
    protected $transaction_details;
    /**
     * @Attribute()
     */
    protected $fee_details;
    /**
     * @Attribute()
     */
    protected $differential_pricing_id;
    /**
     * @Attribute()
     */
    protected $application_fee;
    /**
     * @Attribute()
     */
    protected $authorization_code;
    /**
     * @Attribute()
     */
    protected $capture;
    /**
     * @Attribute()
     */
    protected $captured;
    /**
     * @Attribute()
     */
    protected $card;
    /**
     * @Attribute()
     */
    protected $call_for_authorize_id;
    /**
     * @Attribute()
     */
    protected $statement_descriptor;
    /**
     * @Attribute()
     */
    protected $refunds;
    /**
     * @Attribute()
     */
    protected $shipping_amount;
    /**
     * @Attribute()
     */
    protected $additional_info;
    /**
     * @Attribute()
     */
    protected $campaign_id;
    /**
     * @Attribute()
     */
    protected $coupon_amount;
    /**
     * @Attribute(type = "int")
     */
    protected $installments;
    /**
     * @Attribute()
     */
    protected $token;
    /**
     * @Attribute()
     */
    protected $description;
    /**
     * @Attribute()
     */
    protected $notification_url;
    /**
     * @Attribute()
     */
    protected $issuer_id;
    /**
     * @Attribute()
     */
    protected $processing_mode;
    /**
     * @Attribute()
     */
    protected $merchant_account_id;
    /**
     * @Attribute()
     */
    protected $merchant_number;
    /**
     * @Attribute()
     */
    protected $metadata;
    /**
     * @Attribute()
     */
    protected $callback_url;

    public function toJson(){
      $response = array(
        "id" => $this->id,
        "date_created" => $this->date_created,
        "date_approved" => $this->date_approved,
        "date_last_updated" => $this->date_last_updated,
        "date_of_expiration" => $this->date_of_expiration,
        "money_release_date" => $this->money_release_date,
        "operation_type" => $this->operation_type,
        "operation_type" => $this->operation_type,
        "issuer_id" => $this->issuer_id,
        "payment_method_id" => $this->payment_method_id,
        "payment_type_id" => $this->payment_type_id,
        "status" => $this->status,
        "status_detail" => $this->status_detail,
        "currency_id" => $this->currency_id,
        "description" => $this->description,
        "live_mode" => $this->live_mode,
        "sponsor_id" => $this->sponsor_id,
        "authorization_code" => $this->authorization_code,
        "money_release_schema" => $this->money_release_schema,
        "counter_currency" => $this->counter_currency,
        "collector_id" => $this->collector_id,
        "payer" => $this->payer,
        "metadata" => $this->metadata,
        "additional_info" => $this->additional_info,
        "order" => $this->order,
        "external_reference" => $this->external_reference,
        "transaction_amount" => $this->transaction_amount,
        "transaction_amount_refunded" => $this->transaction_amount_refunded,
        "coupon_amount" => $this->coupon_amount,
        "differential_pricing_id" => $this->differential_pricing_id,
        "deduction_schema" => $this->deduction_schema,
        "transaction_details" => $this->transaction_details,
        "fee_details" => $this->fee_details,
        "captured" => $this->captured,
        "binary_mode" => $this->binary_mode,
        "call_for_authorize_id" => $this->call_for_authorize_id,
        "statement_descriptor" => $this->statement_descriptor,
        "installments" => $this->installments,
        "card" => $this->card,
        "notification_url" => $this->notification_url,
        "refunds" => $this->refunds,
        "processing_mode" => $this->processing_mode,
        "merchant_account_id" => $this->merchant_account_id,
        "acquirer" => $this->acquirer,
        "merchant_number" => $this->merchant_number,
        "acquirer_reconciliation" => $this->acquirer_reconciliation
      );
      return json_encode($response);
    }
}
