<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- Favicon icon -->
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>Tripode</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url("admin/css/lib/bootstrap/bootstrap.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("admin/css/lib/sweetalert/sweetalert.css") ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("admin/css/helper.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("admin/css/style.css") ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.1/css/bootstrap-slider.css">
    <link href="https://fonts.googleapis.com/css?family=Sedgwick+Ave" rel="stylesheet">
    <link href="<?php echo base_url("css/app.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("css/offcanvas.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("css/loading.css") ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url("admin/js/lib/jquery/jquery.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/bootstrap/js/popper.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/bootstrap/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/sweetalert/sweetalert.min.js") ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.1/bootstrap-slider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://secure.mlstatic.com/org-img/checkout/custom/1.0/checkout.js"></script>
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.0.1/dist/sweetalert2.all.min.js"></script>
    <script src="<?php echo base_url("js/jquery.mask.js") ?>"></script>
    <script src="<?php echo base_url("js/flyto.js") ?>"></script>
    <script src="<?php echo base_url("js/loading.js") ?>"></script>
    <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
    </script>
    <style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body,html {
			margin: 0px;
			padding: 0px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: white;
			background-color: transparent;
			font-weight: normal;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: auto;
			width: 90%;
      padding: 4% 0;
		}
		</style>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark navbar_site fixed-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url("store") ?>">Tienda en Linea</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="#">Nuestra App</a>
            </li>
        </ul>
    </div>
    <div class="mx-auto order-0">
        <a class="nav-link " href="<?php echo site_url("/") ?>"><img class="mx-auto" src="<?php echo base_url("imgs/tiny_logo.png"); ?>" /></a>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link " href="#">Sobre Nosotros</a>
            </li>
            <li class="nav-item dropdown">
                <?php if($this->session->userdata('code') != "") {?>
                <a class="nav-link button_purple dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $user_data['first_name']; ?> <i class="fas fa-cog"></i></a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="<?php echo site_url("inicio/user_info") ?>">Perfil</a>
                  <a class="dropdown-item" href="<?php echo site_url("inicio/log_out") ?>">Logout</a>
                </div>
              <?php }else{?>
                <?php
                $loginUrl = $this->facebook->getLoginUrl();
                ?>
                <a class="button_purple" href="<?php echo $loginUrl ?>"><i class="fab fa-facebook-f"></i> Acceder</a>
              <?php }?>
            </li>
            <li class="nav-item cart">
              <?php if($this->session->userdata('code') != "") {?>
              <a class="nav-link" href="<?php echo site_url("store/checkout") ?>"><i class="fas fa-cart-plus"></i><span class="badge"><?php if ($this->shopcart->total_item() > 0) {echo $this->shopcart->total_item();  }  ?></span></a>
            <?php }else{?>
              <?php
              $loginUrl = $this->facebook->getLoginUrl();
              ?>
              <a class="nav-link" href="<?php echo $loginUrl ?>"><i class="fas fa-cart-plus"></i><span class="badge"><?php if ($this->shopcart->total_item() > 0) {echo $this->shopcart->total_item();  }  ?></span></a>
            <?php }?>
            </li>
        </ul>
    </div>
</nav>

<?php echo $content ?>

<div class="footer_temp">
  <a href="<?php echo site_url("inicio/aviso") ?>">Aviso de privacidad</a>
	&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
	<a href="<?php echo site_url("inicio/terminos") ?>">Terminos y condiciones</a>
  <p>BigFan <?php  echo date("Y"); ?></p>
</div>

</body>
</html>
