<div id="container">
	<img src="<?php echo base_url("imgs/logo.jpg"); ?>" width="100px" />
	<div id="cont-terminos" class="cont-lightbox">
  <h2>Términos y condiciones</h2>
  <h4>Políticas de Compra.</h4>
  <p>
    Únicamente podrás comprar los productos que se encuentre disponibles para su venta y cuyo detalles aparezcan en la aplicación.
    <br>
    Todos nuestros precios están en moneda nacional y ya incluyen impuestos.
    <br>
    Una vez que haya seleccionado el producto que desee adquirir, deberás elegir la cantidad que deseas, si estás seguro de tu compra, selecciona la opción pagar, donde deberás elegir la forma de pago y dirección de envió. (Revisa políticas de pago más abajo).
    <br>
    Revisa que todos los datos de la dirección de envió estén correctos y completos, ya que de esto depende el éxito de tu entrega.
    <br>
    Puede haber algunos productos virtuales, nuestro sistema no permite realizar modificación de la compra, por lo que pedimos revisen cuidadosamente su orden.
    <br>
    Una vez acreditado el pago se prepará el envio correspondiente.
    <br>
    Para cualquier duda o aclaración de tu compra escríbenos a: <a href="mailto:contacto@bigfanofficial.com.mx">contacto@bigfanofficial.com.mx</a>
    <br>
    En caso de contar con cupones de descuento deberán ser ingresados al momento de su compra y solamente un cupón descuento podrá ser aplicado por compra.
  </p>
  <h4>Políticas de Envío.</h4>
  <p>
    Big Fan no es responsable si usted puso la dirección incorrecta y el producto no llego al destino por causas externas a Big Fan, nosotros haremos siempre lo mejor por brindarte el mejor servicio.
  </p>
  <h4>Políticas de Pago.</h4>
  <p>
    Pago con Tarjeta de Debito/ Tarjeta de crédito.
    <br>
    Una vez realizada tu compra se creará un registro de tu compra en la aplicación dónde podrás ver el estatus de cada compra.
    <br>
    Una vez reflejado el pago en nuestra cuenta, se actualizará el estatus de tu compra a "Pendiente de envió", lo que indica que tu compra se esta preparando para ser enviada.
    <br><br>
    Pago en Efectivo.
    <br>
    Te llegará un correo electrónico con la información de pago y la tienda o sucursal en donde podrás realizar el pago dependiendo la opción que hayas elegido.
    <br>
    Por seguridad recomendamos conservar los correos electrónicos que te lleguen.
    <br>
    Una vez reflejado el pago en nuestra cuenta, te enviaremos un correo electrónico confirmando pago y otro correo con el número de guía con el cual viaja tu pedido.
  </p>
  <h4>Políticas de Cambios y Devolución.</h4>
  <p>
    Todo situación que se presente por cambio o devolución deberán ser notificado por correo electrónico a contacto@bigfanofficial.com.mx enviando descripción del problema o razón de la devolución.
    <br><br>
    No se aceptan cambio ni devolución de producto no comprado en nuestro aplicación
  </p>
</div>
</div>
