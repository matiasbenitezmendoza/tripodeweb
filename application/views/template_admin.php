<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>Tripode</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url("admin/css/lib/bootstrap/bootstrap.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("admin/css/lib/sweetalert/sweetalert.css") ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("admin/css/helper.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("admin/css/style.css") ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url("admin/js/lib/jquery/jquery.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/bootstrap/js/popper.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/bootstrap/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/sweetalert/sweetalert.min.js") ?>"></script>
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo site_url("miadmin") ?>">
                               <!-- Logo text -->
                               <span><img src="<?php echo base_url("/admin/images/logoletras.png") ?>" alt="homepage" class="dark-logo" width="120px"/></span> 
                          
                          <!-- Logo icon -->
                          <b><img src="<?php echo base_url("/admin/images/logo.png") ?>" wit alt="homepage" class="dark-logo" width="40px" /></b> 
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="fa fa-bars"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="fa fa-bars"></i></a> </li>
                    </ul>
                    
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $this->auth->username(); ?>
							            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?php if( $this->auth->imagen() != "" ): ?>
                                <img src="<?php echo base_url("imgs/usuarios/".$this->auth->imagen()) ?>" alt="user" class="profile-pic" />
                              <?php else: ?>
                                <i class="fa fa-user-circle-o" style="font-size:32px"></i>
                              <?php endif; ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                   <!-- <li><a href="#"><i class="ti-user"></i> Perfil</a></li>-->
                                    <li><a href="<?php echo site_url("miadmin/login/logout") ?>"><i class="fa fa-power-off"></i> Salir</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Dashboard</li>
                     
                          <li>
                            <a href="<?php echo site_url("miadmin/inicio") ?>" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Tripode</span></a>
                          </li>

                          <!--
                          <li class="nav-label">Pedidos</li>
                          <li>
                            <a href="<?php echo site_url("miadmin/pedidos") ?>" aria-expanded="false"><i class="fa fa-shopping-bag"></i><span class="hide-menu">Ver</span></a>
                          </li>

                          -->
                      
                          <li class="nav-label">Pedidos</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/pedidos") ?>" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            
                          <li class="nav-label">Padecimientos</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/padecimientos") ?>" aria-expanded="false"><i class="fa fa-heartbeat"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            <li>
                              <a href="<?php echo site_url("miadmin/padecimientos/agregar") ?>" aria-expanded="false"><i class="fa fa-plus"></i><span class="hide-menu">Agregar</span></a>
                            </li>


                          <li class="nav-label">Temas de Suscripciones</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/temas") ?>" aria-expanded="false"><i class="fa fa-check-square"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            <li>
                              <a href="<?php echo site_url("miadmin/temas/agregar") ?>" aria-expanded="false"><i class="fa fa-plus"></i><span class="hide-menu">Agregar</span></a>
                            </li>

                        
                          <li class="nav-label">Clientes</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/clientes") ?>" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Lista</span></a>
                            </li>

                          <li class="nav-label">Mensajes</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/chats") ?>" aria-expanded="false"><i class="fa fa-comments"></i><span class="hide-menu">Lista</span></a>
                            </li>
                          
                          <li class="nav-label">Imagenes de Inicio</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/sliders") ?>" aria-expanded="false"><i class="fa fa-picture-o"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            <li>
                              <a href="<?php echo site_url("miadmin/sliders/agregar") ?>" aria-expanded="false"><i class="fa fa-plus"></i><span class="hide-menu">Agregar</span></a>
                            </li>

                          <li class="nav-label">Residencias</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/residencias") ?>" aria-expanded="false"><i class="fa fa-building"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            <li>
                              <a href="<?php echo site_url("miadmin/residencias/agregar") ?>" aria-expanded="false"><i class="fa fa-plus"></i><span class="hide-menu">Agregar</span></a>
                            </li>
                          
                          <!--
                          <li class="nav-label">Productos</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/productos") ?>" aria-expanded="false"><i class="fa fa-archive"></i><span class="hide-menu">Lista</span></a>
                            </li>
                          
                          <li class="nav-label">Pedidos</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/pedidos") ?>" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span class="hide-menu">Lista</span></a>
                            </li>-->
                        

                            

                          <!--Menu Administrador-->
                          <?php if(!$this->auth->loggedin() || $this->auth->usertype() == "Administrador"): ?>


                          <li class="nav-label">Repartidores</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/repartidores") ?>" aria-expanded="false"><i class="fa fa-motorcycle"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            <li>
                              <a href="<?php echo site_url("miadmin/repartidores/agregar") ?>" aria-expanded="false"><i class="fa fa-plus"></i><span class="hide-menu">Agregar</span></a>
                            </li>


                          <li class="nav-label">Usuarios</li>
                            <li>
                              <a href="<?php echo site_url("miadmin/usuarios") ?>" aria-expanded="false"><i class="fa fa-group"></i><span class="hide-menu">Lista</span></a>
                            </li>
                            <li>
                              <a href="<?php echo site_url("miadmin/usuarios/agregar") ?>" aria-expanded="false"><i class="fa fa-plus"></i><span class="hide-menu">Agregar</span></a>
                            </li>



                          
                          <?php endif; ?>
                         
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <?php echo $content ?>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url("admin/js/jquery.slimscroll.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/sidebarmenu.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>
    <script src="<?php echo base_url("admin/js/custom.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/datatables.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js") ?>"></script>
    <script src="<?php echo base_url("js/lib/datatables/datatables-init.js") ?>"></script>

</body>

</html>
