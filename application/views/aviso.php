<div id="container">
	<img src="<?php echo base_url("imgs/logo.jpg"); ?>" width="100px" />
  <div id="cont-aviso" class="cont-lightbox">
  <h2>Aviso de Privacidad</h2>
  <p>
    El presente Aviso de Privacidad se pone a su disposición de conformidad con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su reglamento y demás disposiciones aplicables (la “Ley”).
    <br><br>
    Diego Muñoz Quiroz que en lo sucesivo de denomina como <em>BIG FAN</em>
    <br>
    Persona física con domicilio en Circuito Poetas 44 Ciudad Satélite Naucalpan, CP 53100
    <br>
    México, es la entidad responsable del tratamiento de sus datos personales, del uso que se da a los mismos y de su protección.
    <br><br>
    <em>SI UTILIZA NUESTROS SITIOS WEB O NUESTRAS APPS, O NOS VISITA, ESTA INFORMACIÓN ES IMPORTANTE PARA USTED.</em>
    <br>
    Este Aviso provee información relevante para las personas que visitan, o proporcionan información en la aplicación Big Fan o en alguno de los Sitios web operados por Big Fan, nuestro Aviso de Privacidad también es aplicable a las personas que utilizan o usan nuestras páginas oficiales en redes sociales y las apps que de tiempo en tiempo Big Fan puede lanzar al mercado o poner a disposición de sus clientes (todos los anteriores, conjuntamente, nuestras "Propiedades Digitales"); lo anterior, siempre y cuando dichas Propiedad Digitales establezcan que este Aviso es aplicable. Este Aviso de Privacidad también es aplicable a las personas que nos proporcionan sus datos a través de nuestros servicios de soporte telefónico (call center) y a quienes visitan las instalaciones u oficinas de Big Fan, ya sea porque nos proporcionen sus datos para alguna finalidad o porque los mismos sean obtenidos a través de nuestros sistemas de video-vigilancia. A las personas aquí referidas, se les denominará conjuntamente los "clientes" o "usted").
    <br><br>
    El presente Aviso detalla la forma en que Big Fan, sus afiliadas y subsidiarias utilizan sus datos personales y sobre los derechos que usted tiene. Por favor tómese un minuto para revisar esta información.
    <br><br>
    <em>NUESTRO COMPROMISO CON SUS DERECHOS Y SU PRIVACIDAD.</em>
    <br>
    En Big Fan entendemos la relevancia de proteger la privacidad de las personas con las que interactuamos día a día incluyendo nuestros clientes, prospectos de clientes, invitados, prospectos de distribuidores, proveedores diversos, empleados y candidatos de empleo (los "titulares de datos", los "sujetos" o "usted"). El presente Aviso contiene información importante para los clientes de Big Fan y explica la forma en que obtenemos, utilizamos y compartimos sus datos personales y los derechos con los que usted cuenta. En Big Fan nos comprometemos a tratar sus datos personales conforme a los principios de licitud, calidad, consentimiento, información, finalidad, lealtad, proporcionalidad y responsabilidad establecidos en la Ley.
  </p>
</div>
</div>
