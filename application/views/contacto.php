<section id="inicio">
  <div id="cont_header_nosotros">
    <div class="swiper-container1">
      <div class="swiper-slide img_backgournd" id="img_contacto">
        <h1>CONTACTO</h1>
      </div>
    </div>
    <a href="#elmezcal" id="scrollDown"></a>
  </div>
</section>

<section id="elmezcal">
  <div id="cont_elmezcal">
    <p>
    Si deseas mayor información acerca de nuestros productos o acerca de nostros, estamos a tu servicio en:
    <br>
    <img src="<?php echo base_url('imgs/telefono.png'); ?>" class="img_contacto_text"/> 70 98 10 79
    <br>
    <img src="<?php echo base_url('imgs/email.png'); ?>" class="img_contacto_text" />  contacto@mezcalbrije.com
    </p>
    <a href="#img_mascara" id="scrollDown"></a>
  </div>
</section>

<section id="contacto_form">
  <form action="" method="post">
    <?php _print_messages(); ?>
    <div class="form-group">
      <label class="control-label">Nombre</label>
      <input class="form-control" type="text" name="nombre" value="<?php echo $fields['nombre'] ?>" >
    </div>
    <div class="form-group">
      <label class="control-label">Correo</label>
      <input class="form-control" type="text" name="correo" value="<?php echo $fields['correo'] ?>" >
    </div>
    <div class="form-group">
      <label class="control-label">Asunto</label>
      <input class="form-control" type="text" name="asunto" value="<?php echo $fields['asunto'] ?>" >
    </div>
    <div class="form-group">
      <label class="control-label">Mensaje</label>
      <textarea rows="10" name="mensaje"><?php echo $fields['mensaje'] ?></textarea>
    </div>
    <div class="form-group">
      <input type="submit" value="Enviar" name="enviar" class="btn_form">
    </div>
  </form>
</section>
