<div class="checkout">
  <div class="row align_center">
    <div class="col-lg-9 col-md-9 col-sm-9 w70">
      <p class="tittle">Carrito de compras</p>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 w30 toggle_checkout">
      <button type="submit" name="button" class="button_pink toggle_checkout_button">Total y Envio</button>
    </div>
  </div>
  <div class="row row-offcanvas row-offcanvas-right">
    <div class="col-lg-9 col-md-12">
        <div class="line_black"></div>
        <div class="row product_list">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <p class="tcenter text-uppercase">Articulo</p>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 w33">
            <p class="tcenter text-uppercase">Cantidad</p>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 w33">
            <p class="tcenter text-uppercase">Precio</p>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 w33">
            <p class="tcenter text-uppercase">Envio</p>
          </div>
        </div>
        <div class="line_black"></div>
        <?php
        $subtotal = 0;
        $envio = 0;
        if (isset($productos) && is_array($productos) && count($productos) > 0):
          foreach($productos as $key => $producto):
        		if ($producto->imagen != ""): ?>
            <div class="row product_list">
              <div class="col-lg-2 col-md-2 col-sm-2 w50">
                <a href="<?php echo site_url("store/product_detail/$producto->idproducto") ?>"><img src="<?php echo base_url("imgs/productos/" . $producto->imagen) ?>" alt=""></a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 w50">
                <p class="name"><?php echo $producto->nombre ?></p>
                <p class="descrip_short"><?php echo $producto->descripcion_corta ?></p>
                <button type="submit" name="button" class="button_red" value="<?php echo $producto->idproducto ?>">Eliminar</button>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 w33 align_center_center">
                <input id="prod_<?php echo $producto->idproducto ?>" type="number" class="quantity" name="" value="<?php echo $store['item'][$producto->idproducto]; ?>" min="1" max="<?php echo $producto->stock ?>">
                <img id="<?php echo $producto->idproducto ?>" src="<?php echo base_url("imgs/refresh.png"); ?>" alt="" class="img_quantity display_none" >
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 w33">
                <p class="price">$<?php echo $producto->precio ?></p>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 w33">
                <p class="price">$<?php echo $producto->envio ?></p>
              </div>
            </div>
            <div class="line_black"></div>
            <?php
            $envio_prod = $producto->envio;
            $envio = $envio + $envio_prod;
            $total_prod = $store['item'][$producto->idproducto] * $producto->precio;
            $subtotal = $subtotal + $total_prod;
        		endif;
        	endforeach;
        endif;
        ?>
      </div>
    <div class="col-lg-3 col-md-12 col-sm-12 sidebar-offcanvas" id="sidebar">
      <div class="address">
        <p class="font-weight-bold">Dirección de envío</p>
        <?php if ($addresses[0] != "") {
          for ($i=0; $i < count($addresses); $i++) {?>
            <div class="form-check align_center_center">
              <input class="form-check-input direcciones" type="radio" name="direcciones" id="dir_<?php echo $addresses[$i]->iddireccion; ?>" value="<?php echo $addresses[$i]->iddireccion; ?>" <?php if($i == 0){echo "checked";} ?> >
              <label class="form-check-label" for="exampleRadios1">
                <?php echo $addresses[$i]->calle; ?> <?php echo $addresses[$i]->exterior; ?> <?php echo $addresses[$i]->interior; ?>
                <?php echo $addresses[$i]->colonia; ?> <?php echo $addresses[$i]->ciudad; ?> <?php echo $addresses[$i]->estado; ?> <?php echo $addresses[$i]->codigo_postal; ?>
              </label>
            </div>
            <div class="line_black"></div>
          <?php };?>
        <?php };?>
      </div>
      <div class="payment">
          <p class="font-weight-bold">Resumen</p>
          <div class="row">
            <div class="col-6">
              <p>Costo de envio</p>
            </div>
            <div class="col-6">
              <p class="text-right"><?php echo $envio ?></p>
            </div>
          </div>
          <div class="line_black"></div>
          <div class="row">
            <div class="col-6">
              <p>Subtotal</p>
            </div>
            <div class="col-6">
              <p id="subtotal" class="text-right"><?php echo $subtotal ?></p>
            </div>
          </div>
          <div class="line_black"></div>
          <div class="row">
            <div class="col-6">
              <p>Total</p>
            </div>
            <div class="col-6">
              <p id="total" class="text-right"><?php echo $subtotal+$envio ?></p>
            </div>
          </div>
        </div>
      <div class="payment_button">
        <form id="pay_form" action="<?php echo base_url("index.php/store/payment"); ?>/<?php echo $addresses[0]->iddireccion; ?>/<?php echo $subtotal+$envio ?>" method="POST">
          <script
              src="https://www.mercadopago.com.mx/integrations/v1/web-tokenize-checkout.js"
              data-public-key="<?php echo $publicKey; ?>"
              data-transaction-amount="<?php echo $subtotal+$envio ?>"
              data-button-label="Realizar pago"
              data-elements-color="#4e0195"
              data-header-color="#c0392b"
              data-customer-id="<?php echo $id_mp_user; ?>"
              data-card-ids="<?php for ($i=0; $i < sizeof($cards_info) ; $i++){ echo $cards_info[$i]->{'id'}.","; } ?>"
              data-summary-product-label="Productos"
              data-summary-product="<?php echo $subtotal ?>"
              data-summary-shipping="<?php echo $envio ?>">
            </script>
        </form>
      </div>
      </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
$('.toggle_checkout_button').click(function () {
  $('.row-offcanvas').toggleClass('active')
});
});
</script>
<script type="text/javascript">
$( ".button_red" ).click(function() {
    $(".checkout").loading({
      message: 'Borrando...'
    });
    var $id_item = $( this ).val();
    console.log($id_item);
    //Delete card
    $.post( "<?php echo site_url("/store/delete_cart") ?>", {id_item:$id_item}, function( data ) {
    var res = data;
    if( res == "error" ){
    swal("My Competition", res.msg, res.type);
    return false;
    }else{
    location.reload("/");
    //$('#subtotal' ).replaceWith( "<p id='subtotal' class='text-right'>$<?php echo $subtotal ?></p>" );
    }
    console.log("eliminado");
  });
});
</script>
<script type="text/javascript">
$( ".img_quantity" ).click(function() {
    $(".checkout").loading({
      message: 'Actualizando...'
    });
    var $id_item = $( this ).attr('id');
    var input_quantity = "#prod_"+$id_item;
    var $quantity = $( input_quantity ).val();
    console.log($id_item + $quantity);
    //Delete card
    $.post( "<?php echo site_url("/store/add_cart") ?>", {id_item:$id_item,quantity:$quantity}, function( data ) {
    var res = data;
    if( res == "error" ){
    swal("My Competition", res.msg, res.type);
    return false;
    }else{
      location.reload("/");
    }
    console.log("actualizado");
  });
});
</script>
<script type="text/javascript">
$( ".quantity" ).change(function() {
    var $id_item = $( this ).attr('id');
    var $input_id = $id_item.substring(5);
    var $id_img = "#"+$input_id;
    $( $id_img ).removeClass('display_none');
  });
</script>
<script type="text/javascript">
$('.direcciones').click(function(){
  $id_dir = $( this ).val();
  console.log($id_dir);
  $address_action = '<?php echo base_url("index.php/store/payment"); ?>' +'/'+ $id_dir + '/' + '<?php echo $subtotal+$envio ?>';
 $('#pay_form').attr('action', $address_action);
});
</script>
