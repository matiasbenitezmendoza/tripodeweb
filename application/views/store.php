<?php
/*
* Mensajes
*/
_print_messages();
?>
<div class="start_page"></div>
<div class="banner_store">
  <p>Libera tu Big Fan<br>y llevate increibles artículos y experiencias</p>
</div>
<div class="div_store">
  <div class="row toggle_checkout">
    <div class="col-6">
      <button type="submit" name="button" class="button_pink toggle_checkout_button m1r"><i class="fas fa-angle-left"></i>Filtros</button>
    </div>
  </div>
  <div class="row row-offcanvas row-offcanvas-left">
    <div class="col-lg-3 col-md-12 filters sidebar-offcanvas" id="sidebar">
        <div class="card">
          <form class="" action="<?php echo site_url("store/search") ?>" method="post">
            <input type="text" name="f_search" class="form-control" placeholder="Buscar" autocomplete="off">
          <a data-toggle="collapse" data-target="#advanced_search" class="advanced_search_toggle"><p><i class="fas fa-angle-down"></i>Busqueda avanzada</p></a>
          <div class="line_black mtop"></div>
          <div id="advanced_search" class="collapse">
            <p class="tittle">Ordernar</p>
            <div class="row">
              <input type="hidden" name="order_by" value="asc">
              <div class="col-3 text-center">
                <a href="<?php echo site_url("store/search/asc") ?>"><i class="fas fa-sort-alpha-down" data-toggle="tooltip" data-placement="top" title="Ascendente"></i></a>
              </div>
              <div class="col-3 text-center">
                <a href="<?php echo site_url("store/search/desc") ?>"><i class="fas fa-sort-alpha-up" data-toggle="tooltip" data-placement="top" title="Descendente"></i></a>
              </div>
              <div class="col-3 text-center">
                <a href="<?php echo site_url("store/search/asc/precio") ?>"><i class="fas fa-sort-numeric-down" data-toggle="tooltip" data-placement="top" title="Menor"></i></a>
              </div>
              <div class="col-3 text-center">
                <a href="<?php echo site_url("store/search/desc/precio") ?>"><i class="fas fa-sort-numeric-up"  data-toggle="tooltip" data-placement="top" title="Mayor"></i></a>
              </div>
            </div>
            <div class="line_black"></div>
            <p class="tittle">Filtrar</p>
            <div class="row">
              <select class="custom-select" id="inputGroupSelect01">
                <option selected>Choose...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
            </div>
            <p class="tittle">Talento</p>
            <div class="row">
              <div class="col-4 text-center">
                <a href="store/search/desc/precio"><i class="fas fa-female" data-toggle="tooltip" data-placement="top" title="Mujer"></i></a>
              </div>
              <div class="col-4 text-center">
                <a href="store/search/desc/precio"><i class="fas fa-male" data-toggle="tooltip" data-placement="top" title="Hombre"></i></a>
              </div>
              <div class="col-4 text-center">
                <a href="store/search/desc/precio"><i class="fas fa-users" data-toggle="tooltip" data-placement="top" title="Agrupacion"></i></a>
              </div>
            </div>
            <p class="tittle">Rango de precio</p>
            <div class="row">
              <div class="col-6">
                <b id="value1">$100</b>
              </div>
              <div class="col-6 text-right">
                <b id="value2">$100000</b>
              </div>
              <div class="col-12">
                <input id="ex2" type="text" class="span2" value="" data-slider-min="100" data-slider-max="50000" data-slider-step="100" data-slider-value="[100,50000]"/>
              </div>
              <input id="price_range" type="hidden" name="range" value="">
            </div>
            <p class="tittle">Envio</p>
            <div class="row">
              <div class="col-6 text-center">
                <a href="store/search/desc/precio"><i class="fas fa-truck" data-toggle="tooltip" data-placement="top" title="Gratis"></i></a>
              </div>
              <div class="col-6 text-center">
                <a href="store/search/desc/precio"><i class="fas fa-money-bill-alt" data-toggle="tooltip" data-placement="top" title="Con costo"></i></a>
              </div>
            </div>
            <p class="tittle">Categorias</p>
            <div class="row">
              <?php
              if (isset($categorias) && is_array($categorias) && count($categorias)>0):
                foreach ($categorias as $key => $categoria):
                 ?>
                  <div class="col-6">
                    <input type="checkbox" class="form-check-input checkbox-success" name="categorias[]" value="<?php echo $categoria->nombre ?>" id="cat_<?php echo $categoria->nombre ?>">
                    <label class="form-check-label" for="cat_<?php echo $categoria->nombre ?>"><?php echo $categoria->nombre ?></label>
                  </div>
                  <?php
                endforeach;
              endif;
             ?>
            </div>
          </div>
          <div class="button_search">
            <button type="submit" name="button" class="button_pink">Buscar</button>
          </div>
          </form>
        </div>


    </div>
    <div class="col-lg-9 col-md-12 products">
      <div class="row">
        <?php
          if (isset($productos) && is_array($productos) && count($productos)>0):
            foreach ($productos as $key => $producto):
             ?>
        <?php if ($producto->imagen != ""): ?>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <a href="<?php echo site_url('store/product_detail/'.$producto->idproducto) ?>">
            <div class="card">
              <div class="img_prod">
                <img src="<?php echo base_url("imgs/productos/".$producto->imagen) ?>" alt="">
                <div class="tittle">
                  <p><?php echo $producto->nombre ?><br><span>$<?php echo $producto->precio ?></span></p>
                </div>
              </div>
            </div>
          </a>
        </div>
      <?php endif;
          endforeach;
        endif;
      ?>
      </div>

      <?php if ($total_paginas > 1): ?>
      <div id="cont_pager">
        <?php if ($pagina_actual > 1): ?>
          <a href="<?php echo site_url('store/pagina/'.($pagina_actual-1)) ?>" class="start"><i class="fas fa-angle-double-left"></i></a>
        <?php endif; ?>
        <?php for ($i=1;$i<=ceil($total_paginas);$i++): ?>
          <a href="<?php echo site_url('store/pagina/'.$i) ?>" <?php echo ($i==$pagina_actual)?'class="active"':'' ?>><?php echo $i ?></a>
        <?php endfor; ?>
        <?php if ($pagina_actual < $total_paginas): ?>
          <a href="<?php echo site_url('store/pagina/'.($pagina_actual+1)) ?>" class="end"><i class="fas fa-angle-double-right "></i></a>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    </div>
  </div>
</div>
<script>
$(document).ready(function () {
$('.toggle_checkout_button').click(function () {
  $('.row-offcanvas').toggleClass('active');
  $(".toggle_checkout_button i").toggleClass("fa-angle-right");
});
$(".advanced_search_toggle").click(function(){
    $(".fa-angle-down").toggleClass("fa-angle-up");
});
});
</script>
<script type="text/javascript">
// With JQuery
$("#ex2").slider({});
$("#ex2").change(function(){
  var value = $("#ex2").slider('getValue');
  $("#value1").text(value[0]);
  $("#value2").text(value[1]);
  $("#price_range").val(value);

  console.log(value);
});

</script>
