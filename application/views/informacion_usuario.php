<div class="informacion_usuario">
  <div class="row">
    <div class="col-10">
      <p class="tittle">Informacion del usuario</p>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-12">
      <p class="subtittle">Informacion General</p>
      <div class="line_black"></div>
      <div class="information_block">
        <div class="info_element text-right">
          <!--<a href="#" class="edit_link_user">Editar</a>-->
        </div>
        <div class="info_element">
          <img src="<?php echo $img_profile ?>" alt="">
        </div>
        <form class="" action="<?php echo base_url("index.php/inicio/user_info") ?>" method="post">
               <?php if($users[0]->email == $user_data['email']){ ?>
                 <input type="hidden" name="id" value="<?php echo $users[0]->id; ?>">
                 <div class="info_element">
                   <p class="tittle_info">Nombre(s)</p>
                   <input type="text" name="first_name" class="input_data_user" value="<?php echo $users[0]->first_name ?>" disabled>
                 </div>
                 <div class="info_element">
                   <p class="tittle_info">Apellido(s)</p>
                   <input type="text" name="last_name" class="input_data_user" value="<?php echo $users[0]->last_name ?>" disabled>
                 </div>
                 <div class="info_element">
                   <p class="tittle_info">Correo Electronico</p>
                   <input type="text" name="email" class="input_data_user" value="<?php echo $users[0]->email ?>" disabled>
                 </div>
                 <!--<div class="info_element">
                   <p class="tittle_info">Contraseña</p>
                   <input type="password" name="password" class="input_data_user" value="<?php echo $user->password ?>" disabled>
                 </div>-->
              <?php } ?>
          <!--<button type="submit" name="button" class="button_purple ele_center input_data_user" disabled>Guardar</button>-->
        </form>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
      <p class="subtittle">Metodos de pago</p>
      <div class="line_black"></div>
      <div class="information_block">
        <div class="info_element text-right">
          <a href="#" class="edit_link_cards">Editar</a>
        </div>
        <?php $cards_info = $this->mercadopagofunctions->get_cards_info($user_data['email'],$id_mp_user); ?>
        <?php for ($i=0; $i < sizeof($cards_info) ; $i++) {?>
         <div class="info_element">
           <p class="tittle_info">Nombre del tarjetahabiete</p>
           <p class="value_info"><?php echo $cards_info[$i]->{'cardholder'}->{'name'}; ?></p>
           <input type="hidden" name="" value="<?php echo $cards_info[$i]->{'id'}; ?>">
         </div>
         <div class="info_element">
           <p class="tittle_info">No. de tarjeta</p>
           <p class="value_info">*********<?php  echo $cards_info[$i]->{'last_four_digits'}; ?></p>
           <img src="<?php echo $cards_info[$i]->{'payment_method'}->{'thumbnail'}; ?>" alt="" class="card_img">
         </div>
         <div class="info_element">
           <p class="tittle_info">Fecha de vencimiento</p>
           <p class="value_info"><?php  echo $cards_info[$i]->{'expiration_month'}; ?>/<?php  echo $cards_info[$i]->{'expiration_year'}; ?></p>
         </div>
         <div class="info_element">
           <button type="button" name="button" class="delete_card display_none ele_center button_red" value="<?php echo $cards_info[$i]->{'id'}; ?>" disabled>Borrar</button>
           <div class="loader loader_red loader_delete_card display_none <?php echo $cards_info[$i]->{'id'}; ?>">Loading...</div>
         </div>
         <div class="line_black"></div>
        <?php } ?>
        <div class="button_add_payment">
          <button name="button" class="button_purple_shop" data-toggle="modal" data-target="#add_card">Agregar Metodo</button>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
      <p class="subtittle">Direcciones de envio</p>
      <div class="line_black"></div>
      <div class="information_block">
        <div class="info_element text-right">
          <a href="#" class="edit_link_address">Editar</a>
        </div>
        <div id="accordion">
        <?php if ($addresses[0] != "") {
          for ($i=0; $i < count($addresses); $i++) {?>
            <div class="">
              <div class="address_accordion" id="address<?php echo $i ?>">
                <h5 class="mb-0">
                  <button class="btn btn-link <?php if ($i == 0) {echo "";}else{echo "collapsed";} ?>" data-toggle="collapse" data-target="#address_<?php echo $i ?>" aria-expanded="<?php if ($i == 0) {echo "true";}else{echo "false";} ?>" aria-controls="collapseOne">
                    <?php echo $addresses[$i]->calle; ?> <?php echo $addresses[$i]->exterior; ?>
                  </button>
                </h5>
              </div>
              <div id="address_<?php echo $i ?>" class="collapse <?php if ($i == 0) {echo "show";}else{echo "";} ?>" aria-labelledby="address<?php echo $i ?>" data-parent="#accordion">
                <form class="" action="<?php echo base_url("index.php/inicio/edit_addres");?>" method="post">
                  <input type="hidden" name="iddireccion" value="<?php echo $addresses[$i]->iddireccion; ?>">
                  <input type="hidden" name="idusuario" value="<?php echo $addresses[$i]->idusuario; ?>">
                  <div class="info_element">
                    <p class="tittle_info">Calle</p>
                    <input type="text" name="calle" class="input_data_address" value="<?php echo $addresses[$i]->calle; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">Numero Exterior</p>
                    <input type="text" name="exterior" class="input_data_address" value="<?php echo $addresses[$i]->exterior; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">Número Interior o piso</p>
                    <input type="text" name="interior" class="input_data_address" value="<?php echo $addresses[$i]->interior; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">Colonia</p>
                    <input type="text" name="colonia" class="input_data_address" value="<?php echo $addresses[$i]->colonia; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">Ciudad</p>
                    <input type="text" name="ciudad" class="input_data_address" value="<?php echo $addresses[$i]->ciudad; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">Estado</p>
                    <input type="text" name="estado" class="input_data_address" value="<?php echo $addresses[$i]->estado; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">C.P.</p>
                    <input type="text" name="codigo_postal" class="input_data_address" value="<?php echo $addresses[$i]->codigo_postal; ?>" disabled>
                  </div>
                  <div class="info_element">
                    <p class="tittle_info">Telefono</p>
                    <input type="text" name="telefono" class="input_data_address" value="<?php echo $addresses[$i]->telefono; ?>" disabled>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <button type="submit" name="button" class="button_purple ele_center edit_address display_none input_data_address" value="<?php echo $addresses[$i]->iddireccion; ?>" disabled>Actualizar</button>
                    </div>
                    <div class="col-6">
                      <button type="submit" name="button" class="button_red ele_center delete_address display_none input_data_address" value="<?php echo $addresses[$i]->iddireccion; ?>" disabled>Borrar</button>
                    </div>
                  </div>
                  <div class="loader loader_edit_address display_none <?php echo $addresses[$i]->iddireccion; ?>edit">Loading...</div>
                  <div class="loader loader_red loader_delete_address display_none <?php echo $addresses[$i]->iddireccion; ?>delete">Loading...</div>
                </form>
              </div>
            </div>
            <div class="line_black"></div>
          <?php };?>
        <?php };?>
        </div>
        <div class="button_add_payment">
          <button name="button" class="button_purple_shop" data-toggle="modal" data-target="#add_address">Agregar dirección</button>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
      <p class="subtittle">Historial de compras</p>
      <div class="line_black"></div>
      <div class="information_block">
        <?php
        if ($purchases)  //Ensure that there is at least one result
        {
           foreach ($purchases as $row)  //Iterate through results
           { ?>
              <div class="info_element">
                <div class="row align_center">
                  <div class="col-9">
                    <p class="tittle_info m0"><?php echo substr($row->fecha, 0, -9); ?></p>
                    <p class="value_info m0"><?php echo $row->producto; ?></p>
                  </div>
                  <div class="col-3 text-right">
                    <p class="tittle_info m0"><?php echo $row->cantidad; ?> x <?php echo $row->precio; ?></p>
                  </div>
                  <div class="col-9">
                    <p class="tittle_info m0">Envio</p>
                  </div>
                  <div class="col-3 text-right">
                    <p class="tittle_info m0"><?php echo $row->envio; ?></p>
                  </div>
                  <div class="col-12">
                    <p class="value_status m0"><?php echo $row->estatus; ?></p>
                  </div>
                  <div class="col-12 text-right">
                    <p class="tittle_info m0 text-right">Total: <?php echo $row->costo; ?></p>
                  </div>
                </div>

                <div class="line_black"></div>
              </div>
           <?php }
        } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="add_card" tabindex="-1" role="dialog" aria-labelledby="add_card" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Agregar una nueva tarjeta</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="line_black"></div>
      <div class="modal-body">
        <form action="<?php echo base_url("index.php/inicio/user_info") ?>" method="post" id="form-pagar-mp">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="form_label">Numero de Tarjeta:</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input class="form_input ele_center card_number" data-checkout="cardNumber" type="text" placeholder="5474 9254 3267 0366"/>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="form_label">Codigo de Seguridad:</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input class="form_input ele_center cvv" data-checkout="securityCode" type="text" placeholder="123" />
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="form_label">Mes de Expiracion:</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input class="form_input ele_center date" data-checkout="cardExpirationMonth" type="number" placeholder="05" max="12"/>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="form_label">Año de Expiracion:</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input class="form_input ele_center date" data-checkout="cardExpirationYear" type="number" placeholder="22" min="<?php echo date("y"); ?>"/>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="form_label">Titular de la Tarjeta:</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input class="form_input ele_center" data-checkout="cardholderName" type="text" placeholder="Tu Nombre"/>
              </div>
            </div>
            <!--<p>Numero de Documento: <input data-checkout="docNumber" type="text"/></p>-->
            <input data-checkout="docType" type="hidden" value=""/>
            <div class="line_black"></div>
            <div class="row">
              <div class="col-12">
                <input type="submit" value="Agregar Tarjeta" class="btn btn-primary ele_center add_card">
                <div class="loader loader_add_card display_none">Loading...</div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="add_address" tabindex="-1" role="dialog" aria-labelledby="add_address" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Agregar una nueva direccion</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="line_black"></div>
      <div class="modal-body">
        <form class="" action="<?php echo base_url("index.php/inicio/insert_address"); ?>" method="post">
          <input type="hidden" name="idusuario" value="<?php echo $users[0]->id ?>">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Calle</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="calle" class="input_data_address" value="" placeholder="Calle" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Número exterior</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="exterior" class="input_data_address" value="" placeholder="Nº Exterior" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Número interior</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="interior" class="input_data_address" value="" placeholder="Nº Interior" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Colonia</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="colonia" class="input_data_address" value="" placeholder="Colonia" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Ciudad</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="ciudad" class="input_data_address" value="" placeholder="Ciudad" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Estado</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="estado" class="input_data_address" value="" placeholder="Estado" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">C.P.</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="codigo_postal" class="input_data_address codigo_postal" value="" placeholder="Codigo Postal" autocomplete="off">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="tittle_info">Telefono</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <input type="text" name="telefono" class="input_data_address phone_number" value="" placeholder="Telefono" autocomplete="off">
              </div>
            </div>
            <div class="line_black"></div>
            <div class="row">
              <div class="col-12">
                <input type="submit" value="Agregar Dirección" class="btn btn-primary ele_center add_address">
                <div class="loader add_address display_none">Loading...</div>
              </div>
            </div>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.date').mask('00');
  $('.cvv').mask('0000');
  $('.card_number').mask('0000 0000 0000 0000');
  $('.codigo_postal').mask('00000');
  $('.phone_number').mask('00 00 00 00 00 00');
});
</script>
<script type="text/javascript">
$( ".edit_link_user" ).click(function() {
  $(".input_data_user" ).prop( "disabled", false );
});

$( ".edit_link_address" ).click(function() {
  $(".input_data_address" ).prop( "disabled", false );
  $(".delete_address" ).toggleClass( "display_none" );
  $(".edit_address" ).toggleClass( "display_none" );
});

$( ".edit_link_cards" ).click(function() {
  $(".delete_card" ).prop( "disabled", false );
  $(".delete_card" ).toggleClass( "display_none" );
});

$( ".add_card" ).click(function() {
  $(".loader_add_card" ).toggleClass( "display_none" );
  $(".add_card").toggleClass( "display_none");
});

$( ".add_address" ).click(function() {
  $(".loader_add_address" ).toggleClass( "display_none" );
  $(".add_address").toggleClass( "display_none");
});

$( ".edit_address" ).click(function() {
  var $id = $( this ).val();
  var $class_button = "."+$id+"edit";
  $($class_button ).toggleClass( "display_none" );
  $(".edit_address").toggleClass( "display_none");
  $(".delete_address").toggleClass( "display_none");
});

</script>
<script type="text/javascript">
Checkout.setPublishableKey("<?php echo $publicKey ?>");
$("#form-pagar-mp").submit(function(event) {
    var $form = $(this);
    Checkout.createToken($form, mpResponseHandler);
    event.preventDefault();
    return false;
});
var mpResponseHandler = function(status, response) {
    var $form = $('#form-pagar-mp');
    if (response.error) {
      console.log(response.cause[0]);
      $error = response.cause[0].code;
      switch ($error) {
          case "E301","205":
            Swal.fire(
              'Numero de Tarjeta',
              'Por favor verifica el numero de tu tarjeta',
              'error'
            )
          break;
          case "E302","224":
              Swal.fire(
                'Codigo de Seguridad',
                'Por favor verifica el codigo de seguridad de tu tarjeta',
                'error'
              )
          break;
          case "208","325":
              Swal.fire(
                'Mes de Expiración',
                'Por favor verifica el  mes de expiración de tu tarjeta',
                'error'
              )
          break;
          case "209":
              Swal.fire(
                'Año de Expiración',
                'Por favor verifica el  año de expiración de tu tarjeta',
                'error'
              )
          break;
          case "221":
              Swal.fire(
                'Titular de Tarjeta',
                'Por favor verifica el titular de tu tarjeta',
                'error'
              )
          break;
          default:
          Swal.fire(
            'Información de Tarjeta',
            'Por favor verifica la información de tu tarjeta',
            'error'
          )
          break;
      }
      $(".loader_add_card").addClass("display_none");
      $(".add_card").removeClass("display_none");
    } else {
        //console.log(response);
        var $card_token_id = response.id;
        //$form.append($('<input type="hidden" id="card_token_id" name="card_token_id"/>').val($card_token_id));
        $.post( "<?php echo site_url("/inicio/add_card_user") ?>", {id_card:$card_token_id,id_mp_user:"<?php echo $id_mp_user; ?>"}, function( data ) {
        var res = data;
        if( res.type == "error" ){
        swal("My Competition", res.msg, res.type);
        return false;
        }else{
        location.reload("/");
        }
        console.log("Agregado");
        });
    }
}
</script>
<script type="text/javascript">
$( ".delete_card" ).click(function() {
  Swal.fire({
  title: 'Estas Seguro?',
  text: "No podras revertir esta acción!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#da4a47',
  cancelButtonColor: '#4e0195',
  confirmButtonText: 'Si, Borrar!'
}).then((result) => {
  if (result.value) {
    var $id_card = $( this ).val();
    var $class_button = "."+$id_card;
    $(".delete_card" ).toggleClass( "display_none" );
    $( $class_button ).toggleClass( "display_none" );

    //Delete card
    $.post( "<?php echo site_url("/inicio/delete_card_user") ?>", {id_card:$id_card,id_mp_user:"<?php echo $id_mp_user; ?>"}, function( data ) {
    var res = JSON.parse(data);
    if( res.type == "error" ){
    swal("My Competition", res.msg, res.type);
    return false;
    }else{
    location.reload("/");
    }
    console.log("Eliminado");
  });
  }
});

});
</script>
<script type="text/javascript">
$( ".delete_address" ).click(function() {
    var $id_address = $( this ).val();
    var $class_button = "."+$id_address+"delete";
    $($class_button ).toggleClass( "display_none" );
    $(".edit_address").toggleClass( "display_none");
    $(".delete_address").toggleClass( "display_none");

    //Delete card
    $.post( "<?php echo site_url("/inicio/delete_address") ?>", {id_address:$id_address}, function( data ) {
    var res = data;
    if( res == "error" ){
    swal("My Competition", res.msg, res.type);
    return false;
    }else{
    location.reload("/");
    }
    console.log("Eliminado");
  });
});
</script>
