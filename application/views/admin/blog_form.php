<script src="https://cdn.ckeditor.com/4.7.3/standard-all/ckeditor.js"></script>
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-edit"></i> Blog</h1>
      <p>Contenido de tu blog</p>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li><a href="<?php echo site_url('miadmin/blog') ?>">Blog</a></li>
        <li>nombre de la entrada puede ser muy larga</li>
      </ul>
    </div>
  </div>
  <!-- /Titulo de la página -->

  <?php _print_messages(); ?>

  <!-- Contenido -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <h3 class="card-title"><?php echo $accion ?></h3>
        <div class="card-body">
          <form method="post" action="<?php echo site_url('miadmin/blog/'.mb_strtolower($accion).'/'.$id) ?>" enctype="multipart/form-data">
            <div class="form-group">
              <label class="control-label">Título</label>
              <input class="form-control" type="text" placeholder="Ingresa el título de la entrada" name="titulo" value="<?php echo $fields['titulo']['value'] ?>" />
            </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="destacada" value="1" <?php echo ($fields['destacada']['value']==1)?'checked':'' ?>> Entrada destacada
                </label>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Slug</label>
              <input class="form-control" type="text" placeholder="" disabled value="<?php echo $fields['slug']['value'] ?>" >
            </div>
            <div class="form-group">
              <label class="control-label">Introducción</label>
              <textarea class="form-control" rows="4" name="introduccion" placeholder="Ingresa la introducción de la entrada"><?php echo $fields['introduccion']['value'] ?></textarea>
            </div>
            <div class="form-group">
              <label class="control-label">Contenido</label>
              <textarea class="form-control" rows="24" name="contenido" id="contenido_editor" placeholder="Ingresa el contenido de la entrada"><?php echo $fields['contenido']['value'] ?></textarea>
            </div>
            <div class="form-group">
              <label class="control-label">Keywords - SEO</label>
              <textarea class="form-control" rows="4" name="keywords" placeholder="Ingresa las 'keywords' de la entrada"><?php echo $fields['keywords']['value'] ?></textarea>
            </div>
            <div class="form-group">
              <label class="control-label">Description - SEO</label>
              <textarea class="form-control" rows="4" name="description" placeholder="Ingresa la 'description' de la entrada"><?php echo $fields['description']['value'] ?></textarea>
            </div>
            <div class="form-group">
              <label class="control-label">Imagen</label>
              <input class="form-control" type="file" name="imagen">
              <?php if( $fields['imagen']['value'] != '' ): ?>
                <br>
                <img src="<?php echo base_url("blog/".$fields['imagen']['value']) ?>" width=""="50%" />
              <?php endif; ?>
            </div>
            <div class="card-footer">
              <button class="btn btn-primary icon-btn" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>
              &nbsp;&nbsp;&nbsp;
              <a class="btn btn-default icon-btn" href="<?php echo site_url('miadmin/blog') ?>"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancelar</a>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	CKEDITOR.replace( 'contenido_editor', {
    filebrowserImageUploadUrl:  '<?php echo site_url("miadmin/blog/uploadimage") ?>',
    filebrowserBrowseUrl:  '<?php echo site_url("miadmin/blog/show_img") ?>',
    extraAllowedContent: '*(*);*{*};*[*]',
    AllowedContent: '*(*);*{*};*[*]',
    removeDialogTabs: '',
    extraPlugins: 'image'
		});
</script>
