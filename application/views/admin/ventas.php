<section id="administracion">
	<h1 id="title_adminInt">Ventas</h1>
	<div class="txt_right">
		<a href="<?php echo site_url('admin/venta/agregar') ?>" class="btn_black fancybox fancybox.iframe">+ Nueva Venta</a>
	</div>
	<br>
	<?php echo form_open('','name="formPromos" id="formPromos"') ?>
	<table class="table_admin">
		<thead>
			<tr>
				<th>Cliente</th>
				<th>Productos</th>
				<th>Total</th>
				<th>Fecha</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if( isset($ventas) && is_array($ventas) ):
				foreach($ventas as $info):
				?>
					<tr>
					<td><?php echo $info->nombre ?></td>
					<td>
						<?php
						$total = 0;
						$productos_arr = json_decode($info->productos);
						if( isset($productos_arr) && is_array($productos_arr) ){
							foreach($productos_arr as $item){
								echo $item->producto.'<br>';
								$p = explode("-", $item->producto);
								$total += $p[1]*$item->cantidad;
							}
						}
						?>
					</td>
					<td><?php echo "$".number_format($info->total,2) ?></td>
					<td><?php echo $info->fecha ?></td>
				</tr>
			<?php
				endforeach;
			endif;
			?>
		</tbody>
	</table>
	</form>
	
	<script type="text/javascript">
		function eliminar( id ){
			if( confirm("¿Desea eliminar la categoría?") ){
				window.location = "<?php echo site_url("admin/categorias/eliminar/") ?>/"+id
			}
		}
	</script>
</section>