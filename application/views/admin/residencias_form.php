<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Residencias</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Residencias</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->

          <!-- Inicia formulario de temas -->
          <?php
          /*
          * Mensajes
          */
          _print_messages();
          ?>
            <div class="col-lg-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">
                                <?php echo ($accion=="Agregar")?"Agregar Residencia":"Editar Residencias " ?>
                                </a> </li>
                            </ul>
                            <br>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-body">
                                    
                                        <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                                            <div class="form-group row">
                                                <label class="col-md-3">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Nombre de la residencia" value="<?php echo $fields['nombre']['value'] ?>" name="nombre" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3">Latitud</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="latitud" placeholder="Mueva el marcador del mapa" value="<?php echo $fields['latitud']['value'] ?>" name="latitud" class="form-control form-control-line" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3">Longitud</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="longitud" placeholder="Mueva el marcador del mapa" value="<?php echo $fields['longitud']['value'] ?>" name="longitud" class="form-control form-control-line" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                       <div id="map" style="height: 40vh;"></div>
                                            </div>
                                            </br>
                                            </br>
                                            

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                                    <button type="button" class="btn btn-inverse" onclick="window.location = '<?php echo site_url("miadmin/residencias") ?>'">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>


<script src="https://maps.google.com/maps/api/js?key=AIzaSyCanuwN4qVPGJTX81JDGPtSrwA1Y6B9IPg&libraries=visualization" type="text/javascript"></script>

<script src="<?php echo base_url("admin/js/maps.js") ?>" type="text/javascript"></script>
<script type="text/javascript">

  maps.initMap();


  
</script>
