<div class="page-wrapper">
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"> Conversación</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Mensajes</li>
            </ol>
        </div>
    </div>

    <div class="container-fluid">
    <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-content">
                                   
                                    <div class="inbox">

                                        <div class="m-t-10 m-b-20" role="toolbar">
                                            <div class="btn-group">
                                                <a href="../../../miadmin/chats" class="btn btn-danger btn-block waves-effect waves-light">
                                                   <i class="fa fa-reply-all" aria-hidden="true"></i>
                                                   Mensajes
                                                   <i class="fa fa-envelope" aria-hidden="true"></i> 
                                                </a>
                                             </div>
                                        </div>                            
                                

                                        <div class="mt-4" id="cajamensaje" style="height: 60vh;  overflow: scroll;">
                                            <hr/>
                                            
                                            <?php
                                              if( isset($chats) && count($chats)>0 ):
                                              $user = $usuario[0];
                                              foreach ($chats as $key => $item):
                                              if( $item->id_usuario == -1 ):
                                            ?>
                                            <div class="media mb-4 mt-1">
                                                <i class="fa fa-user-circle fa-2x" aria-hidden="true" style="padding-right: 10px;"></i>
                                                <div class="media-body">
                                                    <span class="pull-right">
                                                            <?php $date=date_create($item->fecha); 
                                                                   echo date_format($date,"F j, Y, g:i a");
                                                            ?>     
                                                    </span>
                                                      <h6 class="m-0" style="color: orange;"> Administrador</h6>
                                                      <small class="text-muted">De:  Administrador</small>
                                                    
                                                </div>
                                            </div>

                                            <p > 
                                               <?php echo $item->mensaje ?>
                                            </p>
                                            <hr/>

                                            <?php 
                                              endif;
                                              if( $item->id_usuario != -1 ):
                                            ?>

                                            <div class="media mb-4 mt-1">
                                                <i class="fa fa-user-circle-o fa-2x" aria-hidden="true" style="padding-right: 10px;"></i>
                                                <div class="media-body">
                                                    <span class="pull-right">
                                                            <?php $date=date_create($item->fecha); 
                                                                   echo date_format($date,"F j, Y, g:i a");
                                                            ?>    
                                                    </span>
                                                    <h6 class="m-0"> <?php echo $user->usuario ?></h6>
                                                     <small class="text-muted">De:  <?php echo $user->email ?></small>
                                                     
                                                </div>
                                            </div>

                                            <p style="color: black;"> 
                                               <?php echo $item->mensaje ?>
                                            </p>
                                            <hr/>

                                            <?php 
                                              endif;
                                              endforeach;
                                              endif;
                                            ?>
                                        </div>
                                       
                                        <br>
                                        <div class="text-right">
                                           <a href="<?php  echo site_url("miadmin/chats/agregar/".$user->id_usuario) ?>">
                                            <button type="button" class="btn btn-primary waves-effect waves-light w-md m-b-30">
                                                  RESPONDER 
                                                  <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                            </button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    
</div>
<script>
     var target = document.getElementById("cajamensaje");
     var tam = target.scrollHeight;
     target.scrollTop = tam;
</script>
