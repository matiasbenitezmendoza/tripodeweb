<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Imagenes de Inicio </h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="inicio">Inicio </a></li>
                / Sliders
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-body">
                      <a href="<?php echo site_url("miadmin/sliders/agregar") ?>">
                        <button type="button" class="btn btn-info m-b-10 m-l-5 right">
                          <i class="fa fa-user-plus"></i> Agregar Imagen de inicio
                        </button>
                      </a>
                      <div class="table-responsive m-t-40">
                          <table id="myTable" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Descripcion</th>
                                      <th>Eliminar</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                if( isset($sliders) && count($sliders)>0 ):
                                  foreach ($sliders as $key => $item):
                                   ?>
                                    <tr>
                                        <td>
                                          <?php if( $item->imagen != "" ): ?>
                                            <div >
                                              <a href="<?php echo site_url("miadmin/sliders/editar/".$item->id) ?>"><img width="80px" src="<?php echo base_url("imgs/sliders/".$item->imagen) ?>"  ></a>
                                            </div>
                                          <?php endif; ?>
                                        </td>
                                        <td><a href="<?php echo site_url("miadmin/sliders/editar/".$item->id) ?>"><i class="fa fa-pencil" style="font-size:24px"></i> &nbsp;&nbsp;<?php echo $item->descripcion ?></a></td>
                                        <td>
                                          <button class="btn btn-danger btn" onclick="return _delete(<?php echo $item->id ?>)"><i class="fa fa-close"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                  endforeach;
                                endif;
                               ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer> -->
    <!-- End footer -->
</div>
<script type="text/javascript">
  function _delete( entrada ){
    swal({
      title: "¿Deseas eliminar el registro?",
      text: "La información ya no estará disponible",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnClickOutside: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if( isConfirm ){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url("miadmin/sliders/eliminar") ?>",
            dataType: "json",
            data: {"entrada": entrada},
            complete: function(data) {
              var v_titulo = '¡Eliminación';
              var v_texto = 'No se pudo eliminar el registro, intentelo más tarde';
              var v_type = 'warning';
              if(data.responseText != ''){
                var obj = JSON.parse(data.responseText);
                v_titulo = obj.titulo;
                v_texto = obj.texto;
                v_type = obj.type;
              }
              swal({
                title: v_titulo,
                text: v_texto,
                type: v_type,
                confirmButtonText: "Ok",
                 closeOnClickOutside: false,
                closeOnConfirm: false,
              }, function(isConfirm) {
                if( isConfirm ){
                  window.location.reload();
                }
              });
            }
        });
      }
    });
    return true;
  }
</script>
