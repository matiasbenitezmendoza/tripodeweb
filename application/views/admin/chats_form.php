<div class="page-wrapper">
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"> Mensajes</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Mensajes</li>
            </ol>
        </div>
    </div>

    <div class="container-fluid">
            <div class="row">

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-content">
                                    <div class="inbox">
                                        <?php
                                              $user = $usuario[0];
                                        ?>
                                        <div class="" role="toolbar">
                                            <div class="btn-group">
                                                <a href="../../../miadmin/chats" class="btn btn-danger btn-block waves-effect waves-light">
                                                   <i class="fa fa-reply-all" aria-hidden="true"></i>
                                                   Mensajes
                                                   <i class="fa fa-envelope" aria-hidden="true"></i> 
                                                </a>
                                            </div>
                                            <div class="btn-group" >
                                                <a href="<?php  echo site_url("miadmin/chats/read/".$user->id_usuario) ?>" class="btn btn-danger waves-effect waves-light">
                                                   <i class="fa fa-reply-all" aria-hidden="true"></i>
                                                   Conversacion
                                                   <i class="fa fa-comments" aria-hidden="true"></i> 
                                                </a>
                                             </div>
                                        </div>

                                       
                                        <div class="mt-4">
                                        <?php
                                            _print_messages();
                                        ?>

                                        <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                                                <div class="form-group">
                                                        <label class="col-md-12">Para :</label>
                                                        <div class="col-md-12">
                                                              <input type="email" class="form-control"  value="<?php echo $user->usuario ?>" disabled>
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                        <label class="col-md-12">Mensaje :</label>
                                                        <div class="col-md-12">
                                                        <textarea name="mensaje" class="form-control" style="height:300px"></textarea>
                                                        </div>
                                                </div>

                                                <div class="form-group m-b-0">
                                                    <div class="text-right">
                                                        <button  type="submit" class="btn btn-primary waves-effect waves-light"> <span>Enviar</span> <i class="fa fa-send"></i> </button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <!-- end card-->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
                   
    </div>
    
</div>