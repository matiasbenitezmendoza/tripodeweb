<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="inicio">Inicio </a></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
   <!-- Container fluid  -->
   <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="card bg-warning p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                   <span><i class="fa fa-shopping-cart f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $total_pedidos ?></h2>
                                    <p class="m-b-0">Total de pedidos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card bg-info p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-user f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                   <h2><?php echo $total_clientes ?></h2>
                                    <p class="m-b-0">Total Clientes</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card bg-primary p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-envelope f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $total_chat ?></h2>
                                    <p class="m-b-0">Total de Mensajes nuevos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-map-marker f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $totales_pedidos_activos ?></h2>
                                    <p class="m-b-0">Pedidos Recibidos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-motorcycle f-s-60 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $totales_pedidos_ruta ?></h2>
                                    <p class="m-b-0">Pedidos en Ruta</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-archive f-s-60 color-warning"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $totales_pedidos_finalizados ?></h2>
                                    <p class="m-b-0">Pedidos Entregados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>

          
          
                <!-- TABLA PEDIDOS RECIENTES-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Pedidos recientes </h4>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                              <th>Orden</th>
                                              <th>Cliente</th>
                                              <th>Fecha</th>
                                              <th>Status</th>
                                              <th>Tipo Pago</th>
                                              <th>Status Pago</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                 <?php
                                 if( isset($pedidos) && count($pedidos)>0 ):
                                  foreach ($pedidos as $key => $item):
                                   ?>
                                    <tr>
                                        <td><?php echo ("#".$item->orden) ?></td>
                                        <td><?php echo $item->usuario ?></td>
                                        <td><?php echo $item->fecha ?></td>
                                        <td><?php 
                                                if($item->status_pedido == 1):
                                                    echo 'Pedido Recibido'; 
                                                endif;
                                                if($item->status_pedido == 2):
                                                    echo 'Saliendo a tu Direccion'; 
                                                endif;
                                                if($item->status_pedido == 3):
                                                    echo 'Pedido Entregado';
                                                endif;
                                                if($item->status_pedido == 4):
                                                    echo 'Devolución/Cancelación';
                                                endif;
                                                   ?>
                                        </td>
                                        <td><?php
                                                    if($item->tipo_pago == 1):
                                                        echo 'Tarjeta'; 
                                                    endif;
                                                    if($item->tipo_pago == 2):
                                                        echo 'Tarjeta en Domicilio'; 
                                                    endif;
                                                    if($item->tipo_pago == 3):
                                                        echo 'Efectivo'; 
                                                    endif;
                                        ?></td>      
                                        <td><?php 
                                                if($item->status_pago == 2):
                                                    echo 'Pendiente'; 
                                                endif;
                                                if($item->status_pago == 1):
                                                    echo 'Pagado'; 
                                                endif;
                                        ?></td>                                                                          
                                    </tr>
                                    <?php
                                  endforeach;
                                 endif;
                                 ?>
                                        </tbody>




                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
    <!-- End Container fluid  -->
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer> -->
    <!-- End footer -->
</div>