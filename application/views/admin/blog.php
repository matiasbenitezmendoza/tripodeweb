<script type="text/javascript" src="<?php echo base_url("admin/js/plugins/bootstrap-notify.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("admin/js/plugins/sweetalert.min.js") ?>"></script>
<div class="content-wrapper">

  <div class="page-title">
    <div>
      <h1><i class="fa fa-edit"></i> Blog</h1>
      <p>Contenido de tu blog</p>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li>Blog</li>
      </ul>
    </div>
  </div>

  <a class="btn btn-primary btn-xs pull-right" style="margin: 10px;" href="<?php echo site_url('miadmin/blog/agregar') ?>">+ Agregar</a>
  <!-- Contenido -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <table class="table table-hover table-bordered" id="dataTable">
            <thead>
              <tr>
                <th>Título</th>
                <th>Imagen</th>
                <th>Introducción</th>
                <th>Destacada</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if( isset($entradas) && is_array($entradas) && count($entradas)>0 ):
                foreach ($entradas as $key => $info):
              ?>
              <tr>
                <td><?php echo $info->titulo ?></td>
                <td>
                  <?php if( $info->imagen != '' ): ?>
                    <img src="<?php echo base_url("blog/".$info->imagen) ?>" height="150px" />
                  <?php endif; ?>
                </td>
                <td><?php echo $info->introduccion ?></td>
                <td><?php echo ($info->destacada==1)?'SI':'' ?></td>
                <td>
                  <a class="btn btn-info" href="<?php echo site_url('miadmin/blog/editar/'.$info->identrada) ?>">Editar</a>
                  <br><br>
                  <a class="btn btn-danger" href="javascript:void(0)" onclick="return _delete(<?php echo $info->identrada ?>)">Eliminar</a>
                </td>
              </tr>
              <?php
                endforeach;
              endif;
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /Contenido -->

</div>
<script type="text/javascript">
  $('#dataTable').DataTable();
  function _delete( entrada ){
    swal({
      title: "¿Deseas eliminar el contacto?",
      text: "La información ya no estará disponible",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnClickOutside: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if( isConfirm ){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url("miadmin/blog/eliminar") ?>",
            dataType: "json",
            data: {"entrada": entrada},
            complete: function(data) {
              var v_titulo = '¡Eliminación';
              var v_texto = 'No se pudo eliminar el registro, intentelo más tarde';
              var v_type = 'warning';
              if(data.responseText != ''){
                var obj = JSON.parse(data.responseText);
                v_titulo = obj.titulo;
                v_texto = obj.texto;
                v_type = obj.type;
              }
              swal({
                title: v_titulo,
                text: v_texto,
                type: v_type,
                confirmButtonText: "Ok",
                 closeOnClickOutside: false,
                closeOnConfirm: false,
              }, function(isConfirm) {
                if( isConfirm ){
                  window.location.reload();
                }
              });
            }
        });
      }
    });
    return true;
  }
</script>
