<link rel="stylesheet" href="css/lib/html5-editor/bootstrap-wysihtml5.css" />
  <style type="text/css">
    .wysihtml5-toolbar li{display: inline-block;}
  </style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Noticias</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Noticias</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
          <?php
          /*
          * Mensajes
          */
          _print_messages();
          ?>
          <div class="col-lg-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Información</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                                            <div class="form-group">
                                                <label class="col-md-12">* Titulo</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="" value="<?php echo $fields['titulo']['value'] ?>" name="titulo" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Imagen</label>
                                                <div class="col-md-12">
                                                    <input type="file" name="imagen" class="form-control form-control-line">
                                                    <?php if( $fields['imagen']['value'] != "" ): ?>
                                                      <br><img src="<?php echo base_url("imgs/noticias/".$fields['imagen']['value']) ?>" alt="<?php echo $fields['titulo']['value'] ?>" width="100px" />
                                                    <?php endif ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">* Fecha de publicación</label>
                                                <div class="col-md-12">
                                                    <input type="date" value="<?php echo $fields['fecha_publicacion']['value'] ?>" name="fecha_publicacion" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">* Descripción</label>
                                                <div class="col-md-12">
                                                    <textarea class="form-control" rows="5" name="descripcion" placeholder="Ingrese la descripción ..." style="height:150px"><?php echo $fields['descripcion']['value'] ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">* Contenido</label>
                                                <textarea class="textarea_editor form-control" rows="15" name="contenido" placeholder="Ingrese el contenido ..." style="height:450px"><?php echo $fields['contenido']['value'] ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                                    <button type="button" class="btn btn-inverse" onclick="window.location = '<?php echo site_url("miadmin/noticias") ?>'">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>

<script src="<?php echo base_url("admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/html5-editor/wysihtml5-0.3.0.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/html5-editor/bootstrap-wysihtml5.js") ?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $('.textarea_editor').wysihtml5();
  });
</script>
