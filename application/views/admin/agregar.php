<section id="administracion">
    <h1 id="title_adminInt">Promociones</h1>
		<h1 id="title_lightbox"><?php echo (isset($id))?'EDITAR':'AGREGAR' ?> FIESTA</h1>
		<div class="txt_center">
            <?php
            /*
            * Mensajes
            */
            echo validation_errors('<div class="error">', '</div>');
        
            $all = $this->messages->get();
            foreach($all as $type=>$messages)
                foreach($messages as $message)
                    echo '<div class="'.$type.'">'.$message.'</div>'; 
            ?>
            <?php echo form_open_multipart('','name="formInfo"') ?>
				<input type="text" placeholder="Titulo de la fiesta" class="txt_inputAdmin" name="nombre" value="">
				<br>
                <input type="text" placeholder="Descripción de la fiesta" class="txt_inputAdmin" name="descripcion" value="">

				<div class="txt_center">
					<a href="<?php echo site_url("admin") ?>" class="btn_blackBorder closeBtn" >Cancelar</a>
					<input type="submit" value="<?php echo (isset($id))?'editar':'agregar' ?>" class="btn_blackBorder">
				</div>
			</form>
		</div>
</section>