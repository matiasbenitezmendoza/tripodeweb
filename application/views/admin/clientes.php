<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Clientes</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="inicio">Inicio </a></li>
                / Clientes
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-body">
                      
                      <div class="table-responsive m-t-40">
                          <table id="myTable" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>Nombre</th>
                                      <th>Apellido Paterno</th>
                                      <th>Apellido Materno</th>
                                      <th>Correo</th>
                                      <th>Telefono</th>
                                      <th>Fecha de Nacimiento</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                if( isset($usuarios) && count($usuarios)>0 ):
                                  foreach ($usuarios as $key => $item):
                                   ?>
                                    <tr>
              
                                        <td><?php echo $item->nombre ?></td>
                                        <td><?php echo $item->apellido_paterno ?></td>
                                        <td><?php echo $item->apellido_materno ?></td>
                                        <td><?php echo $item->email ?></td>
                                        <td><?php echo $item->telefono ?></td>
                                        <td><?php echo $item->fecha_nacimiento ?></td>

                                    </tr>
                                    <?php
                                  endforeach;
                                endif;
                               ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer> -->
    <!-- End footer -->
</div>
