<script src="https://cdn.ckeditor.com/4.7.3/standard-all/ckeditor.js"></script>
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-edit"></i> SEO</h1>
      <p>Posicionamiento de tu sitio web</p>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li><a href="<?php echo site_url('miadmin/seo') ?>">SEO</a></li>
        <li><?php echo mb_strtoupper($fields['pagina']['value']) ?></li>
      </ul>
    </div>
  </div>
  <!-- /Titulo de la página -->

  <?php _print_messages(); ?>

  <!-- Contenido -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <h3 class="card-title"><?php echo $accion ?></h3>
        <div class="card-body">
          <form method="post" action="<?php echo site_url('miadmin/seo/'.mb_strtolower($accion).'/'.$pagina) ?>" enctype="multipart/form-data">
            <div class="form-group">
              <label class="control-label">Página</label>
              <input class="form-control" type="text" placeholder="" disabled value="<?php echo $fields['pagina']['value'] ?>" >
              <input type="hidden" value="<?php echo $fields['pagina']['value'] ?>" name="pagina" >
            </div>
            <div class="form-group">
              <label class="control-label">Título</label>
              <input class="form-control" type="text" placeholder="Ingresa el título de la página" name="titulo" value="<?php echo $fields['titulo']['value'] ?>" />
            </div>
            <div class="form-group">
              <label class="control-label">Keywords - SEO</label>
              <textarea class="form-control" rows="4" name="keywords" placeholder="Ingresa las 'keywords' de la entrada"><?php echo $fields['keywords']['value'] ?></textarea>
            </div>
            <div class="form-group">
              <label class="control-label">Description - SEO</label>
              <textarea class="form-control" rows="4" name="description" placeholder="Ingresa la 'description' de la entrada"><?php echo $fields['description']['value'] ?></textarea>
            </div>
            <div class="card-footer">
              <button class="btn btn-primary icon-btn" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Guardar</button>
              &nbsp;&nbsp;&nbsp;
              <a class="btn btn-default icon-btn" href="<?php echo site_url('miadmin/seo') ?>"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancelar</a>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
