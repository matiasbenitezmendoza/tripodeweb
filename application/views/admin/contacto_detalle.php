<script type="text/javascript" src="<?php echo base_url("admin/js/plugins/bootstrap-notify.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("admin/js/plugins/sweetalert.min.js") ?>"></script>
<div class="content-wrapper">
  <!-- Titulo de la página -->
  <div class="page-title">
    <div>
      <h1><i class="fa fa-edit"></i> Contactos</h1>
      <p>Contactos recibidos del sitio web</p>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li><a href="<?php echo site_url("miadmin/contactos") ?>">Contactos</a></li>
        <li>Detalle</li>
      </ul>
    </div>
  </div>
  <!-- /Titulo de la página -->

  <?php _print_messages(); ?>

  <!-- Contenido -->
  <div class="bs-element-section card row">
    <?php if( is_object($contacto) && isset($contacto->nombre) ): ?>
    <div class="col-lg-12">
      <div class="bs-component">
        <div class="panel panel-default">
          <div class="panel-heading"><?php echo $contacto->nombre ?></div>
          <div class="panel-body">
            <b>Teléfono:</b> <?php echo $contacto->telefono ?><br><br>
            <b>Email:</b> <?php echo $contacto->email ?><br><br>
            <b>Comentario:</b> <?php echo $contacto->comentario ?><br><br>
            <b>Fecha:</b> <?php echo date('d-m-Y H:i:s',strtotime($contacto->fecha)) ?><br><br>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <a class="btn btn-primary btn-xs" href="<?php echo site_url('miadmin/contactos') ?>">Regresar</a>
  </div>
  <!-- /Contenido -->

</div>
