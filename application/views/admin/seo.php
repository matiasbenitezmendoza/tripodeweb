<div class="content-wrapper">

  <div class="page-title">
    <div>
      <h1><i class="fa fa-edit"></i> SEO</h1>
      <p>Posicionamiento de tu sitio web</p>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li><a href="#">SEO</a></li>
      </ul>
    </div>
  </div>

  <div class="col-md-12">
    <div class="card">
      <h3 class="card-title">Table Hover</h3>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Página</th>
            <th>Título</th>
            <th>Keywords</th>
            <th>Description</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if( is_array($seo) && count($seo)>0 ):
            foreach( $seo as $key => $info ):
          ?>
            <tr>
              <td><?php echo mb_strtoupper($info->pagina) ?></td>
              <td><?php echo $info->titulo ?></td>
              <td><?php echo $info->keywords ?></td>
              <td><?php echo $info->description ?></td>
              <td>
                <a class="btn btn-info" href="<?php echo site_url('miadmin/seo/editar/'.$info->pagina) ?>">Editar</a>
              </td>
            </tr>
          <?php
            endforeach;
          endif;
          ?>
        </tbody>
      </table>
    </div>
  </div>

</div>
