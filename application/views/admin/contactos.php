<script type="text/javascript" src="<?php echo base_url("admin/js/plugins/bootstrap-notify.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("admin/js/plugins/sweetalert.min.js") ?>"></script>
<div class="content-wrapper">
  <!-- Titulo de la página -->
  <div class="page-title">
    <div>
      <h1><i class="fa fa-edit"></i> Contactos</h1>
      <p>Contactos recibidos del sitio web</p>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><i class="fa fa-home fa-lg"></i></li>
        <li><a href="#">Contactos</a></li>
      </ul>
    </div>
  </div>
  <!-- /Titulo de la página -->

  <!-- Contenido -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <table class="table table-hover table-bordered" id="dataTable">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Email</th>
                <th>Comentario</th>
                <th>Fecha</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if( isset($contactos) && is_array($contactos) && count($contactos)>0 ):
                foreach ($contactos as $key => $info):
              ?>
              <tr>
                <td><?php echo $info->nombre ?></td>
                <td><?php echo $info->telefono ?></td>
                <td><?php echo $info->email ?></td>
                <td><?php echo $info->comentario ?></td>
                <td><?php echo date('d-m-Y - H:i:s',strtotime($info->fecha)) ?></td>
                <td>
                  <a class="btn btn-info" id="demoNotify" href="<?php echo site_url('miadmin/contactos/detalle/'.$info->idcontacto) ?>">Ver</a>
                  <br><br>
                  <a class="btn btn-danger" href="javascript:void(0)" onclick="return _delete(<?php echo $info->idcontacto ?>)">Eliminar</a>
                </td>
              </tr>
              <?php
                endforeach;
              endif;
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /Contenido -->

</div>
<script type="text/javascript">
  $('#dataTable').DataTable();
  function _delete(entrada){
    swal({
      title: "¿Deseas eliminar el contacto?",
      text: "La información ya no estará disponible",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnClickOutside: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if( isConfirm ){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url("miadmin/contactos/eliminar") ?>",
            dataType: "json",
            data: {"entrada": entrada},
            complete: function(data) {
              var v_titulo = '¡Eliminación';
              var v_texto = 'No se pudo eliminar el registro, intentelo más tarde';
              var v_type = 'warning';
              if(data.responseText != ''){
                var obj = JSON.parse(data.responseText);
                v_titulo = obj.titulo;
                v_texto = obj.texto;
                v_type = obj.type;
              }
              swal({
                title: v_titulo,
                text: v_texto,
                type: v_type,
                confirmButtonText: "Ok",
                 closeOnClickOutside: false,
                closeOnConfirm: false,
              }, function(isConfirm) {
                if( isConfirm ){
                  window.location.reload();
                }
              });
            }
        });
      }
    });
    return true;
  }
</script>
