<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Imagenes de Inicio</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Sliders</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
          <?php if( $accion != "Agregar" ): ?>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <?php if( $fields['imagen']['value'] != "" ): ?>
                              <header>
                                  <div class="avatar">
                                      <img src="<?php echo base_url("imgs/sliders/".$fields['imagen']['value']) ?>"  />
                                  </div>
                              </header>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
          <?php endif; ?>
          <!-- Inicia formulario de temas -->
          <?php
          /*
          * Mensajes
          */
          _print_messages();
          ?>
          <div class="col-lg-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Información</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                   
                                            <div class="form-group">
                                                <label class="col-md-12">* Descripcion</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="" value="<?php echo $fields['descripcion']['value'] ?>" name="descripcion" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Imagen</label>
                                                <div class="col-md-12">
                                                    <input type="file" name="imagen" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                                    <button type="button" class="btn btn-inverse" onclick="window.location = '<?php echo site_url("miadmin/sliders") ?>'">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
