<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Categorías</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Categorías</li>
                <li class="breadcrumb-item active"><?php echo $accion ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
              <?php
              /*
              * Mensajes
              */
              _print_messages();
              ?>
              <div class="card card-outline-info">
                  <div class="card-header">
                      <h4 class="m-b-0 text-white"><?php echo ($accion=="Agregar")?"Agregar categoría":"" ?></h4>
                  </div>
                  <div class="card-body m-t-15">
                      <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                          <div class="form-body">
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Nombre</label>
                                  <div class="col-md-9">
                                      <input type="text" placeholder="" name="nombre" class="form-control" value="<?php echo $fields['nombre']['value'] ?>">
                                    </div>
                              </div>
                          </div>
                          <div class="form-actions">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="row">
                                          <div class="offset-sm-3 col-md-9">
                                              <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                              <button type="button" class="btn btn-inverse" onclick="window.location = '<?php echo site_url("miadmin/categorias") ?>'">Cancel</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer> -->
    <!-- End footer -->
</div>
