<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Compras</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Compras</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-body">
                      <div class="table-responsive m-t-40">
                          <table id="myTable" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>Fecha</th>
                                      <th>Comprador</th>
                                      <th>Producto</th>
                                      <th>Talento</th>
                                      <th>Estatus</th>
                                      <th>Pago</th>
                                      <th>Metodó de pago</th>
                                      <th>Fecha de aprovación</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                if( isset($compras) && is_array($compras) && count($compras)>0 ):
                                  foreach ($compras as $key => $compra):
                                   ?>
                                    <tr>
                                        <td>
                                          <a href="<?php echo site_url("miadmin/".$page."/editar/".$compra->idcompra) ?>">
                                            <i class="fa fa-pencil" style="font-size:24px"></i> &nbsp;&nbsp;<?php echo $compra->fecha ?>
                                          </a>
                                        </td>
                                        <td><?php echo $compra->first_name." ".$compra->last_name ?></td>
                                        <td><?php echo $compra->producto ?></td>
                                        <td><?php echo $compra->talento ?></td>
                                        <td>
                                          <span class="badge badge-<?php echo ($compra->estatus=='pagar')?'danger':'warning' ?>"><?php echo $compra->estatus ?></span>
                                        </td>
                                        <td>
                                          <?php
                                          $detalle = json_decode($compra->detalle);
                                          if( isset($detalle->status) && isset($detalle->status_detail) ){
                                            ?>
                                            <span class="badge badge-<?php echo ($detalle->status=='approved')?'success':'warning' ?>"><?php echo $detalle->status." - ".$detalle->status_detail ?></span>
                                            <?php
                                          }
                                          ?>
                                        </td>
                                        <td>
                                          <?php
                                          if( isset($detalle->payment_type_id) && isset($detalle->payment_method_id) ){
                                            echo $detalle->payment_type_id." - ".$detalle->payment_method_id;
                                          }
                                          ?>
                                        </td>
                                        <td>
                                          <?php
                                          if( isset($detalle->date_approved) ){
                                            echo date("d-m-Y",strtotime($detalle->date_approved));
                                          }
                                          ?>
                                        </td>
                                    </tr>
                                    <?php
                                  endforeach;
                                endif;
                               ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer> -->
    <!-- End footer -->
</div>
<script type="text/javascript">
  function _delete( entrada ){
    swal({
      title: "¿Deseas eliminar la compra?",
      text: "La información ya no estará disponible",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Si",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnClickOutside: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if( isConfirm ){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url("miadmin/$page/eliminar") ?>",
            dataType: "json",
            data: {"entrada": entrada},
            complete: function(data) {
              var v_titulo = '¡Eliminación';
              var v_texto = 'No se pudo eliminar el registro, intentelo más tarde';
              var v_type = 'warning';
              if(data.responseText != ''){
                var obj = JSON.parse(data.responseText);
                v_titulo = obj.titulo;
                v_texto = obj.texto;
                v_type = obj.type;
              }
              swal({
                title: v_titulo,
                text: v_texto,
                type: v_type,
                confirmButtonText: "Ok",
                 closeOnClickOutside: false,
                closeOnConfirm: false,
              }, function(isConfirm) {
                if( isConfirm ){
                  window.location.reload();
                }
              });
            }
        });
      }
    });
    return true;
  }
</script>
