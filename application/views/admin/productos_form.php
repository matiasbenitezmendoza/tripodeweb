<link rel="stylesheet" href="css/lib/html5-editor/bootstrap-wysihtml5.css" />
<style type="text/css">
  .wysihtml5-toolbar li{display: inline-block;}
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Productos</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Productos</li>
                <li class="breadcrumb-item active"><?php echo $accion ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
          <?php
          /*
          * Mensajes
          */
          _print_messages();
          ?>
            <div class="col-12">
              <div class="card card-outline-info">
                  <div class="card-header">
                      <h4 class="m-b-0 text-white"><?php echo ($accion=="Agregar")?"Agregar producto":"Editar - ".$fields['nombre']['value'] ?></h4>
                  </div>
                  <div class="card-body m-t-15">
                      <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                          <div class="form-body">
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Nombre</label>
                                  <div class="col-md-9">
                                    <input type="text" placeholder="Nombre del producto" name="nombre" value="<?php echo $fields['nombre']['value'] ?>" class="form-control">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Precio $</label>
                                  <div class="col-md-9">
                                      <input type="text" placeholder="100" name="precio" value="<?php echo $fields['precio']['value'] ?>" class="form-control">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Costo de envio $</label>
                                  <div class="col-md-9">
                                      <input type="text" placeholder="100" name="envio" value="<?php echo $fields['envio']['value'] ?>" class="form-control">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Stock</label>
                                  <div class="col-md-9">
                                    <!--Name = columna DB -->
                                      <input type="text" placeholder="100" name="stock" value="<?php echo $fields['stock']['value'] ?>" class="form-control">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Talento</label>
                                  <div class="col-md-9">
                                      <select class="form-control" name="talento">
                                        <option value="">Elija una opción</option>
                                        <?php
                                        if( isset($talentos) && is_array($talentos) && count($talentos)>0 ):
                                          foreach ($talentos as $key => $talento):
                                            $selected = ($fields['talento']['value']==$talento->idtalento)?'selected':'';
                                           ?>
                                            <option value="<?php echo $talento->idtalento ?>" <?php echo $selected ?>><?php echo $talento->nombre ?></option>
                                            <?php
                                          endforeach;
                                        endif;
                                       ?>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Imagen 1</label>
                                  <div class="col-md-9">
                                      <input type="file" class="form-control" name="imagen">
                                      <?php if( $fields['imagen']['value'] != "" ): ?>
                                        <br><img src="<?php echo base_url("imgs/productos/".$fields['imagen']['value']) ?>" alt="<?php echo $fields['nombre']['value'] ?> imagen 1" border="0" width="50px;">
                                      <?php endif; ?>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Imagen 2</label>
                                  <div class="col-md-9">
                                    <input type="file" class="form-control" name="imagenb">
                                    <?php if( $fields['imagenb']['value'] != "" ): ?>
                                      <br><img src="<?php echo base_url("imgs/productos/".$fields['imagenb']['value']) ?>" alt="<?php echo $fields['nombre']['value'] ?> imagen 2" border="0" width="50px;">
                                    <?php endif; ?>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Imagen 3</label>
                                  <div class="col-md-9">
                                    <input type="file" class="form-control" name="imagenc">
                                    <?php if( $fields['imagenc']['value'] != "" ): ?>
                                      <br><img src="<?php echo base_url("imgs/productos/".$fields['imagenc']['value']) ?>" alt="<?php echo $fields['nombre']['value'] ?> imagen 3" border="0" width="50px;">
                                    <?php endif; ?>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Imagen 4</label>
                                  <div class="col-md-9">
                                    <input type="file" class="form-control" name="imagend">
                                    <?php if( $fields['imagend']['value'] != "" ): ?>
                                      <br><img src="<?php echo base_url("imgs/productos/".$fields['imagend']['value']) ?>" alt="<?php echo $fields['nombre']['value'] ?> imagen 4" border="0" width="50px;">
                                    <?php endif; ?>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Imagen 5</label>
                                  <div class="col-md-9">
                                    <input type="file" class="form-control" name="imagene">
                                    <?php if( $fields['imagene']['value'] != "" ): ?>
                                      <br><img src="<?php echo base_url("imgs/productos/".$fields['imagene']['value']) ?>" alt="<?php echo $fields['nombre']['value'] ?> imagen 5" border="0" width="50px;">
                                    <?php endif; ?>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Categorias</label>
                                  <div class="col-md-9">
                                      <select class="form-control" multiple="true" name="categorias">
                                        <?php
                                        if( isset($categorias) && is_array($categorias) && count($categorias)>0 ):
                                          foreach ($categorias as $key => $categoria):
                                            $selected = ($fields['talento']['value']==$categoria->idcategoria)?'selected':'';
                                           ?>
                                            <option value="<?php echo $categoria->slug ?>"><?php echo $categoria->nombre ?></option>
                                            <?php
                                          endforeach;
                                        endif;
                                       ?>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Descripción corta</label>
                                  <div class="col-md-9">
                                      <textarea class="form-control" rows="5" name="descripcion_corta" placeholder="Ingrese la descripción ..." style="height:150px"><?php echo $fields['descripcion_corta']['value'] ?></textarea>
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="control-label text-right col-md-3">Descripción larga</label>
                                  <div class="col-md-9">
                                      <textarea class="textarea_editor form-control" rows="15" name="descripcion_larga" placeholder="Ingrese el contenido ..." style="height:450px"><?php echo $fields['descripcion_larga']['value'] ?></textarea>
                                  </div>
                              </div>
                          </div>
                          <div class="form-actions">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="row">
                                          <div class="offset-sm-3 col-md-9">
                                              <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                              <button type="button" class="btn btn-inverse">Cancel</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
<script src="<?php echo base_url("admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/html5-editor/wysihtml5-0.3.0.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/html5-editor/bootstrap-wysihtml5.js") ?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $('.textarea_editor').wysihtml5();
  });
</script>
