<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Pedidos</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="inicio">Inicio </a></li>
                / Pedidos
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-body">
 
                      <div class="table-responsive m-t-40">
                          <table id="myTable" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>Orden</th>
                                      <th>Cliente</th>
                                      <th>Fecha</th>
                                      <th>Status</th>
                                      <th>Tipo Pago</th>
                                      <th>Status Pago</th>

                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                if( isset($pedidos) && count($pedidos)>0 ):
                                  foreach ($pedidos as $key => $item):
                                   ?>
                                    <tr>
                                        <td><a href="<?php echo site_url("miadmin/pedidos/editar/".$item->id) ?>"><i class="fa fa-pencil" style="font-size:24px"></i> &nbsp;&nbsp;<?php echo ("#".$item->orden) ?></a></td>
                                        <td><?php echo $item->usuario ?></td>
                                        <td><?php echo $item->fecha ?></td>
                                        <td><?php 
                                                    if($item->status_pedido == 1):
                                                        echo 'Pedido Recibido'; 
                                                    endif;
                                                    if($item->status_pedido == 2):
                                                        echo 'Saliendo a tu Direccion'; 
                                                    endif;
                                                    if($item->status_pedido == 3):
                                                        echo 'Pedido Entregado';
                                                    endif;
                                                    if($item->status_pedido == 4):
                                                        echo 'Devolución/Cancelación';
                                                    endif;
                                                    ?>
                                        </td>
                                        <td><?php
                                                    if($item->tipo_pago == 1):
                                                        echo 'Tarjeta'; 
                                                    endif;
                                                    if($item->tipo_pago == 2):
                                                        echo 'Tarjeta en Domicilio'; 
                                                    endif;
                                                    if($item->tipo_pago == 3):
                                                        echo 'Efectivo'; 
                                                    endif;
                                        ?></td>      
                                        <td><?php 
                                                if($item->status_pago == 2):
                                                    echo 'Pendiente'; 
                                                endif;
                                                if($item->status_pago == 1):
                                                    echo 'Pagado'; 
                                                endif;
                                        ?></td>                                                                                  
                                    </tr>
                                    <?php
                                  endforeach;
                                endif;
                               ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer> -->
    <!-- End footer -->
</div>

