<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
             <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                  <div class="col-md-4">
                      <div class="card bg-warning p-30">
                          <div class="media">
                              <div class="media-left meida media-middle">
                                  <span><i class="fa fa-usd f-s-60"></i></span>
                              </div>
                              <div class="media-body media-text-right">
                                  <h2><?php echo number_format($totales['total'],2) ?></h2>
                                  <p class="m-b-0">Total de pedidos</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <?php if($this->auth->usertype() == "Administrador"): ?>
                    <div class="col-md-4">
                        <div class="card bg-info p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-usd f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo number_format($totales['total']-$totales['fee_mp'],2) ?></h2>
                                    <p class="m-b-0">Total menos comisión</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card bg-primary p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-usd f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo number_format($totales['total']-$totales['fee_mp']-$totales['pagado'],2) ?></h2>
                                    <p class="m-b-0">Total menos pagos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  <?php else: ?>
                    <div class="col-md-4">
                        <div class="card bg-info p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-usd f-s-60"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo number_format($totales['pagado'],2) ?></h2>
                                    <p class="m-b-0">Total cobrado</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-shopping-cart f-s-60 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo number_format($compras_total,0) ?></h2>
                                    <p class="m-b-0">Ventas</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-archive f-s-60 color-warning"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo number_format($productos_total,0) ?></h2>
                                    <p class="m-b-0">Productos vendidos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-user f-s-60 color-danger"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo number_format($clientes_total,0) ?></h2>
                                    <p class="m-b-0">Clientes</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row bg-white m-l-0 m-r-0 box-shadow ">

                    <!-- column -->
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Ventas</h4>
                                <div id="extra-area-chart"></div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->

                    <!-- column -->
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body browser">
                              <h4 class="card-title">Productos más vendidos</h4>
                             <?php
                              /*$colors = array("bg-danger","bg-info","bg-success","bg-warning","bg-success");
                              for( $i=0; count($productos)>0 && $i <= 5; $i++ ):
                                if( isset($productos[$i]->total_compras) ):
                                  $porcentaje = round(($productos[$i]->total_compras*100)/$compras_total);
                              ?> 
                              <p class="f-w-600"><?php echo $productos[$i]->nombre." ".(($this->auth->usertype() == "Administrador")?' - '.$productos[$i]->talento:'') ?> <span class="pull-right"><?php echo $porcentaje ?>%</span></p>
                              <div class="progress ">
                                  <div role="progressbar" style="width: <?php echo $porcentaje ?>%; height:8px;" class="progress-bar <?php echo $colors[$i] ?> wow animated progress-animated"></div>
                              </div>
                              <?php
                                endif;
                              endfor; */?>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Pedidos recientes </h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                              <th>Fecha</th>
                                              <th>Comprador</th>
                                              <th>Producto</th>
                                              <th>Talento</th>
                                              <th>Estatus</th>
                                              <th>Pago</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                        /*  if( isset($compras) && is_array($compras) && count($compras)>0 ):
                                            foreach ($compras as $key => $compra):
                                             ?>
                                              <tr>
                                                  <td>
                                                    <a href="<?php echo site_url("miadmin/".$page."/editar/".$compra->idcompra) ?>">
                                                      <i class="fa fa-pencil" style="font-size:24px"></i> &nbsp;&nbsp;<?php echo $compra->fecha ?>
                                                    </a>
                                                  </td>
                                                  <td><?php echo $compra->first_name." ".$compra->last_name ?></td>
                                                  <td><?php echo $compra->producto ?></td>
                                                  <td><?php echo $compra->talento ?></td>
                                                  <td>
                                                    <span class="badge badge-<?php echo ($compra->estatus=='pagar')?'danger':'warning' ?>"><?php echo $compra->estatus ?></span>
                                                  </td>
                                                  <td>
                                                    <?php
                                                    $detalle = json_decode($compra->detalle);
                                                    if( isset($detalle->status) && isset($detalle->status_detail) ){
                                                      ?>
                                                      <span class="badge badge-<?php echo ($detalle->status=='approved')?'success':'warning' ?>"><?php echo $detalle->status." - ".$detalle->status_detail ?></span>
                                                      <?php
                                                    }
                                                    ?>
                                                  </td>
                                              </tr>
                                              <?php
                                            endforeach;
                                          endif;
                                         */?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
    <!-- End Container fluid  -->
</div>
<!-- All Jquery -->
<script src="<?php echo base_url("js/lib/jquery/jquery.min.js") ?>"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url("js/lib/bootstrap/js/popper.min.js") ?>"></script>
<script src="<?php echo base_url("js/lib/bootstrap/js/bootstrap.min.js") ?>"></script>
<!-- <script src="<?php echo base_url("js/jquery.slimscroll.js") ?>"></script>
<script src="<?php echo base_url("js/sidebarmenu.js") ?>"></script> -->
<!--stickey kit -->
<script src="<?php echo base_url("js/lib/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>
<!--Custom JavaScript -->
<!-- Amchart -->
<script src="<?php echo base_url("js/lib/morris-chart/raphael-min.js") ?>"></script>
<script src="<?php echo base_url("js/lib/morris-chart/morris.js") ?>"></script>
<script type="text/javascript">
  $(function(){
  	"use strict";
  	Morris.Area({
  		element: 'extra-area-chart',
  		data: [{
  				period: '<?php echo date('Y') ?> Q1',
  				ventas: <?php echo $totales_q["1"] ?>
          }, {
  				period: '<?php echo date('Y') ?> Q2',
  				ventas: <?php echo $totales_q["2"] ?>
          }, {
  				period: '<?php echo date('Y') ?> Q3',
  				ventas: <?php echo $totales_q["3"] ?>
          }, {
  				period: '<?php echo date('Y') ?> Q4',
  				ventas: <?php echo $totales_q["4"] ?>
          }
      ],
  		lineColors: [ '#FAD9AA' ],
  		xkey: 'period',
  		ykeys: [ 'ventas' ],
  		labels: [ 'ventas' ],
  		pointSize: 0,
  		lineWidth: 3,
  		resize: true,
  		fillOpacity: 0.9,
  		behaveLikeLine: true,
  		gridLineColor: '#e0e0e0',
      pointStrokeColors: ['black'],
  		hideHover: 'auto'
  	});
  });
</script>
