<div class="page-wrapper">
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"> Mensajes</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Mensajes</li>
            </ol>
        </div>
    </div>

    <div class="container-fluid">
                                      <!--
  
    <div class="row">
            <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-content">
                                

                                    <div class="inbox">

                                        <div role="toolbar" class="">
                                            <div class="btn-group">
                                                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-light dropdown-toggle waves-effect" type="button">
                                                                                    Opciones
                                                                                    <span class="caret m-l-5"></span>
                                                                                </button>
                                                <div class="dropdown-menu">
                                                    <span class="dropdown-header">Mostrar :</span>
                                                    <a href="<?php echo site_url("miadmin/chats") ?>" class="dropdown-item">Todos</a>
                                                    <a href="<?php echo site_url("miadmin/chats/search/1") ?>" class="dropdown-item">Vistos</a>
                                                    <a href="<?php echo site_url("miadmin/chats/search/2") ?>" class="dropdown-item">No vistos</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="">
                                            <div class="mt-4">
                                                <div class="">
                                                    <ul class="message-list">


                                                     <?php
                                                     if( isset($chats) && count($chats)>0 ):
                                                        $longitud = count($chats);
                                                        for($i=0; $i<$longitud; $i++){
                                                        $item = $chats[$i];
                                                        if( $item->status == 2 ):
                                                     ?>
                                                        <li class="unread">
                                                            <a href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" >
                                                                <div class="col-mail col-mail-1">
                                                                    <p class="title"><?php echo $item->usuario ?></p><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i>
                                                                </div>
                                                                <div class="col-mail col-mail-2">
                                                                    <div class="subject"><?php echo $item->mensaje ?>
                                                                    </div>
                                                                    <div class="date">
                                                                                         <?php $date=date_create($item->fecha); 
                                                                                              echo date_format($date,"M d");
                                                                                          ?>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>

                                                      <?php 
                                                         endif; 
                                                         if( $item->status == 1 ):  
                                                      ?>
                                                        <li>
                                                            <a href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" >
                                                                <div class="col-mail col-mail-1">
                                                                    <p class="title"><?php echo $item->usuario ?></p><i class="fa fa-envelope-open-o fa-lg" aria-hidden="true"></i>
                                                                </div>
                                                                <div class="col-mail col-mail-2">
                                                                    <div class="subject" ><?php echo $item->mensaje ?> 
                                                                    </div>
                                                                    <div class="date">    <?php $date=date_create($item->fecha); 
                                                                                              echo date_format($date,"M d");
                                                                                          ?>
                                                                    </div>
                                                                </div>
                                                                
                                                            </a>
                                                        </li>
                                                      <?php 
                                                         endif; 
                                                        }
                                                      endif;
                                                      ?>


                                
                                            
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>

                                    
                                    </div>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>
            </div>
    </div>
                                                    -->




    <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-body">
                    
                      <div class="table-responsive m-t-40">
                          <table id="myTable" class="table table-hover">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Usuario</th>
                                      <th>Mensaje</th>
                                      <th>Fecha</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                if( isset($chats) && count($chats)>0 ):
                                  foreach ($chats as $key => $item):
                                    if( $item->status == 2 ):
                                   ?>
                                    <tr>
                                        
                                        <td>
                                              <i class="fa fa-envelope-o fa-lg" style="color: black;" aria-hidden="true"></i> 
                                        </td>
                                        <td style="width: 20%;" >
                                            <b><a style="color: black;" href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" >  <?php echo $item->usuario ?></a></b>
                                        </td>
                                        <td >
                                                <span class="d-inline-block text-truncate" style="max-width: 800px;">
                                                <a style="color: black;" href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" >  <?php echo $item->mensaje ?></a>
                                              
                                                 </span >
                                           
                                        </td >
                                        <td >
                                             <a style="color: black;" href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" > 
                                                <?php $date=date_create($item->fecha); 
                                                 echo date_format($date,"M d");
                                                ?>
                                             </a>
                                        </td>
                                        
                                    </tr>
                                    <?php 
                                     endif; 
                                     if( $item->status == 1 ):  
                                    ?>
                                    <tr>
                                       <td >
                                              <i class="fa fa-envelope-open-o fa-lg" aria-hidden="true"></i>
                                        </td>
                                        <td style="width: 20%;">
                                            <b> <a href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" >  <?php echo $item->usuario ?></a></b>
                                        </td>
                                        <td >
                                             <span class="d-inline-block text-truncate" style="max-width: 800px;">
                                                <a  href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" >  <?php echo $item->mensaje ?></a>
                                             </span >
                                           
                                        </td >
                                        <td >
                                            <a  href="<?php echo site_url("miadmin/chats/read/".$item->id_usuario) ?>" > 
                                                <?php $date=date_create($item->fecha); 
                                                 echo date_format($date,"M d");
                                                ?>
                                             </a>
                                        </td>
                                    </tr>
                                    <?php
                                    endif; 
                                  endforeach;
                                endif;
                               ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
    </div>
    </div>
    
</div>