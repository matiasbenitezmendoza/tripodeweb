<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!-- Place favicon.ico in the root directory -->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>Tripode</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url("admin/css/lib/bootstrap/bootstrap.min.css") ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("admin/css/helper.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("admin/css/style.css") ?>" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                                <h4>Login</h4>
                                <form method="post" action="">
                                  <?php _print_messages(); ?>
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" class="form-control" placeholder="Email" name="username" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" placeholder="Password" name="password" value="">
                                    </div>
                                    <!-- <div class="checkbox">
                                        <label>
                      										<input type="checkbox"> Remember Me
                      									</label>
                                        <label class="pull-right">
                      										<a href="#">Forgotten Password?</a>
                      									</label>
                                    </div> -->
                                    <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Sign in</button>
                                    <!-- <div class="register-link m-t-15 text-center">
                                        <p>Don't have account ? <a href="#"> Sign Up Here</a></p>
                                    </div> -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script src="<?php echo base_url("admin/js/lib/jquery/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/bootstrap/js/popper.min.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/bootstrap/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("admin/js/jquery.slimscroll.js") ?>"></script>
<script src="<?php echo base_url("admin/js/sidebarmenu.js") ?>"></script>
<script src="<?php echo base_url("admin/js/lib/sticky-kit-master/dist/sticky-kit.min.js") ?>"></script>
<script src="<?php echo base_url("admin/js/custom.min.js") ?>"></script>
