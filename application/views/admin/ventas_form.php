<section id="administracion">
    <h1 id="title_adminInt">Ventas</h1>
		<h1 id="title_lightbox"><?php echo (isset($id))?'EDITAR':'AGREGAR' ?></h1>
		<div class="txt_center">
            <?php
            /*
            * Mensajes
            */
            _print_messages();
            ?>
            <?php echo form_open_multipart('','name="formInfo"') ?>
                <input type="text" placeholder="Nombre del cliente" class="txt_inputAdmin" name="nombre" value="<?php echo $fields['nombre']['value'] ?>">
				<br>
                <label>Dirección del cliente</label><br>
                <textarea name="direccion" cols="115"><?php echo $fields['direccion']['value'] ?></textarea>
                <br>
                <label>Productos</label><br>
                <select name="productos_arr" id="productos_arr">
                    <option value="">Elija una opción</option>
                    <?php foreach($productos as $item): ?>
                        <option value="<?php echo $item->idproducto ?>"><?php echo $item->nombre.' - $'.number_format($item->precio,2) ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="text" id="cantidad" name="cantidad" value="1" />
                <input type="hidden" name="productos" id="productos_save" value="" />
                <br>
                <a href="javascript:void(0)" class="link" id="addItem">Agregar</a>
                <br>
                <div id="productos_info">
                    <?php
                    if( is_array($productos_venta) ):
                        foreach( $productos_venta as $item ):
                        ?>
                        <p>
                            <b><?php echo $item->nombre.' '.number_format($item->precio,2) ?></b>
                            (<?php echo $item->cantidad ?>)
                            <a href="<?php echo site_url("admin/venta/quitar/".$item->idproducto) ?>" class="link quitar_item">Quitar</a>
                        </p>
                        <?php
                        endforeach;
                    endif;
                    ?>
                </div>
                <br>
				<div class="txt_center">
					<a href="<?php echo site_url("admin/venta") ?>" class="btn_blackBorder closeBtn" >Cancelar</a>
					<input type="submit" value="<?php echo (isset($id))?'editar':'vender' ?>" class="btn_blackBorder">
				</div>
			</form>
		</div>
</section>

<script type="text/javascript">
$(document).ready(function(){
    $("#addItem").click(function(){
        id  = $("#productos_arr").val();
        if( id != '' ){
            txt = $("#productos_arr option[value='"+id+"']").text();
            $.ajax({
                url: '<?php echo site_url("admin/venta/addItem") ?>',
                async: false,
                method: 'POST',
                data: {id:id,cantidad:$("#cantidad").val(),producto:txt},
            });
            window.location.reload();
        }else{
            alert("Elija un producto");
        }
    });
});
</script>
