<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Pedidos</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Pedidos</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
         
          <!-- Inicia formulario de pedidos -->
          <?php
          /*
          * Mensajes
          */
          _print_messages();
          ?>
          <div class="col-lg-12">
                        <div class="card">
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">EDITAR PEDIDO</a> </li>
                            </ul>
                            </br>
                            <!-- Tab panes -->
                            <div class="tab-content" >
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-body" >
                                        <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                                            <div class="form-group row">
                                                    <label class="control-label col-md-3">Status del Pedido</label>
                                                    <div class="col-md-9">
                                                                    <select class="form-control" name="status_pedido">
                                                                            <option value=1>Pedido Recibido</option>
                                                                            <option value=2>Saliendo a tu Dirección</option>
                                                                            <option value=3>Pedido Entregado</option>
                                                                            <option value=4>Devolución / Cancelación</option>
                                                                    </select>
                                                    </div>
                                            </div>

                                            <?php
                                               if( isset($repartidor) && is_array($repartidor) && count($repartidor) < 1 ):
                                            ?>
                                                        <div class="form-group row">
                                                                <label class="control-label col-md-3">Asignar Repartidor</label>
                                                                <div class="col-md-9">
                                                                            <select class="form-control" name="id_repartidor">
                                                                                    <?php
                                                                                        if( isset($repartidores) && is_array($repartidores) && count($repartidores)>0 ):
                                                                                        foreach ($repartidores as $key => $repartidor):
                                                                                            $selected = ($fields['id_repartidor']['value']==$repartidor->id)?'selected':'';
                                                                                        ?>
                                                                                            <option value="<?php echo $repartidor->id ?>" <?php echo $selected ?>><?php echo $repartidor->repartidor ?></option>
                                                                                            <?php
                                                                                        endforeach;
                                                                                        endif;
                                                                                    ?>
                                                                            </select>
                                                                </div>
                                                        </div>


                                                        <div class="form-group row">
                                                                <label class="control-label col-md-3">Receta Médica</label>
                                                                <div class="control-label col-md-9">
                                                                        <div class="checkbox">
                                                                            <label style="color:black;">
                                                                                <input type="checkbox" name="receta_requerida"> Foto Obligatoria
                                                                            </label>
                                                                        </div>
                                                                </div>
                                                       </div>
                                            <?php
                                               endif;
                                            ?>

                                     
                                            <div class="form-group row">
                                                    <div class="col-md-12">
                                                            </br>
                                                            <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                                            <button type="button" class="btn btn-inverse" onclick="window.location = '<?php echo site_url("miadmin/pedidos") ?>'">Regresar</button>
                                                    </div>
                                            </div>

                                       

                                        </form>
                                    </div>
                                </div>
                            </div>
                            </br>


                            <div class="card">
                                        <div class="card-title">
                                            <h4 style="color: red;">Informacion del Pedido </h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Orden</th>
                                                            <th>Cliente</th>
                                                            <th>Fecha</th>
                                                            <th>Status del Pedido</th>
                                                            <th>Tipo de Pago</th>
                                                            <th>Status del Pago</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><span class="badge badge-success"><?php echo ('#'.$pedidos[0]->orden) ?></span></td>
                                                            <td><?php echo $pedidos[0]->usuario ?></td>
                                                            <td><?php echo $pedidos[0]->fecha ?></td>
                                                            <td>
                                                            

                                                                <?php if($pedidos[0]->status_pedido == 1):?>
                                                                         <span class="badge badge-danger"> Pedido Recibido</spam>
                                                                 <?php endif; ?>
                                                                 <?php if($pedidos[0]->status_pedido == 2):?>
                                                                         <span class="badge badge-success">  Saliendo a tu Direccion </spam>
                                                                 <?php endif; ?>
                                                                 <?php if($pedidos[0]->status_pedido == 3):?>
                                                                         <span class="badge badge-primary"> Pedido Entregado </spam>
                                                                 <?php endif; ?>
                                                                 <?php if($pedidos[0]->status_pedido == 4):?>
                                                                         <span class="badge badge-danger"> Devolución / Cancelación </spam>
                                                                 <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if($pedidos[0]->tipo_pago == 1):?>
                                                                        Tarjeta
                                                                 <?php endif; ?>
                                                                 <?php if($pedidos[0]->tipo_pago == 2):?>
                                                                          Tarjeta en domicilio
                                                                 <?php endif; ?>
                                                                 <?php if($pedidos[0]->tipo_pago == 3):?>
                                                                          Efectivo 
                                                                 <?php endif; ?>
                            
                                                            </td>
                                                            <td>
                                                                 <?php if($pedidos[0]->status_pago == 2):?>
                                                                         <span class="badge badge-danger">  Pendiente </spam>
                                                                 <?php endif; ?>
                                                                 <?php if($pedidos[0]->status_pago == 1):?>
                                                                         <span class="badge badge-primary"> Pagado </spam>
                                                                 <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                        <!--
                                                        <tr>
                                                            <th scope="row">2</th>
                                                            <td>Kolor Tea Shirt For Women</td>
                                                            <td><span class="badge badge-success">Tax</span></td>
                                                            <td>January 30</td>
                                                            <td class="color-success">$55.32</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">3</th>
                                                            <td>Blue Backpack For Baby</td>
                                                            <td><span class="badge badge-danger">Extended</span></td>
                                                            <td>January 25</td>
                                                            <td class="color-danger">$14.85</td>
                                                        </tr>-->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        </br>
                                        </br>
                                    
                            </div>

                            <div class="card">
                                        <div class="card-title">
                                            <h4 style="color: red;">Detalle del Pedido </h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover ">
                                                    <thead>
                                                        <tr>
                                                            <th>Producto</th>
                                                            <th>Cantidad</th>
                                                            <th>Precio</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      
                                                            <?php
                                                                    if( isset($detalles) && count($detalles)>0 ):
                                                                    foreach ($detalles as $key => $item):
                                                                    ?>
                                                                        <tr>
                                                                            <td class="color-primary"><?php echo $item->producto ?></td>
                                                                            <td class="color-success"><?php echo $item->cantidad ?></td>
                                                                            <td class="color-primary"><?php echo '$'.$item->precio ?></td>                                                                        
                                                                            <td class="color-danger"><?php echo '$'.$item->precio_total ?></td>
                                                                        </tr>
                                                                        <?php
                                                                    endforeach;
                                                                    endif;
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        
                                        </div>

                            </div>

                            <div class="card">
                                        <div class="card-title">
                                            <h4 style="color: red;">Direccion de Envio</h4>
                                        </div>
                                      
                                        <div class="card-body">
                                            <h5 style="color: black;">
                                            <?php echo (
                                                        $pedidos[0]->calle.' #'.$pedidos[0]->numero_exterior.', interior #'.
                                                        $pedidos[0]->numero_interior.', CP. '.$pedidos[0]->codigo_postal.' ,'.
                                                        $pedidos[0]->residencial.', '.$pedidos[0]->colonia.', '.
                                                        $pedidos[0]->ciudad.', '.$pedidos[0]->estado.'.'
                                                        ) ?>
                                            </h5>
                                          
                                        </div>
                            </div>

                            <div class="card">
                                        <div class="card-title">
                                            <h4 style="color: red;">Observaciones del Pedido</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Cliente</th>
                                                            <th>
                                                              <?php 
                                                                 $h = 0;
                                                                 if( $pedidos[0]->calificacion_usuario != null && $pedidos[0]->calificacion_usuario != ""):
                                                                    for ($i = 0; $i < $pedidos[0]->calificacion_usuario; $i++):
                                                                        $h++;
                                                              ?>
                                                                       <img src="<?php echo base_url("imgs/star_copy_3.png") ?>" width="35px" border="0">
                                                              <?php
                                                                    endfor;
                                                                 endif;
                                                                    for ($i = $h; $i < 5; $i++):
                                                              ?>
                                                                     <img src="<?php echo base_url("imgs/star_copy_4.png") ?>" width="35px" border="0">
                                                              <?php
                                                                    endfor;
                                                              ?>    
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th>Comentarios</th>
                                                            <th><?php echo $pedidos[0]->comentarios_usuario ?></th>   
                                                        </tr>
                                                    </thead>
                                                  
                                                </table>
                                            </div>

                                                                </br>
                                                                </br>
                                                                </br>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Repartidor</th>
                                                            <th>
                                                              <?php 
                                                                 $j = 0;
                                                                 if( $pedidos[0]->calificacion_repartidor != null && $pedidos[0]->calificacion_repartidor != ""):
                                                                    for ($i = 0; $i < $pedidos[0]->calificacion_repartidor; $i++):
                                                                        $j++;
                                                              ?>
                                                                       <img src="<?php echo base_url("imgs/star_copy_3.png") ?>" width="35px" border="0">
                                                              <?php
                                                                    endfor;
                                                                 endif;
                                                                    for ($i = $j; $i < 5; $i++):
                                                              ?>
                                                                     <img src="<?php echo base_url("imgs/star_copy_4.png") ?>" width="35px" border="0">
                                                              <?php
                                                                    endfor;
                                                              ?>    
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th>Comentarios</th>
                                                            <th><?php echo $pedidos[0]->comentarios_repartidor ?></th>   
                                                        </tr>
                                                    </thead>
                                                  
                                                </table>
                                            </div>

                                       
                                        </div>
                                        
                                        
                            </div>

                            <div class="card">
                                        <div class="card-title">
                                            <h4 style="color: red;">Datos del repartidor</h4>
                                        </div>

                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Foto</th>
                                                            <th>Nombre</th>
                                                            <th>Telefono</th>
                                                            <th>Correo</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                            <?php
                                                            if( isset($repartidor) && is_array($repartidor) && count($repartidor)>0 ):
                                                            ?>
                                                                <tr>
                                                                    
                                                                    <td>
                                                                        <?php if( $repartidor[0]->imagen != "" ): ?>
                                                                             <div class="round-img">
                                                                                 <img src="<?php echo base_url("imgs/repartidores/".$repartidor[0]->imagen) ?>"  border="0">
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <td><?php echo ($repartidor[0]->nombre.' '.$repartidor[0]->apellido_paterno.' '.$repartidor[0]->apellido_materno) ?></td>
                                                                    <td><?php echo $repartidor[0]->telefono ?></td>
                                                                    <td><?php echo $repartidor[0]->email ?></td>   
                                                                </tr>        
                                                               
                                                            <?php
                                                                endif;
                                                            ?>                            
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                            </div>

                            <div class="card">
                                        <div class="card-title">
                                            <h4 style="color: red;">Receta Medica</h4>
                                        </div>

                                        <div class="card-body">
                
                                                    <?php if( $pedidos[0]->receta != "" && $pedidos[0]->receta != null  ): ?>
                                                                 <a href="<?php echo base_url("imgs/recetas/".$pedidos[0]->receta) ?>" class="btn btn-info">Ver Receta</a>
                                                    <?php endif; ?>
                                        </div>
                            </div>

                            </br>
                            </br>
                        
                        </div>
                    </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
