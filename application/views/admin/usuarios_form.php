<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Usuarios</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Incio</a></li>
                <li class="breadcrumb-item active">Usuarios</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
          <?php if( $accion != "Agregar" ): ?>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <?php if( $fields['imagen']['value'] != "" ): ?>
                              <header>
                                  <div class="avatar">
                                      <img src="<?php echo base_url("imgs/usuarios/".$fields['imagen']['value']) ?>" alt="<?php echo $fields['first_name']['value'] ?>" />
                                  </div>
                              </header>
                            <?php endif ?>
                            <h3><?php echo $fields['first_name']['value']." ".$fields['last_name']['value'] ?></h3>
                        </div>
                    </div>
                </div>
            </div>
          <?php endif; ?>
          <!-- Inicia formulario de usuario -->
          <?php
          /*
          * Mensajes
          */
          _print_messages();
          ?>
          <div class="col-lg-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Información</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <?php echo form_open_multipart('','name="formInfo" class="form-horizontal form-material"') ?>
                                            <div class="form-group">
                                                <label class="col-md-12">* Nombre(s)</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="" value="<?php echo $fields['first_name']['value'] ?>" name="first_name" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">* Apellido(s)</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="" value="<?php echo $fields['last_name']['value'] ?>" name="last_name" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Imagen</label>
                                                <div class="col-md-12">
                                                    <input type="file" name="imagen" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">* Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" value="<?php echo $fields['email']['value'] ?>" name="email" class="form-control form-control-line" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">* Contraseña</label>
                                                <div class="col-md-12">
                                                    <input type="password" value="" name="password" class="form-control form-control-line">
                                                    <!--<small class="form-control-feedback"> Si <b>no</b> se va a cambiar la contraseña, el campo debe ir vacío </small>-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">* Teléfono fijo</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="" value="<?php echo $fields['tel_fijo']['value'] ?>" name="tel_fijo" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">* Teléfono móvil</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="" value="<?php echo $fields['tel_movil']['value'] ?>" name="tel_movil" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">* Perfil</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control form-control-line" name="type">
                                                          <option value="">Elija una opción</option>
                                                          <option value="Administrador" <?php echo ($fields['type']['value']=="Administrador")?'selected':'' ?>>Administrador</option>
                                                          <option value="Manager" <?php echo ($fields['type']['value']=="Manager")?'selected':'' ?>>Manager</option>
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> Enviar</button>
                                                    <button type="button" class="btn btn-inverse" onclick="window.location = '<?php echo site_url("miadmin/usuarios") ?>'">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
