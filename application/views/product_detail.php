<div class="product_detail">
  <?php
    if (isset($productos) && is_array($productos) && count($productos)>0):
      foreach ($productos as $key => $producto):
       ?>
  <?php if ($producto->imagen != ""): ?>
    <div class="row align_center">
      <div class="col-lg-1"></div>
      <div class="col-lg-4 col-md-6 col-sm-6 img_product">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <?php if ($producto->imagenb != ""){ ?>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <?php } ?>
            <?php if ($producto->imagenc != ""){ ?>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <?php } ?>
            <?php if ($producto->imagend != ""){ ?>
              <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <?php } ?>
            <?php if ($producto->imagene != ""){ ?>
              <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
            <?php } ?>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="card">
                <img class="img_product" src="<?php echo base_url("imgs/productos/".$producto->imagen) ?>" alt="">
              </div>
            </div>
            <?php if ($producto->imagenb != ""){ ?>
            <div class="carousel-item">
              <div class="card">
                <img src="<?php echo base_url("imgs/productos/".$producto->imagenb) ?>" alt="">
              </div>
            </div>
            <?php } ?>
            <?php if ($producto->imagenc != ""){ ?>
            <div class="carousel-item">
              <div class="card">
                <img src="<?php echo base_url("imgs/productos/".$producto->imagenc) ?>" alt="">
              </div>
            </div>
            <?php } ?>
            <?php if ($producto->imagend != ""){ ?>
            <div class="carousel-item">
              <div class="card">
                <img src="<?php echo base_url("imgs/productos/".$producto->imagend) ?>" alt="">
              </div>
            </div>
            <?php } ?>
            <?php if ($producto->imagene != ""){ ?>
            <div class="carousel-item">
              <div class="card">
                <img src="<?php echo base_url("imgs/productos/".$producto->imagene) ?>" alt="">
              </div>
            </div>
            <?php } ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 info_product">
        <div class="row p1r">
          <div class="col-12">
            <p class="tittle"><?php echo $producto->nombre ?></p>
            <p class="price">$<?php echo $producto->precio ?></p>
            <p class="descrip_short"><?php echo $producto->descripcion_corta ?></p>
            <p class="descrip_long"><?php echo $producto->descripcion_larga ?></p>
          </div>
        </div>
        <div class="row p1r">
          <div class="col-lg-4 col-md-6 col-sm-6 m1r">
            <legend>Cantidad</legend>
            <select id="can_stock">
            <?php if ($producto->stock >5) {
              for ($i = 1; $i <= 5; $i++) {?>
                <option value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php }
            }else {
              for ($i = 1; $i <= $producto->stock; $i++) {?>
                <option value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php }
            } ?>
            </select>
          </div>
        </div>
        <div class="row p1r">
          <div class="col-lg-8 ">
            <!--<a href="<?php echo site_url("store/checkout/$producto->idproducto") ?>">-->
              <button type="submit" name="button" class="button_purple_shop" value="<?php echo $producto->idproducto ?>"><img src="<?php echo base_url("imgs/productos/".$producto->imagen) ?>" alt="" style="display:none">AGREGAR AL CARRITO</button>
            <!--</a>-->
          </div>
        </div>
        <div class="row p1r">
          <div class="col-12">
            <a href="#" class="politicas_entrega">Políticas de entrega</a>
          </div>
        </div>
      </div>
    </div>
  <?php endif;
    endforeach;
  endif;?>
</div>
<script type="text/javascript">
$('.product_detail').flyto({
item      : '.card',
target    : '.cart',
button    : '.button_purple_shop'
});
$('.product_detail').flyto({
  shake: true
});
</script>
<script type="text/javascript">
$( ".button_purple_shop" ).click(function() {
    $(".product_detail").loading({
      message: 'Agregando...',
      stoppable: true
    });
    var $id_item = $( this ).val();
    var $quantity = $('#can_stock option:selected').val();
    console.log($id_item + $quantity);
    //Delete card
    $.post( "<?php echo site_url("/store/add_cart") ?>", {id_item:$id_item,quantity:$quantity}, function( data ) {
    var res = data;
    if( res == "error" ){
    swal("My Competition", res.msg, res.type);
    return false;
    }else{
      $.post( "<?php echo site_url("/store/total_items_cart") ?>", {}, function( data ) {
        var res = data;
        if( res == "error" ){
        swal("My Competition", res.msg, res.type);
        return false;
        }else{
          $('.badge' ).replaceWith( "<span class='badge'>"+res+"</span>" );
          //$( ".button_purple_shop" ).text( "ACTUALIZAR" );
          //$('.button_purple_shop' ).replaceWith( "<button type='submit' name='button' class='button_purple_shop' value='<?php echo $producto->idproducto ?>'><img src='<?php echo base_url('imgs/productos/'.$producto->imagen) ?>' alt='' style='display:none'>ACTUALIZAR CARRITO</button>" );
        }
        console.log("Incrementado");
        $('.product_detail').loading('stop');
      });
    }
    console.log("agregado");
  });
});
</script>
