<?php


class General extends CI_Model {

	var $table = 'tab_clientes';

	var $id = 'idclientes';

	var $per_page = 3;

	var $fields = array();

    function __construct(){
        parent::__construct();
		}
		
	function insert( $objecto = null ){
	    $this->fields = array();
	    if( $objecto != null && is_array($objecto) ){

		    foreach( $objecto as $key => $info )
		    	$this->fields[$key] = $info['value'];

				$this->fields['fecha'] = date('Y-m-d H:i:s');
				$this->fields['status'] = 1;
        $this->db->insert($this->table, $this->fields);
        return $this->db->insert_id(); // regresa id
	    }
		return false;
	}

	function get( $id = null, $filter = null, $select = "*", $limit = "", $escape = true, $order = "" ){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

			if( $limit != "" )
	    	$this->db->limit( $limit );

			if( $order == "" )
				$order = "desc";

	    if( $filter != null && is_array($filter) )
		    foreach( $filter as $key=>$data )
		    	$this->db->where($key,$data,$escape);

	    $this->db->where("status",1);
	    $this->db->order_by($this->id,$order);
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function update( $objecto = null, $id = null, $filter = null ){
		$this->fields = array();
	    if( $objecto != null && is_array($objecto) ){

	    	foreach( $objecto as $key => $info )
		    	$this->fields[$key] = $info['value'];

				if( $filter != null && is_array($filter) )
			    foreach( $filter as $key=>$data )
			    	$this->db->where($key,$data);

				if( $id != null )
					$this->db->where($this->id,$id);

				$this->db->update($this->table, $this->fields);
				//log_message("info","UPDATE TEST IMG: ". $this->db->last_query());
				return true;
	    }
	    return false;
	}

	function delete( $objecto = null ){

	    if( $objecto != null && is_numeric($objecto) ){

			$this->db->where($this->id, $objecto);
			$this->db->update($this->table, array("status"=>2));
		    return true;

	    }
	    return false;
	}

	function update_user($id, $id_ldcom, $token, $idface) {
                
        $this->db->where("id",$id);
        $data = array(
					'id_ldcom' => $id_ldcom,
					'id_facebook' =>  $idface,
                    'token_facebook' => $token
		);
		
        $this->db->update("reg_usuarios", $data);
        return true;
	}
	
	function getPaginacion( $id = null, $select = "*", $limit = "", $offset = "" ){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

			if( $limit != "" )
				$this->db->limit( $limit );
			
			if( $offset != "" )
		    	$this->db->offset($offset);
				
	    $this->db->where("status",1);
	    $this->db->order_by("fecha","desc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}		

	//chat	
	function get_chat( $id = null, $select = "*", $limit = "", $offset = "" ){

	    if( $id != null ){
		   	$this->db->where("id_usuario",$id);
  			$this->db->or_where("respuesta",$id);
			}

			if( $limit != "" )
			    $this->db->limit($limit);
		
	  	if( $offset != "" )
					$this->db->offset($offset);

	    $this->db->where("status",1);
	    $this->db->order_by("fecha","desc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
		
	function get_chat2( $id = null, $select = "*"){

	    if( $id != null ){
		   	$this->db->where("id_usuario",$id);
  			$this->db->or_where("respuesta",$id);
			}

	    $this->db->where("status",1);
	    $this->db->order_by($this->id,"asc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function get_user_chat(){

		  $this->db->where("tab_chats.id_usuario !=", -1);
	    $this->db->where("tab_chats.status",1);
			$this->db->select("concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as usuario, tab_chats.id_usuario as id_usuario ,tab_chats.mensaje as mensaje, tab_chats.fecha as fecha , tab_chats.status_chat as status", false );
			$this->db->order_by("tab_chats.fecha","desc");
			$this->db->from('reg_usuarios');
			$this->db->join('tab_chats', 'reg_usuarios.id = tab_chats.id_usuario');
			$query = $this->db->get();

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function get_type_chat($busqueda){

		  $this->db->where("tab_chats.id_usuario !=", -1);
			$this->db->where("tab_chats.status",1);
			$this->db->where("tab_chats.status_chat",$busqueda);
			$this->db->select("concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as usuario, tab_chats.id_usuario as id_usuario ,tab_chats.mensaje as mensaje, tab_chats.fecha as fecha , tab_chats.status_chat as status", false );
			$this->db->order_by("tab_chats.fecha","desc");
			$this->db->from('reg_usuarios');
			$this->db->join('tab_chats', 'reg_usuarios.id = tab_chats.id_usuario');
			$query = $this->db->get();

			if ($query->num_rows() >= 1){
				return $query->result();
			}else{
				return false;
					}
					
	}
		
	function update_status_chat($id = null){
	  
        if( $id != null )
					$this->db->where("id_usuario",$id);

				$data = array(
						'status_chat' => 1
				);
				
				$this->db->update("tab_chats", $data);
				return true;
	    
	}

	function get_read_chat($id = null){

		$this->db->where("tab_chats.id_usuario",'-1');
	   //	$this->db->or_where("tab_chats.id_usuario",-1);
		$this->db->select("concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as usuario, reg_usuarios.email as email, tab_chats.mensaje as mensaje, tab_chats.id_usuario as id_usuario ,tab_chats.fecha as fecha , tab_chats.status_chat as status", false );
		$this->db->order_by("tab_chats.fecha","desc");
		$this->db->from('reg_usuarios');
		$this->db->join('tab_chats', 'reg_usuarios.id = tab_chats.id_usuario');
		$query = $this->db->get();

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
    }

	//pedidos

	function getPedidos( $id = null){

	    if( $id != null )
	    	$this->db->where("tab_pedidos.id",$id);

				$this->db->where("tab_pedidos.status",1);

			$this->db->select("tab_pedidos.id as id,tab_pedidos.id_repartidor, tab_pedidos.orden,tab_pedidos.fecha,tab_pedidos.status_pedido,tab_pedidos.tipo_pago, tab_pedidos.status_pago, 
													concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as usuario, reg_usuarios.email,
													tab_pedidos.comentarios_usuario,	tab_pedidos.comentarios_repartidor,	tab_pedidos.calificacion_usuario,	tab_pedidos.calificacion_repartidor,
													tab_pedidos.calle, tab_pedidos.referencia, tab_pedidos.numero_exterior, tab_pedidos.numero_interior, tab_pedidos.colonia, tab_pedidos.estado, 
													tab_pedidos.ciudad, tab_pedidos.tipo_envio,tab_pedidos.codigo_postal,tab_pedidos.residencial, tab_pedidos.codigo_postal_fac,
													tab_pedidos.calle_fac, tab_pedidos.numero_exterior_fac, tab_pedidos.numero_interior_fac, tab_pedidos.colonia_fac, tab_pedidos.estado_fac, 
													tab_pedidos.municipio_fac, tab_pedidos.razon_social,tab_pedidos.rfc,tab_pedidos.receta"
													, false );
			$this->db->order_by("tab_pedidos.id","desc");
			$this->db->from('tab_pedidos');
			$this->db->join('reg_usuarios', 'reg_usuarios.id = tab_pedidos.id_usuario');
			$query = $this->db->get();


			if ($query->num_rows() >= 1){
				return $query->result();
			}else{
				return false;
			}
	}
		
	function getPedidosRecientes( $id = null){

	    if( $id != null )
	    	$this->db->where("tab_pedidos.id",$id);

			$this->db->select("tab_pedidos.id as id,tab_pedidos.id_repartidor, tab_pedidos.orden,tab_pedidos.fecha,tab_pedidos.status_pedido,tab_pedidos.tipo_pago, tab_pedidos.status_pago, 
													concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as usuario, reg_usuarios.email,
													tab_pedidos.comentarios_usuario,	tab_pedidos.comentarios_repartidor,	tab_pedidos.calificacion_usuario,	tab_pedidos.calificacion_repartidor,
													tab_pedidos.calle, tab_pedidos.referencia, tab_pedidos.numero_exterior, tab_pedidos.numero_interior, tab_pedidos.colonia, tab_pedidos.estado, 
													tab_pedidos.ciudad, tab_pedidos.tipo_envio,tab_pedidos.codigo_postal,tab_pedidos.residencial, tab_pedidos.codigo_postal_fac,
													tab_pedidos.calle_fac, tab_pedidos.numero_exterior_fac, tab_pedidos.numero_interior_fac, tab_pedidos.colonia_fac, tab_pedidos.estado_fac, 
													tab_pedidos.municipio_fac, tab_pedidos.razon_social,tab_pedidos.rfc"
													, false );
			$this->db->limit(5);
		  $this->db->offset(0);
			$this->db->order_by("tab_pedidos.id","desc");
			$this->db->from('tab_pedidos');
			$this->db->join('reg_usuarios', 'reg_usuarios.id = tab_pedidos.id_usuario');
			$query = $this->db->get();


		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function getPaginacionPedidos($id = null, $select = "*", $limit = "", $offset = ""){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

			if( $limit != "" )
				$this->db->limit( $limit );
			
			if( $offset != "" )
		    	$this->db->offset($offset);
				
			$this->db->where("status",1);
			$this->db->where("status_pedido <", 3);
	    $this->db->order_by("fecha","desc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}		

	function getPaginacionPedidosFinalizados($id = null, $select = "*", $limit = "", $offset = ""){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

			if( $limit != "" )
				$this->db->limit( $limit );
			
			if( $offset != "" )
		    	$this->db->offset($offset);
				
			$this->db->where("status",1);
			$this->db->where("status_pedido", 3);
	    $this->db->order_by("fecha","desc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

      if ($query->num_rows() >= 1){
      	return $query->result();
      }else{
        return false;
      }
	}
	
	function getPedidosFinalizados($id = null, $select = "*"){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

			
		$this->db->where("status",1);
	    $this->db->order_by("fecha","desc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function getPedidosRepartidor( $id = null){

	    if( $id != null )
	    	$this->db->where("tab_pedidos.id_repartidor",$id);

			$this->db->where("tab_pedidos.status_pedido <", 3);

			$this->db->select("tab_pedidos.id as id,tab_pedidos.id_repartidor, tab_pedidos.orden, date(tab_pedidos.fecha_pedido) as fecha, time(tab_pedidos.fecha_pedido) as hora,,tab_pedidos.status_pedido,tab_pedidos.tipo_pago, tab_pedidos.status_pago, 
													concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as cliente, reg_usuarios.telefono,
													tab_pedidos.comentarios_usuario,	tab_pedidos.comentarios_repartidor,	tab_pedidos.calificacion_usuario,	tab_pedidos.calificacion_repartidor,
													tab_pedidos.calle, tab_pedidos.referencia, tab_pedidos.numero_exterior as noExt, tab_pedidos.numero_interior as noInt, tab_pedidos.colonia, tab_pedidos.estado, 
													tab_pedidos.ciudad, tab_pedidos.tipo_envio,tab_pedidos.codigo_postal as codigoPostal,tab_pedidos.residencial, tab_pedidos.codigo_postal_fac,
													tab_pedidos.calle_fac, tab_pedidos.numero_exterior_fac, tab_pedidos.numero_interior_fac, tab_pedidos.colonia_fac, tab_pedidos.estado_fac, 
													tab_pedidos.municipio_fac, tab_pedidos.razon_social,tab_pedidos.rfc, tab_pedidos.receta_requerida"
													, false );
			$this->db->order_by("tab_pedidos.fecha","desc");
			$this->db->from('tab_pedidos');
			$this->db->join('reg_usuarios', 'reg_usuarios.id = tab_pedidos.id_usuario');
			$query = $this->db->get();


			if ($query->num_rows() >= 1){
				return $query->result();
			}else{
				return false;
			}
	}

	function getPedidosRepartidorRuta( $id = null){

	    if( $id != null )
	    	$this->db->where("tab_pedidos.id_repartidor",$id);

			$this->db->where("tab_pedidos.status_pedido", 2);

			$this->db->select("tab_pedidos.id as id,tab_pedidos.id_repartidor, tab_pedidos.orden, date(tab_pedidos.fecha_pedido) as fecha, time(tab_pedidos.fecha_pedido) as hora,,tab_pedidos.status_pedido,tab_pedidos.tipo_pago, tab_pedidos.status_pago, 
													concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as cliente, reg_usuarios.telefono,
													tab_pedidos.comentarios_usuario,	tab_pedidos.comentarios_repartidor,	tab_pedidos.calificacion_usuario,	tab_pedidos.calificacion_repartidor,
													tab_pedidos.calle, tab_pedidos.referencia, tab_pedidos.numero_exterior as noExt, tab_pedidos.numero_interior as noInt, tab_pedidos.colonia, tab_pedidos.estado, 
													tab_pedidos.ciudad, tab_pedidos.tipo_envio,tab_pedidos.codigo_postal as codigoPostal,tab_pedidos.residencial, tab_pedidos.codigo_postal_fac,
													tab_pedidos.calle_fac, tab_pedidos.numero_exterior_fac, tab_pedidos.numero_interior_fac, tab_pedidos.colonia_fac, tab_pedidos.estado_fac, 
													tab_pedidos.municipio_fac, tab_pedidos.razon_social,tab_pedidos.rfc, tab_pedidos.receta_requerida"
													, false );
			$this->db->order_by("tab_pedidos.fecha","desc");
			$this->db->from('tab_pedidos');
			$this->db->join('reg_usuarios', 'reg_usuarios.id = tab_pedidos.id_usuario');
			$query = $this->db->get();


			if ($query->num_rows() >= 1){
				return $query->result();
			}else{
				return false;
			}
	}

	function getPedidosRepartidorFinalizado( $id = null,  $limit = "", $offset = ""){

	    if( $id != null )
	    	$this->db->where("tab_pedidos.id_repartidor",$id);

		if( $limit != "" )
				$this->db->limit( $limit );
			
		if( $offset != "" )
				$this->db->offset($offset);
				
			$this->db->where("tab_pedidos.status_pedido", 3);

			$this->db->select("tab_pedidos.id as id,tab_pedidos.id_repartidor, tab_pedidos.orden, date(tab_pedidos.fecha_pedido) as fecha, time(tab_pedidos.fecha_pedido) as hora,,tab_pedidos.status_pedido,tab_pedidos.tipo_pago, tab_pedidos.status_pago, 
													concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as cliente, reg_usuarios.telefono,
													tab_pedidos.comentarios_usuario,	tab_pedidos.comentarios_repartidor,	tab_pedidos.calificacion_usuario,	tab_pedidos.calificacion_repartidor,
													tab_pedidos.calle, tab_pedidos.referencia, tab_pedidos.numero_exterior as noExt, tab_pedidos.numero_interior as noInt, tab_pedidos.colonia, tab_pedidos.estado, 
													tab_pedidos.ciudad, tab_pedidos.tipo_envio,tab_pedidos.codigo_postal as codigoPostal,tab_pedidos.residencial, tab_pedidos.codigo_postal_fac,
													tab_pedidos.calle_fac, tab_pedidos.numero_exterior_fac, tab_pedidos.numero_interior_fac, tab_pedidos.colonia_fac, tab_pedidos.estado_fac, 
													tab_pedidos.municipio_fac, tab_pedidos.razon_social,tab_pedidos.rfc"
													, false );
			$this->db->order_by("tab_pedidos.fecha","desc");
			$this->db->from('tab_pedidos');
			$this->db->join('reg_usuarios', 'reg_usuarios.id = tab_pedidos.id_usuario');
			$query = $this->db->get();


			if ($query->num_rows() >= 1){
				return $query->result();
			}else{
				return false;
			}
	}

	

	//amdin

	function getTotal($tabla = null){
		$this->db->where("status",1);
	    $query = $this->db->select( "count(*) as total", false );
	    $query = $this->db->get($tabla);
		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	

	function getTotalP($tabla = null){
		$this->db->where("status",1);
		$this->db->where("status_pedido <", 4);
	    $query = $this->db->select( "count(*) as total", false );
	    $query = $this->db->get($tabla);
		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
    }

	function getTotalChat(){
		$this->db->where("status",1);
		$this->db->where("status_chat",2);
		$this->db->where("id_usuario !=", -1);
	    $query = $this->db->select( "count(*) as total", false );
	    $query = $this->db->get("tab_chats");
		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotalPedidos($status = null){
		$this->db->where("status",1);
		$this->db->where("status_pedido", $status);
	    $query = $this->db->select( "count(*) as total", false );
	    $query = $this->db->get("tab_pedidos");
		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getTotalPedidosActivos(){
		$this->db->where("status",1);
		$this->db->where("status_pedido !=", 4);
	    $query = $this->db->select( "count(*) as total", false );
	    $query = $this->db->get("tab_pedidos");
		if ($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}


}
