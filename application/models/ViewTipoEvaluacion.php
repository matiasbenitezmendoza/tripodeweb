<?php defined('BASEPATH') or exit('No direct script access allowed');

class ViewTipoEvaluacion extends CI_Model {

    protected $_VIEW_NAME = "app_view_tevaluaciones";
    protected $_COLUMN_FARMACIA = "idfarmacia";
    protected $_COLUMN_PERIODO = "periodo";

    public function findWhere($idFarmacia,$periodo){
        $this->db->where($this->_COLUMN_FARMACIA, $idFarmacia);
        $this->db->where($this->_COLUMN_PERIODO, $periodo);
        $query = $this->db->get($this->_VIEW_NAME);

        $rows = [];
        foreach ($query->result() as $row) {
            $rows[] = $row;
        }

        //log_message("INFO","Evaluaciones periodo: ". $periodo);
        //log_message("INFO","Evaluaciones: ". json_encode($rows));
        return $rows;
    }

}
?>
