<?
defined('BASEPATH') or exit('No direct script access allowed');

/* modelo ya no usado */
class VisitaModel extends CI_Model {

    protected $_TABLE_VISIT = "app_evaluaciones";
    protected $_TABLE_RESULT = "app_evaluaciones_resultados";
    protected $_COLUMN_FECHA_EVAL = "fecha_evaluacion";
    protected $_COLUMN_IDUSUARIO = "idusuario";
    protected $_COLUMN_PERIODO = "periodo";
    protected $_COLUMN_STATUS = "status";
    protected $_COLUMN_IDFARMACIA = "idfarmacia";
    protected $_COLUMN_ABIERTO = "abierto";
    protected $_COLUMN_IDEVALUACION = "idevaluacion";

    protected $NO_VISITADO = 0;
    protected $ABIERTO = 1;
    protected $CERRADO = 2;

    public function findWhere($idfarmacia,$periodo) {
        $this->db->where($this->_COLUMN_PERIODO, $periodo);
        $this->db->where($this->_COLUMN_IDFARMACIA, $idfarmacia);
        $this->db->where($this->_COLUMN_STATUS, 1);
        $this->db->order_by($this->_COLUMN_IDEVALUACION, "DESC");
        $this->db->limit(1);
        $query = $this->db->get($this->_TABLE_VISIT);
        if($query->num_rows()) {
            log_message("INFO","VISITA: ". json_encode($query->row()));
            return $query->row();
        }
        return null;
    }

    public function save($user, $visit, $result) {

        $_RESULT = [
            'status' => false,
            'message' => '',
            'data' => null
        ];

        $yaVisitado = null;
        $timezone = new DateTimeZone('America/Mexico_City');
        $fecha = new DateTime('now', $timezone);

        // buscamos la visit
        $this->db->where($this->_COLUMN_FECHA_EVAL, $visit->fechaEval);
        $this->db->where($this->_COLUMN_IDUSUARIO, $user->idusuario);
        $this->db->where($this->_COLUMN_IDFARMACIA, $visit->idfarmacia);
        $this->db->limit(1);
        $query = $this->db->get($this->_TABLE_VISIT);
        if($query->num_rows()) {
            $yaVisitado = $query->row();
        }

        $_idevaluacion = null;
        // visita ya existe
        if ($yaVisitado != null ) {
            $_idevaluacion = $yaVisitado->idevaluacion;
        } else {
            // guardar visita
            $obj_visita = new stdClass;
            $obj_visita->idfarmacia = $visit->idfarmacia;
            $obj_visita->idusuario = $user->idusuario;
            $obj_visita->abierto = $visit->abierto;
            $obj_visita->img = "por aqui hay que hacer algo"; // aun por revisar
            $obj_visita->fecha = $fecha->format("Y-m-d H:i:s");
            $obj_visita->fecha_evaluacion = $visit->fechaEval;
            $obj_visita->version = $user->version;
            $obj_visita->status = 1;

            $this->db->insert($this->_TABLE_VISIT, $obj_visita);
            $_idevaluacion = $this->db->insert_id();

            if ($_idevaluacion) {
                $obj_visita->idevaluacion = $_idevaluacion;
            } else {
                $_RESULT['status'] = FALSE;
                $_RESULT['message'] = "NO FUE POSIBLE REGISTRAR LA VISITA";
                return $_RESULT;
            }

        }

        if ($_idevaluacion == null) {
            $_RESULT['status'] = FALSE;
            $_RESULT['message'] = "NO SE ENCONTRO LA VISITA";
            return $_RESULT;
        }

        if (!empty($result)) {
            if ($_idevaluacion != null) {
                $result_eval = new stdClass;
                $result_eval->idtipoevaluacion = $result->idtipoevaluacion;
                $result_eval->idevaluacion = $_idevaluacion;
                $result_eval->existe = $result->existe;
                $result_eval->corregido = $result->corregido;
                $result_eval->idmotivo = $result->idmotivo;
                $result_eval->img = "por aqui hay que hacer algo";
                $result_eval->fecha = $fecha->format("Y-m-d H:i:s");
                $result_eval->status = 1;

                $id_result = $this->db->insert($this->_TABLE_RESULT, $result_eval);
                if ($id_result != 0) {
                    $_RESULT['status'] = TRUE;
                    $_RESULT['message'] = "EVALUACIÓN GUARDADA CORRECTAMENTE";
                    return $_RESULT;
                }

                $_RESULT['status'] = FALSE;
                $_RESULT['message'] = "OCURRIO ALGO AL GUARDAR EL RESULTADO";
                return $_RESULT;
            }
        } else {
            if ($_idevaluacion != null) {
                $_RESULT['status'] = TRUE;
                $_RESULT['message'] = "VISITA #" . $_idevaluacion . " REGISTRADA EXITOSAMENTE ";
                $_RESULT['data'] = $result;
                return $_RESULT;
            }
        }
    }

}
?>
