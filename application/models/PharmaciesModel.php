<?
defined('BASEPATH') or exit('No direct script access allowed');

class PharmaciesModel extends CI_Model {

    protected $_TABLE_EVAL = "app_evaluaciones";
    protected $_TABLE_NAME = "app_directorio_con_visita";
    protected $_TABLE_NAME_CAMBIOS = "app_directorio_cambios";
    protected $_COLUMN_ZONA = "zona";
    protected $_COLUMN_REGION = "region";
    protected $_COLUMN_STATUS = "status";
    protected $_COLUMN_ID_FARMACIA = "idfarmacia";


    public function __construct() {
        parent::__construct();
        $this->load->model("ViewTipoEvaluacion");
        $this->load->model("VisitaModel");
    }

    public function findWhere($user) {

        $this->db->where($this->_COLUMN_REGION, $user->region);
        $this->db->where($this->_COLUMN_ZONA,   $user->zona);
        $this->db->where($this->_COLUMN_STATUS, 1);
        $query = $this->db->get($this->_TABLE_NAME);

        $rows = [];

        foreach ($query->result() as $row) {
            // buscar visita dentro del periodo
            $row->tipo_evaluaciones = $this->ViewTipoEvaluacion->findWhere($row->idfarmacia,$user->periodo);            
            if( is_array($row->tipo_evaluaciones) && count($row->tipo_evaluaciones)>0 ){            
              $visita = $this->VisitaModel->findWhere($row->idfarmacia,$user->periodo);
              $row->visitado = (isset($visita->abierto))?$visita->abierto:3;
              $rows[] = $row;
            }
        }
        
        return $this->orderByLocation($user, $rows);
    }


    public function update($farmacia) {
        // insert if not exists
        $this->db->where($this->_COLUMN_ID_FARMACIA, $farmacia->idfarmacia);
        $this->db->limit(1);
        $query = $this->db->get($this->_TABLE_NAME_CAMBIOS);
        if($query->num_rows()) {
            $this->db->where($this->_COLUMN_ID_FARMACIA, $farmacia->idfarmacia);
            return $this->db->update($this->_TABLE_NAME_CAMBIOS, $farmacia);
        } else {
            return $this->db->insert($this->_TABLE_NAME_CAMBIOS, $farmacia);
        }


        // update if exists
    }
    /**
     * Ordena por coordenadas de usuario
     *
     * @return void
     */
    private function orderByLocation($user, $farmacias) {
        if(!isset($user->latitud) || !isset($user->longitud)
            || empty($user->latitud) || empty($user->longitud)) return $farmacias;

        foreach($farmacias as $farmacia) {
            // calcula la distancia
            $farmacia->distance = $this->_distance_haversine(
                $this->stoDouble($user->latitud),
                $this->stoDouble($user->longitud),
                $this->stoDouble($farmacia->latitud),
                $this->stoDouble($farmacia->longitud));
        }

        // ordena por distancia
        $bydistance = array();
        foreach ($farmacias as $key => $row) {
            $bydistance[$key] = $row->distance;
        }
        array_multisort($bydistance, SORT_ASC, $farmacias);

        log_message("INFO","FARMACIAS: ". json_encode($farmacias));
        return $farmacias;
    }

    private function _distance_haversine($latitudeFrom,$longitudeFrom,$latitudeTo,$longitudeTo){
        $delta_lat = $latitudeTo - $latitudeFrom ;
        $delta_lon = $longitudeTo - $longitudeFrom ;

        $earth_radius = 6372.795477598;

        $alpha    = $delta_lat/2;
        $beta     = $delta_lon/2;
        $a        = sin(deg2rad($alpha)) * sin(deg2rad($alpha)) + cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * sin(deg2rad($beta)) * sin(deg2rad($beta)) ;
        $c        = asin(min(1, sqrt($a)));
        $distance = 2*$earth_radius * $c;
        return round($distance*1.61,4);
    }

    private function stoDouble($value) {
        try {
            return doubleval($value);
        } catch (Exception $e) {
            return 0;
        }
    }

}
?>
