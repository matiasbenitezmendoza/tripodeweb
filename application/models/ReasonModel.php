<?
defined('BASEPATH') or exit('No direct script access allowed');

class ReasonModel extends CI_Model {
    
    protected $_TABLE_NAME = "app_motivos";    
    protected $_COLUMN_STATUS = "status";
    
    public function findAll() {        
        $this->db->where($this->_COLUMN_STATUS, 1);
        $query = $this->db->get($this->_TABLE_NAME);
        $rows = [];

        foreach ($query->result() as $row)
            $rows[] = $row;
        
        return $rows;
    }
}
?>