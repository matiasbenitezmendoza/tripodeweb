<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RepartidoresModel extends CI_Model {

    protected $_TABLE_NAME = "reg_repartidores";    
    protected $_COLUMN_USER = "email";
    protected $_COLUMN_STATUS = "status";

    public function findOneWhere($user) {
        $this->db->where($this->_COLUMN_USER, $user);
        $this->db->where($this->_COLUMN_STATUS, 1);
        $this->db->limit(1);
        $query = $this->db->get($this->_TABLE_NAME);
        if($query->num_rows()) {
            return $query->row();
        }
        return null;
    }

}

?>