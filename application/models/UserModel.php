<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model {

    protected $_TABLE_NAME = "reg_usuarios";    
    protected $_COLUMN_USER = "email";
    protected $_COLUMN_STATUS = "status";

    public function findOneWhere($user) {
        $this->db->where($this->_COLUMN_USER, $user);
        $this->db->where($this->_COLUMN_STATUS, 1);
        $this->db->limit(1);
        $query = $this->db->get($this->_TABLE_NAME);
        if($query->num_rows()) {
            return $query->row();
        }
        return null;
    }

    public function direccion($id) {
        $this->db->where("id_usuario", $id);
        $this->db->where("status", 1);
        $this->db->limit(1);
        $query = $this->db->get("tab_direcciones");
        if($query->num_rows()) {
            return $query->row();
        }
        return null;
    }

    public function update($id, $id_ldcom) {
                
        $this->db->where("id",$id);
        $data = array(
                    'id_ldcom' => $id_ldcom
        );

        $this->db->update("reg_usuarios", $data);
        return true;
    }




}

?>