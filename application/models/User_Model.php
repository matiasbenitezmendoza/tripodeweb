<?php
defined('BASEPATH') or exit('No direct script access allowed');


class User_Model extends CI_Model {

    // database table name
    var $table = 'users';

    /**
     * Add a user, password will be hashed
     *
     * @param array user
     * @return int id
     */
    public function insert($user) {
        // need the library for hashing the password
        $this->load->library("auth");

        $user['password'] = $this->hash($user['password']);
        $user['registered'] = time();

        $this->db->insert($this->table, $user);
        return $this->db->insert_id();
    }

    /**
     * Update a user, password will be hashed
     *
     * @param int id
     * @param array user
     * @return int id
     */
    public function update($id, $user) {
        // prevent overwriting with a blank password
        if (isset($user['password']) && $user['password']) {
            $user['password'] = $this->hash($user['password']);
        } else {
            unset($user['password']);
        }

        $this->db->where('id', $id)->update($this->table, $user);
        return $id;
    }

    /**
     * Delete a user
     *
     * @param string where
     * @param int value
     * @param string identification field
     */
    public function delete($where, $value = FALSE) {
        if (!$value) {
            $value = $where;
            $where = 'id';
        }

        $this->db->where($where, $value)->delete($this->table);
    }

    /**
     * Retrieve a user
     *
     * @param string where
     * @param int value
     * @param $filter arreglo con los parametros y valores para filtrar
     */
    public function get($where = null, $value = FALSE, $filter = null ) {

        if( $value != FALSE )
	    	$this->db->where("email",$value);

	    if( $filter != null && is_array($filter) )
		    foreach( $filter as $key=>$data )
		    	$this->db->where($key,$data);

	   $this->db->where("status",1);

       $query = $this->db->get( $this->table );

        if ($query->num_rows() >= 1){
        	return $query->result();
        }else{
	        return false;
        }
    }

    /**
     * Retrieve a user
     *
     * @param string where
     * @param int value
     * @param $filter arreglo con los parametros y valores para filtrar
     */
    public function getLogin($where = null, $value = FALSE, $filter = null ) {

      if( $value != FALSE )
    	$this->db->where("email",$value);

      $this->db->where_in("status",array(1,9));

      $query = $this->db->get( $this->table );

      if ($query->num_rows() >= 1){
      	return $query->result();
      }else{
        return false;
      }
    }

    /**
     * Check if a user exists
     *
     * @param string where
     * @param int value
     * @param string identification field
     */

    public function exists($where, $value = FALSE) {
        if (!$value) {
            $value = $where;
            $where = 'id';
        }

        return $this->db->where($where, $value)->count_all_results($this->table);
    }

    /**
     * Password hashing function
     *
     * @param string $password
     */
    public function hash($password) {
        $this->load->library('PasswordHash', array('iteration_count_log2' => 8, 'portable_hashes' => FALSE));

        // hash password
        return $this->passwordhash->HashPassword($password);
    }

    /**
     * Compare user input password to stored hash
     *
     * @param string $password
     * @param string $stored_hash
     */
    public function check_password($password, $stored_hash) {
        $this->load->library('PasswordHash', array('iteration_count_log2' => 8, 'portable_hashes' => FALSE));

        // check password
        return $this->passwordhash->CheckPassword($password, $stored_hash);
        //return $password;
    }
}
