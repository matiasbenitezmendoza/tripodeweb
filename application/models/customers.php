<?php


class Customers extends CI_Model {

	var $table = 'tab_clientes';

	var $id = 'idclientes';

	var $news_per_page = 3;

	var $fields = array();

    function __construct(){
        parent::__construct();

    }


    function insert( $cliente = null ){
	    $this->fields = array();
	    if( $cliente != null && is_array($cliente) ){

		    foreach( $cliente as $key => $info )
		    	$this->fields[$key] = $info['value'];

			$this->fields['fecha'] = date('Y-m-d H:m:s');

			return $this->db->insert($this->table, $this->fields);

	    }

	    return false;

    }


    function getInventario( $id = null, $filter = null ){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

	    if( $filter != null && is_array($filter) )
		    foreach( $filter as $key=>$data )
		    	$this->db->where($key,$data);

	    $this->db->where("status",1);
	    $this->db->where("stock <=",5,false);
	    $this->db->order_by("idproducto","desc");
	    $query = $this->db->get( "productos" );

        if ($query->num_rows() >= 1){
        	return $query->result();
        }else{
	        return false;
        }

    }

    function get( $id = null, $filter = null, $select = "*" ){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

	    if( $filter != null && is_array($filter) )
		    foreach( $filter as $key=>$data )
		    	$this->db->where($key,$data);

	    $this->db->where("status",1);
	    $this->db->order_by($this->id,"desc");
	    $query = $this->db->select( $select, false );
	    $query = $this->db->get( $this->table );

        if ($query->num_rows() >= 1){
        	return $query->result();
        }else{
	        return false;
        }

    }

    function getCategorias( $id = null, $filter = null ){

	    if( $id != null )
	    	$this->db->where($this->id,$id);

	    if( $filter != null && is_array($filter) )
		    foreach( $filter as $key=>$data )
		    	$this->db->where($key,$data);

	    $this->db->where("status",1);
	    $this->db->order_by("idcategoria","desc");
	    $query = $this->db->get( "categorias" );

        if ($query->num_rows() >= 1){
        	return $query->result();
        }else{
	        return false;
        }

    }


    function update( $cliente = null, $idcliente = null ){

	    if( $cliente != null && is_array($cliente) && $idcliente != null && is_numeric($idcliente) ){

	    	foreach( $cliente as $key => $info )
		    	$this->fields[$key] = $info['value'];

			$this->db->where($this->id,$idcliente);
			return $this->db->update($this->table, $this->fields);
	    }

	    return false;

    }


    function delete( $cliente = null ){

	    if( $cliente != null && is_numeric($cliente) ){

			$this->db->where($this->id, $cliente);
			$this->db->update($this->table, array("status"=>2));
		    return true;

	    }

	    return false;
    }


    function getPerPage( $page = null ){

    	$star = 0;
    	if( $page != null && is_numeric($page) )
    		$star =  ( $this->news_per_page * $page) - $this->news_per_page;

    	$this->db->limit( $this->news_per_page, $star );
    	$this->db->where("status",1);
	    $this->db->order_by($this->id,"desc");
	    $query = $this->db->get( $this->table );

        if ($query->num_rows() >= 1)
        	return $query->result();
        else
	        return false;
    }


    function getTotalPages(){
    	$this->db->where('status', '1');
		$this->db->from( $this->table );

	    return $this->db->count_all_results() / $this->news_per_page;
    }

}
