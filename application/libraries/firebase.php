<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
define('FIREBASE_API_KEY', 'AAAAuZNSFKw:APA91bG2vR_h9OaR64qocRynUp5gJdPfq0rZ6g66aVakLi2MxEKN0gOnsQJsPPuJJdHG8R-aMIOak6rMEMIcWE5mAtO1RSkQxSfkhyDzUITCm7c37gQGi29fjuIoaw9mfHmGNNDFrwkx');
define('PROJECT_ID', 'tripode-app');
/**
 *
 */
class Firebase
{
  public function sendNotification($dispositivo, $mensaje, $orden) {
    //$fields =   '{ "to" : "'.$dispositivo.'", "data" : { "body": "'.$mensaje.'" } }';
    $fields = array(
      'to' => $dispositivo,
      'data' => $this->cuerpoMensaje($mensaje, $orden),
      "priority" => "high",
      "sound" => "default",
      'notification' => $this->cuerpoMensaje($mensaje, $orden)
    );
    
   //return $fields;
      return $this->sendPushNotification($fields);
  }

  public function sendNotificationAnd($dispositivo, $mensaje, $orden) {
    $fields = array(
      'to' => $dispositivo,
      'data' => $this->cuerpoMensaje($mensaje, $orden),
      "priority" => "high"
    );
    return $this->sendPushNotification($fields);
  }

  private function cuerpoMensaje($value = "", $orden = "") {
      $cuerpoNotificacion['mensaje'] = $value;
      $cuerpoNotificacion['title'] = "Tripode";
      $cuerpoNotificacion['id'] = $orden;
      $cuerpoNotificacion['body'] = $value;
      $cuerpoNotificacion['timestamp'] = date('Y-m-d G:i:s');
      $res['data'] = $cuerpoNotificacion;
      return $cuerpoNotificacion;
  }

  private function sendPushNotification($fields) {
    $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Authorization: key=' . FIREBASE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        return $result;
  }
}

 ?>
