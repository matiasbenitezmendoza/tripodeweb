<?php
if (!defined("BASEPATH"))exit("No direct script access allowed");

//require_once '/vendor/autoload.php';

class ShopCart {

  public function __construct($config = array()) {
    $this->ci = &get_instance();
  }

  public function add_item_cart($id_item,$quantity){
    $store = $this->ci->session->userdata('items_cart');
    $store['item'][$id_item] = $quantity;
		$this->ci->session->set_userdata('items_cart', $store);
    return "Agregado";
  }

  public function get_total($productos = ""){
    $store = $this->ci->session->userdata('items_cart');
    $subtotal = 0;
    if (isset($productos) && is_array($productos) && count($productos) > 0){
      foreach($productos as $key => $producto){
        $total_prod = $store['item'][$producto->idproducto] * $producto->precio;
        $subtotal = $subtotal + $total_prod;
      };
    };
    return $subtotal;
  }

  public function delete_item_cart($id_item){
    $store = $this->ci->session->userdata('items_cart');
    $store['item'][$id_item] = "";
    $this->ci->session->set_userdata('items_cart', $store);
    return "Eliminado";
  }

  public function total_item(){
    $store = $this->ci->session->userdata('items_cart');
    if (isset($store['item'])) {
      return count(array_filter($store['item']));
    }else{
      return 0;
    }

  }

}
?>
