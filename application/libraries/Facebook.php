<?php
if (!defined("BASEPATH"))exit("No direct script access allowed");

require_once './vendor/autoload.php';

class Facebook {

  private $fb = null;
  private $app_id = '156632948387185';
  private $app_secret = '3026380fdc4b4a3c8154b76bcf6207ac';
  private $site_url = 'https://www.codeandote.com/wa/bigfan/';
  private $permissions = ['public_profile','email'];
  private $helper = null;
  private $accessToken = "";

  public function __construct($config = array()) {
      $this->ci = &get_instance();
      $this->ci->load->library('session');

      $this->fb = new Facebook\Facebook([
        'app_id' => $this->app_id,
        'app_secret' => $this->app_secret,
        'default_graph_version' => 'v2.12',
      ]);
      $this->helper = $this->getHelper();

      if(isset($_GET['code']) && $_GET['code'] == "logout"){
        $this->deleteAccessToken();
      }

      if (isset($_GET['state'])) {
          $this->ci->session->set_userdata('state', $_GET['state']);
      }
			if (isset($_GET['code'])) {
          $this->ci->session->set_userdata('code', $_GET['code']);
          $this->setAccessToken();
          if ($this->ci->session->userdata('items_cart') == "") {
            redirect("/store");
          }else{
            redirect("/store/checkout");
          }

      }
  }

  public function getHelper(){
    return $this->fb->getRedirectLoginHelper();
  }

  public function getLoginURL(){
    return $this->helper->getLoginUrl($this->site_url, $this->permissions);
  }

  //Cerrar sesion en facebook
  public function getLogoutURL(){
    return $this->helper->getLogoutUrl($this->ci->session->userdata('accessToken'),$this->site_url);
  }

  public function setAccessToken(){
    $this->helper->getPersistentDataHandler()->set('state', $this->ci->session->userdata('state'));
    try {
      $this->accessToken = $this->helper->getAccessToken();
      $this->ci->session->set_userdata('accessToken', $this->accessToken->getValue());
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      log_message('error', 'Graph returned an error: ' . $e->getMessage());
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      log_message('error', 'Facebook SDK returned an error: ' . $e->getMessage());
    }
  }

  public function deleteAccessToken(){
    $this->helper->getPersistentDataHandler()->set('state', '');
    $this->ci->session->set_userdata('accessToken', '');
    redirect("/");
  }

  public function getAccessToken(){
    //print_r ($this->ci->session->userdata('accessToken'));
    return $this->ci->session->userdata('accessToken');
  }

  public function getMe(){
    $me = null;
    if ($this->ci->session->userdata('user_data') == "") {
      try {
        $AccessToken = $this->getAccessToken();
        if( $AccessToken != "" ){
          // $this->fb->setDefaultAccessToken($AccessToken);
          $response = $this->fb->get('/me?fields=id,name,email,first_name,last_name',$AccessToken);
          $me = $response->getGraphUser();
          // $me = $response;
          $this->ci->session->set_userdata('user_data',Array
            (
                "id" => $me['id'],
                "name" => $me['name'],
                "email" => $me['email'],
                "first_name" => $me['first_name'],
                "last_name" => $me['last_name']
            ));
        }
      } catch(\Facebook\Exceptions\FacebookResponseException $e) {
        log_message('error', 'Graph returned an error: ' . $e->getMessage());
        $this->ci->messages->add($e->getMessage(),"error");
      } catch(\Facebook\Exceptions\FacebookSDKException $e) {
        log_message('error', 'Facebook SDK returned an error: ' . $e->getMessage());
        $this->ci->messages->add($e->getMessage(),"error");
      }

    }else{
      $me = $this->ci->session->userdata('user_data');
    }
    return $me;
  }

  public function getPicture(){
    $me = $this->getMe();
    $url_picture = 'https://graph.facebook.com/'.$me['id'].'/picture?type=large';
    // $image = file_get_contents('https://graph.facebook.com/'.$fid.'/picture?type=large');
    return $url_picture;
  }

}
?>
