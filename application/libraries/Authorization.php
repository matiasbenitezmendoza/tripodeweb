<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'third_party/php-jwt/JWT.php';
require_once APPPATH . 'third_party/php-jwt/BeforeValidException.php';
require_once APPPATH . 'third_party/php-jwt/ExpiredException.php';
require_once APPPATH . 'third_party/php-jwt/SignatureInvalidException.php';

use \Firebase\JWT\JWT;

class Authorization {
    protected $token_key;
    protected $token_algorithm;
    protected $token_header = ['authorization','Authorization'];
    protected $currentApp = "1.0";

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->config('jwt');
        $this->token_key        = $this->CI->config->item('jwt_key');
        $this->token_algorithm  = $this->CI->config->item('jwt_algorithm');
    }

    public function verificaToken() {

        $header = "".strip_tags($this->CI->input->get_request_header('Authorization'));
        if (!isset($header) || empty($header)) {
            return [
                'code' => REST_Controller::HTTP_UNAUTHORIZED,
                'status' => FALSE,
                'message' => 'ERROR EN LAS CABECERAS',
                'data' => $header
            ];
        }

        $headbearer = explode(" ", $header);

        if (empty($headbearqer) && count($headbearer) != 2 ) {
            return [
                'code' => REST_Controller::HTTP_UNAUTHORIZED,
                'status' => FALSE,
                'message' => 'ERROR DE AUTENTICACION header: '.$header,
                'data' => $headbearer
            ];
        }

        $token = $headbearer[1];
        return $this->validateToken($token);
    }

    public function generateToken($data) {
        try {
            return JWT::encode($data, $this->token_key, $this->token_algorithm);
        }
        catch(Exception $e) {
            return 'Message: ' .$e->getMessage();
        }
    }

    public function validateToken($token) {
        try {
            try {
                $token_decode = JWT::decode($token, $this->token_key, array($this->token_algorithm));
            } catch(Exception $e) {
                return [
                    'code' => REST_Controller::HTTP_BAD_REQUEST,
                    'status' => FALSE,
                    'message' => $e->getMessage(),
                    'data' => null
                ];
            }



            if(!empty($token_decode) AND is_object($token_decode)) {


                if(empty($token_decode->email)) {
                    return [
                        'code' => REST_Controller::HTTP_BAD_REQUEST,
                        'status' => FALSE,
                        'message' => 'USUARIO NO DEFINIDO',
                        'data' => null
                    ];
                }

                $version = $token_decode->version;
                if(empty($version)) {
                    return [
                        'code' => REST_Controller::HTTP_FORBIDDEN,
                        'status' => FALSE,
                        'message' => 'VERSIÓN DE APLICACIÓN NO VALIDA',
                        'data' => null
                    ];
                }

                $_version_app = $this->esVersionValida($version);
                if (!$_version_app) {
                    return [
                        'code' => REST_Controller::HTTP_FORBIDDEN,
                        'status' => FALSE,
                        'message' => 'VERSIÓN DE APLICACIÓN ANTIGUA',
                        'data' => null
                    ];
                }
                // Actualización de la información del usuario desde DB
                $this->CI->load->model("UserModel");
               // $usuario = $this->CI->UserModel->findOneWhere($token_decode->user);
                //$token_decode->periodo = $usuario->periodo;
                return [
                    'code' => REST_Controller::HTTP_OK,
                    'status' => TRUE,
                    'message' => 'ok',
                    'data' => $token_decode ];
            } else {
                return [
                    'code' => REST_Controller::HTTP_FORBIDDEN,
                    'status' => FALSE,
                    'message' => 'FORBIDDEN',
                    'data' => null
                ];
            }
        } catch(Exception $e) {
            return [
                'code' => REST_Controller::HTTP_FORBIDDEN,
                'status' => FALSE,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }
    }

    public function tokenIsExist($headers) {
        if(!empty($headers) AND is_array($headers)) {
            foreach ($this->token_header as $key) {
                if (array_key_exists($key, $headers) AND !empty($key))
                    return ['status' => TRUE, 'key' => $key];
            }
        }
        return ['status' => FALSE, 'message' => 'Token is not defined.'];
    }

    public function esVersionValida($_version_app) {

        $cv_app = explode(".", $this->currentApp); // version valida
        $v_app = explode(".", $_version_app); // version de la app que realiza la peticion

        if (count($cv_app) != count($v_app)) return false;

        $first2DigitsCurrentVString = substr($this->currentApp, 0, 3); // primeros dos digitos de la version 1.0
        $first2DigitsAppVString = substr($_version_app, 0, 3); //

        $last2DigitsCurrentVString = substr($this->currentApp, 2, 3); // ultimos dos digitos de la version 0.0
        $last2DigitsAppVString = substr($_version_app, 2, 3);

        $first2DigitsCurrentV = floatval($first2DigitsCurrentVString); // se convierten los valores a float
        $first2DigitsAppV = floatval($first2DigitsAppVString);

        $last2DigitsCurrentV = floatval($last2DigitsCurrentVString); // se convierten los valores a float
        $last2DigitsAppV = floatval($last2DigitsAppVString);


        if ($first2DigitsAppV >= $first2DigitsCurrentV) {
            if ($last2DigitsAppV >= $last2DigitsCurrentV) {
                return true;
            }
        }
        return false;
    }
}

?>
