<?php
if (!defined("BASEPATH"))exit("No direct script access allowed");

//require_once '/vendor/autoload.php';

class Mercadopagofunctions {

  private $site_url = 'https://api.mercadopago.com/v1/';
  private $accessToken = "TEST-5102298094778510-081013-1fa0e6c0b25600577e094dca81f38b07-324838471";
  private $client_id = "5102298094778510";
  private $client_secret = "bIhW8hTe3fxpu86lDrGZRfrLs3S2laG1";
  private $public_key = "TEST-047f4d72-ff81-4811-b8b4-85f526c25427";

  public function __construct($config = array()) {
    $this->ci = &get_instance();
  }

  public function return_access_token(){
    return $this->accessToken;
  }

  public function return_public_key(){
    return $this->public_key;
  }

  public function get_user_mp($user_email = ""){
    $curl = curl_init();

    $data = array('email'=>$user_email);

    curl_setopt_array($curl, array(
    CURLOPT_URL => $this->site_url."/customers/search?access_token=".$this->accessToken,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => json_encode($data),
    //CURLOPT_COOKIE => "TS01e25bc7=015d8005233ed8caf0633175b521931b422f91ded207d2c1d2c50a5ceebd26858c4cf8e342aa9f38c6d3e542435995a639805254d4; TS016da221=0119b547a2b662ba4214c87220baf4d439e6e3a685c59abe28772c3fa529559121afc5e10c85b4433b74b529705ddff67878bd2995",
    CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
    $answer = json_decode($response);
    $total_results = $answer->{'paging'}->{'total'};

    if ($total_results != 0) {
      $data = array(
        'id' => array('value'=>$answer->{'results'}[0]->{'id'}),
        'email' => array('value'=>$answer->{'results'}[0]->{'email'}),
        'first_name' => array('value'=>$answer->{'results'}[0]->{'first_name'}),
        'last_name' => array('value'=>$answer->{'results'}[0]->{'last_name'})
      );

      $this->ci->auth->login_user($data);

      $id_mp_user = $answer->{'results'}[0]->{'id'};
      return $id_mp_user;
    }else{
      $answer = json_decode($response);
      return "No existe el usuario";
    }

  }
  }

  public function create_user_mp($user_email="",$first_name="",$last_name=""){
    $curl = curl_init();
    $data = array('email'=>$user_email,'first_name'=>$first_name,'last_name'=>$last_name);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->site_url."/customers?access_token=".$this->accessToken,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "content-type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      $answer = json_decode($response);
      $data = array('id' => array('value'=>$answer->{'id'}),
          'email' => array('value'=>$answer->{'email'}),
          'first_name' => array('value'=>$answer->{'first_name'}),
          'last_name' => array('value'=>$answer->{'last_name'})
        );
        $this->ci->auth->login_user($data);
        $id_mp_user = $answer->{'id'};
        return $id_mp_user;

      }
  }

  public function get_cards_info($user_email="",$id_mp_user=""){
    $curl = curl_init();
    $data = array('email'=>$user_email);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->site_url."/customers/".$id_mp_user."/cards?access_token=".$this->accessToken,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "content-type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      //echo $response;
      $answer = json_decode($response);
      return $answer;
    }
  }

  public function add_tokencard_user($card_token="", $id_mp_user=""){
    require_once './vendor/autoload.php';

    MercadoPago\SDK::setAccessToken($this->accessToken);

    $card = new MercadoPago\Card();
    $card->token = $card_token;
    $card->customer_id = $id_mp_user;
    $card->save();

    return "saved";
  }

  public function delete_card($id_mp_user="",$id_card=""){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->site_url."/customers/".$id_mp_user."/cards/".$id_card."?access_token=".$this->accessToken,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "DELETE",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "content-type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
      return $err;
    } else {
      echo $response;
      return $response;
    }
  }

  public function pay_payment($direcciones = "", $productos = "",$store = "",$total = ""){
    if (isset($_REQUEST["token"])) {
      $token = $_REQUEST["token"];
      $payment_method_id = $_REQUEST["payment_method_id"];
      $installments = $_REQUEST["installments"];
      $issuer_id = $_REQUEST["issuer_id"];
      $shipments = array(
        "receiver_address" => array(
        "zip_code" => $direcciones[0]->codigo_postal,
        "street_number" => $direcciones[0]->exterior,
        "street_name" => $direcciones[0]->calle,
        "floor" => $direcciones[0]->interior
        )
      );
      $items = array();
      foreach($productos as $key => $producto) {
         $item[] = array(
           "title" => $producto->nombre,
           "description" => $producto->descripcion_corta,
           "quantity" => $store['item'][$producto->idproducto],
           "unit_price"=> $producto->precio
         );
       };
      $additional_info = array(
        "items" => $item,
        "shipments" => $shipments
      );

      MercadoPago\SDK::setAccessToken('TEST-5102298094778510-081013-1fa0e6c0b25600577e094dca81f38b07-324838471');

        $payment = new MercadoPago\Payment();
        $payment->transaction_amount = $total;
        $payment->token = $token;
        $payment->description = "Compra BigFan".$this->ci->session->userdata('id_mp');
        $payment->installments = $installments;
        $payment->payment_method_id = $payment_method_id;
        $payment->issuer_id = $issuer_id;
        //$payment->sponsor_id = '320489401';
        $payment->notification_url = 'http://bigfanofficial.com.mx:4000/rest/api/v2/pagos/confirmacion';
        $payment->additional_info = $additional_info;
        $payment->payer = array(
          "type" => "customer",
          "id" => $this->ci->session->userdata('id_mp'),
          "first_name" => $this->ci->session->userdata('first_name'),
          "last_name" => $this->ci->session->userdata('last_name'),
          "email" => $this->ci->session->userdata('email')
        );
        // Guarda y postea el pago
        $payment->save();
        //...
        // Imprime el estado del pago
        // print_r($payment);
        return $payment;
        //...
    }
  }
}
?>
