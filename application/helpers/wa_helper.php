<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
*
*	Helper general
*	@webandando by @chumbertoc
*	08-Noviembre-2017
*
*/

/*
* Muestra de mensajes
*	class = error || success
*/
function _print_messages(){
	$CI =& get_instance();
	echo validation_errors('<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>', '</div>');

	$all = $CI->messages->get();
	foreach($all as $type=>$messages):
		foreach($messages as $message):
			echo '<div class="alert alert-'.$type.' alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'.$message.'</div>';
		endforeach;
	endforeach;
}

/*
* Limpia el string para usarlo como url
*/
function _slug( $str = null ){

	$characters = array(
		 "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
		 "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
		 "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u", "¿" => "", "?" => "" , "," => "",
		 "¡" => "", "!" => "", "'" => "", "%" => "", '"' => "", '+' => "", '\'' => "", "." => "", "ü" => "u"
	);
	$str = strtolower($str);
	$str = trim(str_replace(' ', '-', $str));
	$str = strtr($str, $characters);

	return $str;
}

/*
* Regresa el listado de estados
*/
function _getStates($edo = ""){
	$states = array(
		"Aguascalientes",
		"Baja California",
		"Baja California Sur",
		"Campeche",
		"Chiapas",
		"Chihuahua",
		"Ciudad de México",
		"Coahuila",
		"Colima",
		"Durango",
		"Estado de México",
		"Guanajuato",
		"Guerrero",
		"Hidalgo",
		"Jalisco",
		"Michoacán",
		"Morelos",
		"Nayarit",
		"Nuevo León",
		"Oaxaca",
		"Puebla",
		"Querétaro",
		"Quintana Roo",
		"San Luis Potosí",
		"Sinaloa",
		"Sonora",
		"Tabasco",
		"Tamaulipas",
		"Tlaxcala",
		"Veracruz",
		"Yucatán",
		"Zacatecas"
	);

	$states = ( array_key_exists($edo,$states) )?$states[$edo]:$states;
	return $states;
}

/*
* Regresa el nombre del día de la semana en español
*/
function _name_day( $str = null ){

	$days = array(
		 "Monday" => "Lunes", "Tuesday" => "Martes", "Wednesday" => "Miércoles", "Thursday" => "Jueves", "Friday" => "Viernes", "Saturday" => "Sábado", "Sunday" => "Domingo",
		 1 => "Lunes", 2 => "Martes", 3 => "Miércoles", 4 => "Jueves", 5 => "Viernes", 6 => "Sábado", 7 => "Domingo",
		 "lunes" => "LUNES", "martes" => "MARTES", "miercoles" => "MIÉRCOLES", "jueves" => "JUEVES", "viernes" => "VIERNES", "sabado" => "SÁBADO", "domingo" => "DOMINGO",
	);

	$name = ( array_key_exists($str,$days) )?$days[$str]:$days[date("l")];

	return $name;
}

/*
* Regresa el nombre del mes en español
*/
function _name_month( $str = null ){

	$months = array(
		 "01" => "Enero", "02" => "Febrero", "03" => "Marzo", "04" => "Abril", "05" => "Mayo", "06" => "Junio", "07" => "Julio", "08" => "Agosto", "09" => "Septiembre", "10" => "Octubre", "11" => "Noviembre", "12" => "Diciembre",
	);

	$name = ( array_key_exists($str,$months) )?$months[$str]:$months[date("m")];

	return $name;
}

/*
* Regresa horas del día
*/
function _hours( $str = null ){

	$hours = array(
		"00:00:00","00:30:00","01:00:00","01:30:00","02:00:00","02:30:00","03:00:00","03:30:00","04:00:00","04:30:00","05:00:00","05:30:00","06:00:00","06:30:00","07:00:00","07:30:00","08:00:00","08:30:00","09:00:00","09:30:00","10:00:00","10:30:00","11:00:00","11:30:00","12:00:00","12:30:00","13:00:00","13:30:00","14:00:00","14:30:00","15:00:00","15:30:00","16:00:00","16:30:00","17:00:00","17:30:00","18:00:00","18:30:00","19:00:00","19:30:00","20:00:00","20:30:00","21:00:00","21:30:00","22:00:00","22:30:00","23:00:00","23:30:00"
	);

	$name = ( array_key_exists($str,$hours) )?$hours[$str]:$hours;

	return $name;
}

/*
* Envio de mail generico
*/
function _send_mail( $subject = null, $to = null, $body = null ){
	$CI =& get_instance();
  $CI->load->library(array('email'));

	$config = array(
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'newline' => "\r\n"
    );
  $CI->email->initialize($config);

  $CI->email->subject($subject);

  $CI->email->from('contacto@tripode.mx', "Tripode");
	$CI->email->to($to);

	$CI->email->message($body);

	$CI->email->send();
}

/*
* Cadena de caracteres aleatorios
*/
function _RandomString($length=10,$uc=TRUE,$n=TRUE,$sc=FALSE){
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if($n==1) $source .= '1234567890';
    if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }

    }
    return $rstr;
}

//post LD COM
function _curl_ldcom($url, $puerto, $metodo, $body){
					$curl = curl_init();
					$uri =	"https://new.aphcenter.com/FRAGARE/WebAPI_eCommerce/api/v2/ecommerce/".$url;
					curl_setopt_array($curl, array(
						CURLOPT_URL => $uri,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => $metodo,
						CURLOPT_POSTFIELDS => "",
						CURLOPT_HTTPHEADER => array(
							"Postman-Token: dfa43f97-35e9-4b1c-b4c2-e9c5b620a807",
							"cache-control: no-cache"
						),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);
					
					if ($err) {
									return false;
					} 
					else {
								$datos = json_decode($response);
								if(	isset($datos->IsSuccessful) ){
											if($datos->IsSuccessful == true ){
												    if($datos->Data == null || $datos->Data == 'null' ){
														     		if($datos->Message == "" ){
																				return true;
																	}else{
																			return $datos->Message;
																	}
													}else{
													      	return $datos->Data;
													}
											}
								}
								return false;
					}
}

function _curl_ldcom_r($url, $puerto, $metodo, $body){
	$curl = curl_init();
	$uri =	"https://new.aphcenter.com/FRAGARE/WebAPI_eCommerce/api/v2/ecommerce/".$url;
	curl_setopt_array($curl, array(
		CURLOPT_URL => $uri,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => $metodo,
		CURLOPT_POSTFIELDS => "",
		CURLOPT_HTTPHEADER => array(
			"Postman-Token: dfa43f97-35e9-4b1c-b4c2-e9c5b620a807",
			"cache-control: no-cache"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	
	if ($err) {
					return false;
	} 
	else {
				$datos = json_decode($response);
				
				return $datos;
	}
}

function _curl_ldcom_pruebas($url, $puerto, $metodo, $body){
	$curl = curl_init();
	$uri =	"https://new.aphcenter.com/FRAGARE/WebAPI_eCommerce/api/v2/ecommerce/".$url;
	curl_setopt_array($curl, array(
		CURLOPT_URL => $uri,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => $metodo,
		CURLOPT_POSTFIELDS => "",
		CURLOPT_HTTPHEADER => array(
			"Postman-Token: dfa43f97-35e9-4b1c-b4c2-e9c5b620a807",
			"cache-control: no-cache"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	
	if ($err) {
					return $err;
	} 
	else {
     		return $response;
	}
}
					



function _cambiar_llaves($llaves, $datos){
		$d = array();
		
		foreach($datos as $clave => $valor) {
						$myObj = json_decode ("{}");
						foreach( $llaves as $key => $field ){
										$l = 	$llaves[$key]['value'];
							      $myObj->$l = $valor->$key;
						}
						array_push ( $d , $myObj );
		}
		return $d;
}
