<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Talentos extends CI_Controller {
	var $data = array('page'=>'talentos');

	var $fields = array(
					'nombre' => array('value'=>'','validate' => array(
								                     'label'   => 'Nombre',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'slug' => array('value'=>'','validate' => array(
								                     'label'   => 'Slug',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'manager' => array('value'=>'','validate' => array(
								                     'label'   => 'Manager',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'ubicacion' => array('value'=>'','validate' => array(
								                     'label'   => 'Ubicaicón',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'tipo' => array('value'=>'','validate' => array(
								                     'label'   => 'Tipo',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'descripcion_corta' => array('value'=>'','validate' => array(
								                     'label'   => 'Descripción corta',
								                     'rules'   => 'trim|required'
								                  )),
					'descripcion_larga' => array('value'=>'','validate' => array(
								                     'label'   => 'Descripción corta',
								                     'rules'   => 'trim|required'
								                  )),
					'imagen' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen 1',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagen]'
								                  )),
					'imagenb' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen 2',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagenb]'
								                  )),
					'imagenc' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen 3',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagenc]'
								                  )),
					'imagend' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen 4',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagend]'
								                  )),
					'imagene' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen 5',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagene]'
								                  ))
				);

	var $params_imgs = array(
							'upload_path' => './imgs/talentos/',
							'allowed_types' => 'jpg|png|jpeg|gif',
							'file_name' => '',
							'overwrite' => TRUE,
						);
	var $filtro = array();

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin())
      	redirect('miadmin/login');

			if($this->auth->usertype() == "Manager")
				$this->filtro['id'] = $this->auth->userid();

			$this->general->table = "users";
			$this->general->id = "id";
			$this->data['usuarios'] = $this->general->get(null,$this->filtro);

			if($this->auth->usertype() == "Manager"){
					unset($this->filtro['id']);
					$this->filtro['manager'] = $this->auth->userid();
			}

			$this->general->table = "users";
			$this->general->id = "id";
			$this->data['user'] = $this->general->get($this->auth->userid());

			$this->data['user_type'] = $this->auth->usertype();
      $this->data['user_name'] = $this->auth->username();
			$this->general->table = "tab_talentos";
			$this->general->id = "idtalento";
  }

	public function index(){
		$this->data['talentos'] = $this->general->get(null,$this->filtro);
		$this->template->content->view('admin/talentos', $this->data);
		$this->template->publish('template_admin');
	}

	public function agregar( $id = null ){
		$talentos = $this->general->get(null,$this->filtro);

		if( $this->auth->usertype() != "Administrador" ){
			if( isset($talentos) && is_array($talentos) && count($talentos)>=$this->data['user'][0]->talentos ):
				$this->messages->add("No cuentas con permiso para agregar más telentos, ponte en contacto con tu ejecutivo.","danger");
				redirect("miadmin/talentos");
			endif;
		}

		$this->data['accion'] = 'Agregar';
		$this->data['id'] = "";
		$this->config_validates();
		if($this->form_validation->run() != FALSE){
			$this->fields['slug']['value'] = _slug($this->fields['nombre']['value']);
			$this->general->insert($this->fields);
			$this->messages->add("Se ha guardado exitosamente la información.","info");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;
    $this->template->content->view('admin/talentos_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function editar( $id = null ){

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;
		$this->config_validates();
		if ($this->form_validation->run() != FALSE){
			$this->fields['slug']['value'] = _slug($this->fields['nombre']['value']);
			if( $this->fields['imagen']['value'] == '' )
				unset($this->fields['imagen']);

			if( $this->fields['imagenb']['value'] == '' )
				unset($this->fields['imagenb']);

			if( $this->fields['imagenc']['value'] == '' )
				unset($this->fields['imagenc']);

			if( $this->fields['imagend']['value'] == '' )
				unset($this->fields['imagend']);

			if( $this->fields['imagene']['value'] == '' )
				unset($this->fields['imagene']);

			$info = array();
      foreach($this->fields as $key => $value)
        $info[$key] = $value['value'];

			if( $this->general->update($this->fields,$id) ){
				$this->messages->add("Se ha guardado la información correctamente","info");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = null;

			}else{
				$this->messages->add("No se pudo guardar la información, inténtelo más tarde","danger");
			}
		}

		$info = $this->general->get( $id,$this->filtro );
		if( !isset($info[0]->nombre) )
			redirect("miadmin/talentos");

		$this->fields['imagen']['value'] = '';
		$this->fields['imagenb']['value'] = '';
		$this->fields['imagenc']['value'] = '';
		$this->fields['imagend']['value'] = '';
		$this->fields['imagene']['value'] = '';
		foreach( $this->fields as $key=>$field ){
			$this->fields[$key]['value'] = $info[0]->$key;
		}

		$this->data['fields'] = $this->fields;
		$this->template->content->view('admin/talentos_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function eliminar( $id = null ){
		$response = array();
		$id = strip_tags($this->input->post("entrada",true));
    if( $id == null || !is_numeric($id) ){
			$response['titulo'] = "¡Eliminación!";
			$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
			$response['type'] = "warning";
			echo json_encode($response);
		}else{
			if( $this->general->delete( $id ) ){
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "Se elimino la información correctamente.";
				$response['type'] = "success";
				echo json_encode($response);
			}else{
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
				$response['type'] = "warning";
				echo json_encode($response);
			}
		}
	}

	/*
	* Validación y carga de imagenes
	*/
	function validImg( $str, $name ){

		if( isset($_FILES[$name]) && $_FILES[$name]['error']==0 && $_FILES[$name]['size']>0 ){
			$this->load->library('upload');
			$this->params_imgs['file_name']=$name.'_'.time();
			$this->upload->initialize($this->params_imgs);

			if ( ! $this->upload->do_upload($name) ){
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $txt )
					$this->form_validation->set_message('validImg', '<b>%s</b> '.$txt);

				return false;
			}else{
				$file_info = $this->upload->data();
				$this->fields[$name]['value'] = $file_info['file_name'];
			}
		}

		return true;
	}

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}
}
