<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	var $data = array('page'=>'usuarios');
	var $id_user = 0;
	var $metodo = "agregar";
	var $fields = array(
					'first_name' => array('value'=>'','validate' => array(
								                     'label'   => 'Nombre(s)',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'last_name' => array('value'=>'','validate' => array(
								                     'label'   => 'Apellido(s)',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'email' => array('value'=>'','validate' => array(
								                     'label'   => 'Email',
								                     'rules'   => 'trim|required|xss_clean|valid_email|callback_validEmail[email]'
								                  )),
					'password' => array('value'=>'','validate' => array(
								                     'label'   => 'Contraseña',
								                     'rules'   => 'trim|xss_clean|callback_validPassword[password]'
								                  )),
					'tel_fijo' => array('value'=>'','validate' => array(
								                     'label'   => 'Teléfono fijo',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'tel_movil' => array('value'=>'','validate' => array(
								                     'label'   => 'Teléfono móvil',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'type' => array('value'=>'','validate' => array(
								                     'label'   => 'Perfil',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'imagen' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagen]'
								                  ))
				);
	
		
						
	var $params_imgs = array(
							'upload_path' => './imgs/usuarios/',
							'allowed_types' => 'jpg|png|jpeg|gif',
							'file_name' => '',
							'overwrite' => TRUE,
						);

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin() || $this->auth->usertype() != "Administrador")
      	redirect('miadmin/login');

      $this->data['user_type'] = $this->auth->usertype();
      $this->data['user_name'] = $this->auth->username();
			$this->general->table = "users";
			$this->general->id = "id";
    }


	public function index(){
		
		$this->data['usuarios'] = $this->general->get(null,array('status'=>1));
		if (!empty($this->data['usuarios'])){
	     	$this->template->content->view('admin/usuarios', $this->data);
		 	$this->template->publish('template_admin');
		}
		else{
			$this->template->content->view('admin/usuarios', []);
			$this->template->publish('template_admin');
		}
	}

	public function agregar( $id = null ){
		$this->data['accion'] = 'Agregar';
		$this->data['id'] = "";
		$this->metodo = "agregar";
		$this->config_validates();

		if($this->form_validation->run() != FALSE){
			$this->fields['password']['value'] = md5($this->fields['password']['value']);
			$this->general->insert($this->fields);
			$this->messages->add("Se ha guardado exitosamente la información.","info");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;
    $this->template->content->view('admin/usuarios_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function editar( $id = null ){

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;
		$this->id_user = $id;
		$this->metodo = "editar";
		$this->config_validates();

		if ($this->form_validation->run() != FALSE){

			if( $this->fields['imagen']['value'] == '' )
				unset($this->fields['imagen']);

			if( $this->fields['password']['value'] == '' )
				unset($this->fields['password']);
			else
				$this->fields['password']['value'] = md5($this->fields['password']['value']);

			$info = array();
      foreach($this->fields as $key => $value)
        $info[$key] = $value['value'];

			if( $this->general->update($this->fields,$id) ){
				$this->messages->add("Se ha guardado la información correctamente","info");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = null;

			}else{
				$this->messages->add("No se pudo guardar la información, inténtelo más tarde","danger");
			}
		}

		$info = $this->general->get( $id );
		if( !isset($info[0]->first_name) )
			redirect("miadmin/usuarios");

		$this->fields['imagen']['value'] = '';
		$this->fields['password']['value'] = '';
		foreach( $this->fields as $key=>$field ){
			$this->fields[$key]['value'] = $info[0]->$key;
		}

		$this->data['fields'] = $this->fields;
		$this->template->content->view('admin/usuarios_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function eliminar( $id = null ){
		$response = array();
		$id = strip_tags($this->input->post("entrada",true));
        if( $id == null || !is_numeric($id) ){
			$response['titulo'] = "¡Eliminación!";
			$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
			$response['type'] = "warning";
			echo json_encode($response);
		}else{
			if( $this->general->delete( $id ) ){
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "Se elimino la información correctamente.";
				$response['type'] = "success";
				echo json_encode($response);
			}else{
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
				$response['type'] = "warning";
				echo json_encode($response);
			}
		}
	}

	/*
	* Validación y carga de imagenes
	*/
	function validImg( $str, $name ){

		if( isset($_FILES[$name]) && $_FILES[$name]['error']==0 && $_FILES[$name]['size']>0 ){
			$this->load->library('upload');
			$this->params_imgs['file_name']=$name.'_'.time();
			$this->upload->initialize($this->params_imgs);

			if ( ! $this->upload->do_upload($name) ){
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $txt )
					$this->form_validation->set_message('validImg', '<b>%s</b> '.$txt);

				return false;
			}else{
				$file_info = $this->upload->data();
				$this->fields[$name]['value'] = $file_info['file_name'];
			}
		}

		return true;
	}

	function config_validates( ){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}

	function validEmail($email){

		if($this->id_user == 0){
			$search = $this->general->get(null,array('email'=>$email));
			if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
				$this->form_validation->set_message('validEmail', 'El email ya esta registrado');
				return false;
			}
			return true;
		}
		else{
			$search = $this->general->get(null,array('email'=>$email, 'id !='=>$this->id_user));
			if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
				$this->form_validation->set_message('validEmail', 'El email ya esta registrado');
				return false;
			}
			return true;	 
		}	   
		
	}

	function validPassword($val){
			
		if($this->metodo == "editar" && $val == ""){
			   return true;		
		}
		else{
			  if(strlen($val) < 8){
			     	$this->form_validation->set_message('validPassword', 'El password debe ser al menos de 8 caracteres');
            return false;
        }
        else if(!preg_match('/(?=[@#%&]|-|_)/', $val)){
				  	$this->form_validation->set_message('validPassword', 'El password debe contener al menos un caracter alfanumerico');
            return false;
        }
        else if(!preg_match('/(?=\d)/', $val)){
   					$this->form_validation->set_message('validPassword', 'El password debe contener al menos un digito');
            return false;
        }
        else if(!preg_match('/(?=[a-z])/', $val)){
					  $this->form_validation->set_message('validPassword', 'El password debe contener al menos una minuscula');
            return false;
        }
        else if(!preg_match('/(?=[A-Z])/', $val)){ 
					  $this->form_validation->set_message('validPassword', 'El pasword debe contener al menos una mayuscula');
            return false;
        }
		  	return true;
		}
			
  }


}
