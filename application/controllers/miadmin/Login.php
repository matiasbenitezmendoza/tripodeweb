<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	var $data = array();

  var $fields = array(
					'username' => array('value'=>'','validate' => array(
								                     'label'   => 'Email',
								                     'rules'   => 'trim|required|xss_clean|valid_email'
								                  )),
					'password' => array('value'=>'','validate' => array(
								                     'label'   => 'Password',
								                     'rules'   => 'trim|required|xss_clean'
								                  ))
				);

	function __construct(){
      parent::__construct();
  }

	public function index(){
    if($this->auth->loggedin())
      redirect('miadmin/inicio');

    $this->config_validates();
  	if( $this->form_validation->run() != FALSE ){
      $remember = $this->input->post('remember') ? TRUE : FALSE;

      $this->load->model('user_model');
      $user = $this->user_model->getLogin('email', $this->input->post('username'));

      if($user){
          if ($this->user_model->check_password($this->input->post('password'), $user[0]->password)) {
						if( $user[0]->type == "Manager" || $user[0]->type == "Administrador" ){
							$this->auth->login($user, $remember);
              redirect('miadmin/inicio');
						}else{
							$this->messages->add("El perfil del usuario no cuenta con privilegios","danger");
						}
          } else {
              $this->messages->add("El usuario y/o contraseña son incorrectos","danger");
          }
      } else {
          $this->messages->add("El usuario y/o contraseña son incorrectos","danger");
      }
    }

		$this->load->view('admin/login', $this->data);
	}

  public function logout(){
		$this->auth->logout();
        redirect('miadmin/login');
	}

  function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}
}
