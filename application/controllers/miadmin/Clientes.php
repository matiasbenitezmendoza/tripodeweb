<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	var $data = array('page'=>'clientes');
	var $id_user = 0;
	var $metodo = "agregar";


	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin() || $this->auth->usertype() != "Administrador")
      	redirect('miadmin/login');

        $this->data['user_type'] = $this->auth->usertype();
        $this->data['user_name'] = $this->auth->username();
	    $this->general->table = "reg_usuarios";
		$this->general->id = "id";
  }

	public function index(){
		
		$this->data['usuarios'] = $this->general->get(null,null);
		if (!empty($this->data['usuarios'])){
	     	$this->template->content->view('admin/clientes', $this->data);
		 	$this->template->publish('template_admin');
		}
		else{
			$this->template->content->view('admin/clientes', []);
			$this->template->publish('template_admin');
		}
	}



}
