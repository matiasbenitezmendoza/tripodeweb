<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chats extends CI_Controller {
	var $data = array('page'=>'chats');

	var $fields = array(
					'mensaje' => array('value'=>'','validate' => array(
								                     'label'   => 'Mensaje',
								                     'rules'   => 'trim|required|xss_clean'
								                  ))
				);


	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin() || $this->auth->usertype() != "Administrador")
      	redirect('miadmin/login');

      $this->data['user_type'] = $this->auth->usertype();
      $this->data['user_name'] = $this->auth->username();
			$this->general->table = "tab_chats";
			$this->general->id = "id";
    }

/*
SELECT concat(reg_usuarios.nombre,' ', reg_usuarios.apellido_paterno, ' ', reg_usuarios.apellido_materno) as usuario
FROM reg_usuarios
INNER JOIN tab_chats ON reg_usuarios.id = tab_chats.idusuario WHERE tab_chats.idusuario != -1;*/

  public function index(){

				 $this->data['chats'] = $this->general->get_user_chat();
				 if (!empty($this->data['chats'])){
		        $this->template->content->view('admin/chats', $this->data);
		        $this->template->publish('template_admin');		}
				 else{
					 $this->template->content->view('admin/chats', []);
					 $this->template->publish('template_admin');
				 }
	}

	public function search($id = null){
	
				$this->data['chats'] = $this->general->get_type_chat($id);
				if (!empty($this->data['chats'])){
		       $this->template->content->view('admin/chats', $this->data);
		       $this->template->publish('template_admin');
				}
				else{
					$this->template->content->view('admin/chats', []);
					$this->template->publish('template_admin');
				}
	}

	public function read( $id = null ){

		if($id == null){
			redirect("miadmin/chats");
		}
		else{
	  	$this->data['accion'] = 'read';
		  $this->data['id'] = $id;
		  $this->data['chats'] =  $this->general->get_chat2($id);
		  $this->general->table = "reg_usuarios";
		  $this->data['usuario'] =  $this->general->get(null,array('id'=>$id),"id as id_usuario, concat( nombre,' ', apellido_paterno,' ', apellido_materno) as usuario, email");
		  if (!empty($this->data['chats'])){
			    $this->general->update_status_chat($id);
       		$this->template->content->view('admin/chats_read', $this->data);
		      $this->template->publish('template_admin');
      }else{
            redirect("miadmin/chats");
	  	}
		}	

	}

	public function agregar( $id = null ){

		$this->data['accion'] = 'agregar';
		$this->data['id'] = $id;
		$this->general->table = "reg_usuarios";
		$this->data['usuario'] =  $this->general->get(null,array('id'=>$id),"id as id_usuario, concat( nombre,' ', apellido_paterno,' ', apellido_materno) as usuario, email");
		$this->general->table = "tab_chats";


		$this->config_validates();
		if($this->form_validation->run() != FALSE){
			$this->fields['status_chat']['value'] = 2;
			$this->fields['respuesta']['value'] = $id;
			$this->fields['id_usuario']['value'] = -1;
			$this->general->insert($this->fields);
			$this->messages->add("Se ha enviado el mensaje exitosamente.","info");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;

	  if (!empty($this->data['usuario'])){
        		$this->template->content->view('admin/chats_form', $this->data);
		        $this->template->publish('template_admin');
    }else{
            redirect("miadmin/chats");
		}
		

	}

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}


}
