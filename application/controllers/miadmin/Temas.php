<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temas extends CI_Controller {
	var $data = array('page'=>'temas');

	var $fields = array(
					'nombre' => array('value'=>'','validate' => array(
								                     'label'   => 'Nombre',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'descripcion' => array('value'=>'','validate' => array(
								                     'label'   => 'Descripcion',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'imagen' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagen]'
								                  ))
				);

	var $params_imgs = array(
							'upload_path' => './imgs/temas/',
							'allowed_types' => 'jpg|png|jpeg|gif',
							'file_name' => '',
							'overwrite' => TRUE,
						);

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin() || $this->auth->usertype() != "Administrador")
      	redirect('miadmin/login');

      $this->data['user_type'] = $this->auth->usertype();
      $this->data['user_name'] = $this->auth->username();
			$this->general->table = "cat_temas";
			$this->general->id = "id";
  }

	public function index(){
		$this->data['temas'] = $this->general->get(null,array('status'=>1));
		if (!empty($this->data['temas'])){
	    	$this->template->content->view('admin/temas', $this->data);
			$this->template->publish('template_admin');
		}
		else{
			$this->template->content->view('admin/temas', []);
		    $this->template->publish('template_admin');
		}
	}

	public function agregar( $id = null ){
		$this->data['accion'] = 'Agregar';
		$this->data['id'] = "";
		$this->config_validates();
		if($this->form_validation->run() != FALSE){
		//	$this->fields['password']['value'] = md5($this->fields['password']['value']);
			$this->general->insert($this->fields);
			$this->messages->add("Se ha guardado exitosamente la información.","info");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;
        $this->template->content->view('admin/temas_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function editar( $id = null ){

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;
		$this->config_validates();
		if ($this->form_validation->run() != FALSE){

			if( $this->fields['imagen']['value'] == '' )
				unset($this->fields['imagen']);
      
			$info = array();
           
            foreach($this->fields as $key => $value)
                $info[$key] = $value['value'];

			if( $this->general->update($this->fields,$id) ){
				$this->messages->add("Se ha guardado la información correctamente","info");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = null;

			}else{
				$this->messages->add("No se pudo guardar la información, inténtelo más tarde","danger");
			}
		}

		$info = $this->general->get( $id );
		if( !isset($info[0]->nombre) )
			redirect("miadmin/temas");

		$this->fields['imagen']['value'] = '';
		foreach( $this->fields as $key=>$field ){
			$this->fields[$key]['value'] = $info[0]->$key;
		}

		$this->data['fields'] = $this->fields;
		$this->template->content->view('admin/temas_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function eliminar( $id = null ){
		$response = array();
		$id = strip_tags($this->input->post("entrada",true));
        if( $id == null || !is_numeric($id) ){
			$response['titulo'] = "¡Eliminación!";
			$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
			$response['type'] = "warning";
			echo json_encode($response);
		}else{
			if( $this->general->delete( $id ) ){
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "Se elimino la información correctamente.";
				$response['type'] = "success";
				echo json_encode($response);
			}else{
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
				$response['type'] = "warning";
				echo json_encode($response);
			}
		}
	}

	/*
	* Validación y carga de imagenes
	*/
	function validImg( $str, $name ){

		if( isset($_FILES[$name]) && $_FILES[$name]['error']==0 && $_FILES[$name]['size']>0 ){
			$this->load->library('upload');
			$this->params_imgs['file_name']=$name.'_'.time();
			$this->upload->initialize($this->params_imgs);

			if ( ! $this->upload->do_upload($name) ){
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $txt )
					$this->form_validation->set_message('validImg', '<b>%s</b> '.$txt);

				return false;
			}else{
				$file_info = $this->upload->data();
				$this->fields[$name]['value'] = $file_info['file_name'];
			}
		}

		return true;
	}

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}
}
