<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {
	var $data = array('page'=>'pedidos');

	var $fields = array(
		'status_pedido' => array('value'=>'','validate' => array(
										 'label'   => 'Status del pedido',
										 'rules'   => 'trim|required|xss_clean'
									   )),
		'receta_requerida' => array('value'=>'','validate' => array(
										'label'   => 'Receta del pedido',
										'rules'   => 'xss_clean'
									  )),							   
		'id_repartidor' => array('value'=>'','validate' => array(
											'label'   => 'repartidor',
											'rules'   => 'trim|required|xss_clean'
										))
	);

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin() || $this->auth->usertype() != "Administrador")
      	redirect('miadmin/login');

            $this->data['user_type'] = $this->auth->usertype();
            $this->data['user_name'] = $this->auth->username();
			$this->general->table = "tab_pedidos";
			$this->general->id = "id";
			$this->load->library('Firebase');

    }

	public function index(){
		$this->data['pedidos'] = $this->general->getPedidos(null);
		if (!empty($this->data['pedidos'])){
 	    	$this->template->content->view('admin/pedidos', $this->data);
			$this->template->publish('template_admin');
		}
		else{
			$this->template->content->view('admin/pedidos', []);
			$this->template->publish('template_admin');
		}
	}

	public function editar( $id = null ){

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;

		$this->data['pedidos'] = $this->general->getPedidos($id);

		if( !isset($this->data['pedidos'][0]->orden) )
	     	redirect("miadmin/pedidos");

		if( $this->data['pedidos'][0]->id_repartidor != "-1" ){
			$this->general->table = "reg_repartidores";
			$this->data['repartidor'] = $this->general->get($this->data['pedidos'][0]->id_repartidor);
			unset($this->fields['id_repartidor']);
			unset($this->fields['receta_requerida']);
		}
		else{
			$this->data['repartidor'] = [];
		}

		$this->general->table = "reg_repartidores";
		$this->data['repartidores'] =  $this->general->get(null,array('status_rep'=> 1 ),"id, concat(nombre,' ', apellido_paterno,' ',apellido_materno) as repartidor");

		if (empty($this->data['repartidores'])){
			$this->data['repartidores'] = [];
		}

		$this->config_validates();
		if ($this->form_validation->run() != FALSE){

			$info = array();
            foreach($this->fields as $key => $value)
				$info[$key] = $value['value'];
				
			$disponible = true;	
			$repartidor_id = "-1";
			$search = [];
			$noti_rep = false;
			if( $this->data['pedidos'][0]->id_repartidor != "-1" ){
				    
					$disponible = true;
					$repartidor_id = $this->data['pedidos'][0]->id_repartidor;
			}else{
			     	$noti_rep = true;
    				$repartidor_id = $this->fields['id_repartidor']['value'];
     				$this->general->table = "reg_repartidores";
					$reg = $this->general->get(null,array('status_rep'=> 2,'id'=> $this->fields['id_repartidor']['value'] ),"id");
					if(!empty($reg)){
   						$disponible = false;
					}else{
						$disponible = true;	
					}
			}

			if($disponible){
								$this->general->table = "tab_pedidos";
								$search = [];
								if($this->fields['status_pedido']['value'] == 2){
									$search =  $this->general->get(null,array('id_repartidor'=>$repartidor_id, 'status_pedido'=>2, 'id !='=> $id ), "id, id_repartidor");
								}

								if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
									    $this->messages->add("No se puede actualizar el pedido con status 'Saliendo a tu Dirección', el repartidor ya cuenta con 1 pedido en ruta","danger");										
								}else{
            									$this->general->table = "tab_pedidos";
												if( $this->general->update($this->fields,$id) ){
													$this->messages->add("Se ha guardado la información correctamente","info");
													foreach( $this->fields as $key=>$field )
														$this->fields[$key]['value'] = null;

													$this->data['pedidos'] = $this->general->getPedidos($id);
													if( $this->data['pedidos'][0]->id_repartidor != "-1" ){
															$this->general->table = "reg_repartidores";
															$this->data['repartidor'] = $this->general->get($this->data['pedidos'][0]->id_repartidor);
													}

													if($noti_rep == true){
														$this->general->table = "reg_repartidores";
														$busqueda =  $this->general->get($repartidor_id,null,"id, token_firebase");
												        $token = $busqueda[0]->token_firebase;
														$resu = $this->firebase->sendNotificationAnd($token, "Nuevo pedido asignado", 0);
													}
													
												}else{
													$this->messages->add("No se pudo guardar la información, inténtelo más tarde","danger");
												}
								}
			}else{
						$this->messages->add("No se puede asignar el repartidor, ya se encuentra en ruta","danger");
			}
		}


		$this->general->id = "id_pedido";
        $this->general->table = "tab_detalles_pedidos";
		$this->data['detalles'] = $this->general->get($this->data['pedidos'][0]->id, "*");
		if (empty($this->data['detalles'])){
			$this->data['detalles'] = [];
		}


		$this->data['fields'] = $this->fields;
		$this->template->content->view('admin/pedidos_form', $this->data);
		$this->template->publish('template_admin');
	}

	

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}
}
