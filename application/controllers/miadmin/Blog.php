<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	var $data = array('page'=>'blog');

	var $fields = array(
					'titulo' => array('value'=>'','validate' => array(
								                     'label'   => 'Título',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'slug' => array('value'=>'','validate' => array(
								                     'label'   => 'slug',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'introduccion' => array('value'=>'','validate' => array(
								                     'label'   => 'Introducción',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'contenido' => array('value'=>'','validate' => array(
								                     'label'   => 'Contenido',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'destacada' => array('value'=>'','validate' => array(
								                     'label'   => 'Desracada',
								                     'rules'   => 'trim|xss_clean|numeric'
								                  )),
					'keywords' => array('value'=>'','validate' => array(
								                     'label'   => 'keywords',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'description' => array('value'=>'','validate' => array(
								                     'label'   => 'description',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'imagen' => array('value'=>'','validate' => array(
								                     'label'   => 'Imagen',
								                     'rules'   => 'trim|xss_clean|callback_validImg[imagen]'
								                  ))
				);

	var $params_imgs = array(
							'upload_path' => './blog/',
							'allowed_types' => 'jpg|png|jpeg|gif',
							'file_name' => '',
							'overwrite' => TRUE,
						);

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin())
      	redirect('miadmin/login');

			$this->data['user_type'] = $this->auth->usertype();
      $this->data['user_name'] = $this->auth->username();
      $this->general->table = "tab_entradas";
      $this->general->id = "identrada";
  }

	public function index(){
		$this->data['entradas'] = $this->general->get();
		$this->template->content->view('admin/blog', $this->data);
		$this->template->publish('template_admin');
	}

	public function agregar( $id = null ){
		$this->data['accion'] = 'Agregar';
		$this->data['id'] = "";
		$this->config_validates();
		if($this->form_validation->run() != FALSE){
			$this->fields['slug']['value'] = _slug($this->fields['titulo']['value']);
			$this->general->insert($this->fields);
			$this->messages->add("Se ha guardado exitosamente la información.","success");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;
    $this->template->content->view('admin/blog_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function editar( $id = null ){

		if( !is_numeric($id) )
			redirect("miadmin/blog");

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;
		$this->config_validates();
		if ($this->form_validation->run() != FALSE){

			if( $this->fields['imagen']['value'] == '' )
				unset($this->fields['imagen']);

			$info = array();
      foreach($this->fields as $key => $value)
        $info[$key] = $value['value'];

			$this->fields['slug']['value'] = _slug($this->fields['titulo']['value']);
			if( $this->general->update($this->fields,$id) ){
				$this->messages->add("Se ha guardado la información correctamente","success");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = null;

			}else{
				$this->messages->add("No se pudo guardar la información, inténtelo más tarde","error");
			}
		}

		$info = $this->general->get( $id );
		if( !isset($info[0]->titulo) )
			redirect("miadmin/blog");

		$this->fields['imagen']['value'] = '';
		foreach( $this->fields as $key=>$field ){
			$this->fields[$key]['value'] = $info[0]->$key;
		}

		$this->data['fields'] = $this->fields;
		$this->template->content->view('admin/blog_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function eliminar( $id = null ){
		$response = array();
		$id = strip_tags($this->input->post("entrada",true));
    if( $id == null || !is_numeric($id) ){
			$response['titulo'] = "¡Eliminación!";
			$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
			$response['type'] = "warning";
			echo json_encode($response);
		}else{
			if( $this->general->delete( $id ) ){
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "Se elimino la información correctamente.";
				$response['type'] = "success";
				echo json_encode($response);
			}else{
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
				$response['type'] = "warning";
				echo json_encode($response);
			}
		}
	}

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}

	function uploadimage(){
		$this->validImg("","upload");
		echo '<script>window.parent.CKEDITOR.tools.callFunction(CKEditorFuncNum, "'.base_url("blog/".$this->fields["upload"]['value']).'");</script>';
	}

	function show_img(){
		$this->load->helper('file');
		$map = get_filenames('./blog');
		$this->load->view('admin/browserfiles', array( 'imgs' => $map ));
	}

	/*
	* Validación y carga de imagenes
	*/
	function validImg( $str, $name ){

		if( $_FILES[$name]['error']==0 && $_FILES[$name]['size']>0 ){
			$this->load->library('upload');
			$this->params_imgs['file_name']=$name.'_'.time();
			$this->upload->initialize($this->params_imgs);

			if ( ! $this->upload->do_upload($name) ){
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $txt )
					$this->form_validation->set_message('validImg', '<b>%s</b> '.$txt);

				return false;
			}else{
				$file_info = $this->upload->data();
				$this->fields[$name]['value'] = $file_info['file_name'];
			}
		}

		return true;
	}
}
