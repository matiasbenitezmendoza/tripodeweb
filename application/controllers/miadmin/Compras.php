<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	var $data = array('page'=>'compras');

	var $filtro = array();
	var $fields = array(
					'fecha' => array('value'=>'','validate' => array(
								                     'label'   => 'Fecha',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'slug' => array('value'=>'','validate' => array(
								                     'label'   => 'Slug',
								                     'rules'   => 'trim|xss_clean'
								                  )),
				);

	var $params_imgs = array(
							'upload_path' => './imgs/compras/',
							'allowed_types' => 'jpg|png|jpeg|gif',
							'file_name' => '',
							'overwrite' => TRUE,
						);

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin())
      	redirect('miadmin/login');

      if($this->auth->usertype() == "Manager"){
					$this->filtro['manager'] = $this->auth->userid();
			}

      $this->data['user_type'] = $this->auth->usertype();
      $this->data['user_name'] = $this->auth->username();
			$this->general->table = "tab_compras";
			$this->general->id = "idcompra";
  }

	public function index(){
		$this->data['compras'] = $this->general->getCompra(null,$this->filtro);
		$this->template->content->view('admin/compras', $this->data);
		$this->template->publish('template_admin');
	}

	public function agregar( $id = null ){
		$this->data['accion'] = 'Agregar';
		$this->data['id'] = "";
		$this->config_validates();
		if($this->form_validation->run() != FALSE){
			$this->fields['slug']['value'] = _slug($this->fields['nombre']['value']);
			$this->general->insert($this->fields);
			$this->messages->add("Se ha guardado exitosamente la información.","info");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;
    $this->template->content->view('admin/compras_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function editar( $id = null ){

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;
		$this->config_validates();
		if ($this->form_validation->run() != FALSE){
			$this->fields['slug']['value'] = _slug($this->fields['nombre']['value']);

			$info = array();
      foreach($this->fields as $key => $value)
        $info[$key] = $value['value'];

			if( $this->general->update($this->fields,$id) ){
				$this->messages->add("Se ha guardado la información correctamente","info");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = null;

			}else{
				$this->messages->add("No se pudo guardar la información, inténtelo más tarde","danger");
			}
		}

		$info = $this->general->getCompra( $id,$this->filtro );
		if( !isset($info[0]->fecha) )
			redirect("miadmin/compras");

		$this->data['fields'] = $info;
		$this->template->content->view('admin/compras_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function eliminar( $id = null ){
		$response = array();
		$id = strip_tags($this->input->post("entrada",true));
    if( $id == null || !is_numeric($id) ){
			$response['titulo'] = "¡Eliminación!";
			$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
			$response['type'] = "warning";
			echo json_encode($response);
		}else{
			if( $this->general->delete( $id ) ){
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "Se elimino la información correctamente.";
				$response['type'] = "success";
				echo json_encode($response);
			}else{
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
				$response['type'] = "warning";
				echo json_encode($response);
			}
		}
	}

	/*
	* Validación y carga de imagenes
	*/
	function validImg( $str, $name ){

		if( isset($_FILES[$name]) && $_FILES[$name]['error']==0 && $_FILES[$name]['size']>0 ){
			$this->load->library('upload');
			$this->params_imgs['file_name']=$name.'_'.time();
			$this->upload->initialize($this->params_imgs);

			if ( ! $this->upload->do_upload($name) ){
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $txt )
					$this->form_validation->set_message('validImg', '<b>%s</b> '.$txt);

				return false;
			}else{
				$file_info = $this->upload->data();
				$this->fields[$name]['value'] = $file_info['file_name'];
			}
		}

		return true;
	}

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}
}
