<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	var $data = array('page'=>'inicio');
	var $filtro = array();

	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin())
      	redirect('miadmin/login');

			if($this->auth->usertype() == "Manager"){
					$this->filtro['manager'] = $this->auth->userid();
			}

			$month_ini = date("Y-m-d", strtotime('first day of January '.date('Y') ));
			$month_end = date("Y-m-d");
			$this->filtro['c.fecha BETWEEN "'.$month_ini. '" and "'.$month_end.'"'] = null;

		$this->data['user_type'] = $this->auth->usertype();
		$this->data['user_name'] = $this->auth->username();
  }

	public function index(){
		$total_pedidos = $this->general->getTotalP("tab_pedidos");
		$this->data['total_pedidos'] =  $total_pedidos[0]->total;

		$total_clientes = $this->general->getTotal("reg_usuarios");
		$this->data['total_clientes'] =  $total_clientes[0]->total;

		$total_chat = $this->general->getTotalChat();
		$this->data['total_chat'] =  $total_chat[0]->total;

		$totales_pedidos_activos = $this->general->getTotalPedidos(1);
		$this->data['totales_pedidos_activos'] =  $totales_pedidos_activos[0]->total;

		$totales_pedidos_ruta = $this->general->getTotalPedidos(2);
		$this->data['totales_pedidos_ruta'] =  $totales_pedidos_ruta[0]->total;

		$totales_pedidos_finalizados = $this->general->getTotalPedidos(3);
		$this->data['totales_pedidos_finalizados'] =  $totales_pedidos_finalizados[0]->total;

		$this->data['pedidos'] = $this->general->getPedidosRecientes(null);
		if (!empty($this->data['pedidos'])){
 	    	$this->template->content->view('admin/inicio', $this->data);
			$this->template->publish('template_admin');
		}
		else{
			$this->data['pedidos'] = [];
			$this->template->content->view('admin/inicio', $this->data);
			$this->template->publish('template_admin');
		}
		
	}
}
