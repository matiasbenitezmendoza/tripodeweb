<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Residencias extends CI_Controller {
	var $data = array('page'=>'residencias');

	var $fields = array(
					'nombre' => array('value'=>'','validate' => array(
													 'label'   => 'Nombre',
													 'rules'   => 'trim|required|xss_clean|callback_validResidencia[nombre]'
								                  )),
					'latitud' => array('value'=>'','validate' => array(
					                                 'label'   => 'Latitud',
						                             'rules'   => 'trim|required|xss_clean'
					                              )),
					'longitud' => array('value'=>'','validate' => array(
					                                 'label'   => 'Longitud',
						                             'rules'   => 'trim|required|xss_clean'
					                              ))
				);

				
	function __construct(){
      parent::__construct();

      if(!$this->auth->loggedin() || $this->auth->usertype() != "Administrador")
      	redirect('miadmin/login');

        $this->data['user_type'] = $this->auth->usertype();
        $this->data['user_name'] = $this->auth->username();
		$this->general->table = "tab_residencias";
		$this->general->id = "id";

    }

	public function index(){
		$this->data['residencias'] = $this->general->get(null,array('status'=>1));
		if (!empty($this->data['residencias'])){
 	    	$this->template->content->view('admin/residencias', $this->data);
			$this->template->publish('template_admin');
		}
		else{
			$this->template->content->view('admin/residencias', []);
			$this->template->publish('template_admin');
		}
	}

	public function agregar( $id = null ){
		$this->data['accion'] = 'Agregar';
		$this->data['id'] = "";
		$this->config_validates();
		if($this->form_validation->run() != FALSE){
			$this->general->insert($this->fields);
			$this->messages->add("Se ha guardado exitosamente la información.","info");

			foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = null;
		}

		$this->data['fields'] = $this->fields;
        $this->template->content->view('admin/residencias_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function editar( $id = null ){

		$this->data['accion'] = 'Editar';
		$this->data['id'] = $id;
		$this->config_validates();
		if ($this->form_validation->run() != FALSE){

      
			$info = array();
            foreach($this->fields as $key => $value)
                $info[$key] = $value['value'];

			if( $this->general->update($this->fields,$id) ){
				$this->messages->add("Se ha guardado la información correctamente","info");
				foreach( $this->fields as $key=>$field )
					$this->fields[$key]['value'] = null;

			}else{
				$this->messages->add("No se pudo guardar la información, inténtelo más tarde","danger");
			}
		}

		$info = $this->general->get( $id );
		if( !isset($info[0]->nombre) )
			redirect("miadmin/residencias");

		foreach( $this->fields as $key=>$field ){
			$this->fields[$key]['value'] = $info[0]->$key;
		}

		$this->data['fields'] = $this->fields;
		$this->template->content->view('admin/residencias_form', $this->data);
		$this->template->publish('template_admin');
	}

	public function eliminar( $id = null ){
		$response = array();
		$id = strip_tags($this->input->post("entrada",true));
        if( $id == null || !is_numeric($id) ){
			$response['titulo'] = "¡Eliminación!";
			$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
			$response['type'] = "warning";
			echo json_encode($response);
		}else{
			if( $this->general->delete( $id ) ){
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "Se elimino la información correctamente.";
				$response['type'] = "success";
				echo json_encode($response);
			}else{
				$response['titulo'] = "¡Eliminación!";
				$response['texto'] = "No se pudo eliminar la informaicón, intentelo más tarde.";
				$response['type'] = "warning";
				echo json_encode($response);
			}
		}
	}

	function config_validates( ){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}

	function _RandomString($length=10,$uc=TRUE,$n=TRUE,$sc=FALSE){
		$source = 'abcdefghijklmnopqrstuvwxyz';
		if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
		if($length>0){
			$rstr = "";
			$source = str_split($source,1);
			for($i=1; $i<=$length; $i++){
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1,count($source));
				$rstr .= $source[$num-1];
			}
	
		}
		return $rstr;
	}

	function validResidencia($nombre){
			$search = $this->general->get(null,array('nombre'=>$nombre));
			if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
				$this->form_validation->set_message('validResidencia', 'No puede guardar 2 residencias con el mismo nombre');
				return false;
			}
			return true;
	}

}
