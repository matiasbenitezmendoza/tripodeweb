<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Pedidos extends REST_Controller {

  var $params_imgs = array(
    'upload_path' => './imgs/recetas/',
    'allowed_types' => 'jpg|png|jpeg|gif',
    'file_name' => '',
    'overwrite' => TRUE,
  );


  function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['pedidos_repartidor_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['pedidos_activos_repartidor_get']['limit'] = 50000; // 500 requests per hour per user/key

        $this->methods['users_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "tab_pedidos";
        $this->general->id = "id";
        $this->load->library('Authorization');
        $this->load->library('Firebase');

  }

  public function pedido_id_get(){

    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $id = $this->get('id');
        if ($id <= 0){
          return $this->response([
            'status' => FALSE,
            'message' => 'Pedido could not be found',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }

        $pedidos =  $this->general->get(null, array('id'=>$id ), "id, id_usuario, id_repartidor, orden, fecha_pedido, status_pedido, comentarios_usuario, comentarios_repartidor, calificacion_usuario, calificacion_repartidor, tipo_pago, status_pago, calle, numero_exterior, numero_interior, residencial, codigo_postal, estado, colonia, ciudad, referencia, rfc, razon_social, calle_fac, numero_exterior_fac, numero_interior_fac, codigo_postal_fac, estado_fac, colonia_fac, municipio_fac, tipo_envio, status as detalle");

        if (!empty($pedidos)){
                  foreach ($pedidos as $clave => $valor) {
                      $valor->detalle =  $this->_detalle_pedidos($valor->id); 
                  }
                  
                  return $this->response([
                          'status' => TRUE,
                          'message' => '',
                          'data' => $pedidos[0]
                      ], REST_Controller::HTTP_OK); 
        }else{
                return $this->set_response([
                    'status' => TRUE,
                    'message' => 'Sin pedidos',
                    'data' => json_decode ("{}")
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

      }    

      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  public function pedidos_get(){

    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => []);
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $id = (int) $id;
      $limit = $this->get('limit');
      $offset = $this->get('offset');

        if ($limit == "" || (int) $limit <= 0 || $offset == ""){
          return $this->response([
            'status' => FALSE,
            'message' => 'Parametros limit & offset requeridos',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }


        if ($id <= 0){
          return $this->response([
            'status' => FALSE,
            'message' => 'Pedido could not be found',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }
        $this->general->id = "id_usuario";
        $pedidos =  $this->general->getPaginacionPedidos($id,  "id, id_usuario, id_repartidor, orden, fecha_pedido, status_pedido, comentarios_usuario, comentarios_repartidor, calificacion_usuario, calificacion_repartidor, tipo_pago, status_pago, calle, numero_exterior, numero_interior, residencial, codigo_postal, estado, colonia, ciudad, referencia, rfc, razon_social, calle_fac, numero_exterior_fac, numero_interior_fac, codigo_postal_fac, estado_fac, colonia_fac, municipio_fac, tipo_envio, status as detalle", $limit, $offset);

        if (!empty($pedidos)){


				foreach ($pedidos as $clave => $valor) {
						$valor->detalle =  $this->_detalle_pedidos($valor->id); 
			    }

                return $this->response([
                         'status' => TRUE,
                         'message' => '',
                         'data' => $pedidos
                    ], REST_Controller::HTTP_OK); 
        }else{
            return $this->set_response([
                'status' => TRUE,
                'message' => 'Sin pedidos',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

      }    

      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  public function pedidos_finalizados_get(){

    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => []);
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id = $usuario->id;
        $id = (int) $id;

        if ($id <= 0){
          return $this->response([
            'status' => FALSE,
            'message' => 'Pedido could not be found',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }

        $this->general->id = "id_usuario";
        $pedidos =  $this->general->getPedidosFinalizados($id, "id, id_usuario, id_repartidor, orden, fecha_pedido, status_pedido, comentarios_usuario, comentarios_repartidor, calificacion_usuario, calificacion_repartidor, tipo_pago, status_pago, calle, numero_exterior, numero_interior, residencial, codigo_postal, estado, colonia, ciudad, referencia, rfc, razon_social, calle_fac, numero_exterior_fac, numero_interior_fac, codigo_postal_fac, estado_fac, colonia_fac, municipio_fac, tipo_envio, status as detalle");

        if (!empty($pedidos)){
          
                foreach ($pedidos as $clave => $valor) {
                      $valor->detalle =  $this->_detalle_pedidos($valor->id); 
                }

                return $this->response([
                         'status' => TRUE,
                         'message' => '',
                         'data' => $pedidos
                    ], REST_Controller::HTTP_OK); 
        }else{
            return $this->set_response([
                'status' => TRUE,
                'message' => 'Sin pedidos',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

      }    

      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  private function _detalle_pedidos($id_pedido){
        $this->general->id = "id_pedido";
        $this->general->table = "tab_detalles_pedidos";

        $pedido =  $this->general->get($id_pedido, "*");

        if (!empty($pedido)){
              return $pedido;
        }else{
            return [];
        }
  }
  
  public function pedidos_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=>json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $id_usuario_ldcom = $usuario->id_usuario_ldcom;
      $id = (int) $id;
      $data = array(
          'id_usuario' => array('value'=>$id),
          'id_repartidor' => array('value'=>'-1'),
          'orden' => array('value'=>'0'),
          'fecha_pedido' => array('value'=>date("Y-m-d H:i:s")),
          'status_pedido' => array('value'=>$this->post('status_pedido')),
          'tipo_pago' => array('value'=>$this->post('tipo_pago')),
          'status_pago' => array('value'=>$this->post('status_pago')),
          'calle' => array('value'=>$this->post('calle')),   
          'numero_exterior' => array('value'=>$this->post('numero_exterior')),
          'numero_interior' => array('value'=>$this->post('numero_interior')),
          'residencial' => array('value'=>$this->post('residencial')),
          'codigo_postal' => array('value'=>$this->post('codigo_postal')),
          'estado' => array('value'=>$this->post('estado')),
          'colonia' => array('value'=>$this->post('colonia')),
          'ciudad' => array('value'=>$this->post('ciudad')),
          'referencia' => array('value'=>$this->post('referencia')),
          'rfc' => array('value'=>$this->post('rfc')),
          'razon_social' => array('value'=>$this->post('razon_social')),
          'calle_fac' => array('value'=>$this->post('calle_fac')),   
          'numero_exterior_fac' => array('value'=>$this->post('numero_exterior_fac')),
          'numero_interior_fac' => array('value'=>$this->post('numero_interior_fac')),
          'codigo_postal_fac' => array('value'=>$this->post('codigo_postal_fac')),
          'estado_fac' => array('value'=>$this->post('estado_fac')),
          'colonia_fac' => array('value'=>$this->post('colonia_fac')),
          'municipio_fac' => array('value'=>$this->post('municipio_fac')),
          'tipo_envio' => array('value'=>$this->post('tipo_envio')),
          'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );

      $id_direccion = $this->post('id_direccion');  
      $monto = $this->post('monto');  
      $tarjeta = $this->post('tarjeta');  
      $mes_tarjeta = $this->post('mes_tarjeta');  
      $anio_tarjeta = $this->post('anio_tarjeta');  

      if( $data['id_repartidor']['value'] != "" && $data['status_pedido']['value'] != ""  && $monto != "" && $data['tipo_pago']['value'] != "" && $data['status_pago']['value'] != "" && $data['tipo_envio']['value'] != "" ){
                
            $id_insertd = $this->general->insert($data);
            if($id_insertd){
                     $respuesta_ldcom =  $this->_orden($data, $id_usuario_ldcom, $id_direccion, $monto, $tarjeta, $mes_tarjeta, $anio_tarjeta);
                     $json_r = json_decode ($respuesta_ldcom);

                     if($json_r->IsSuccessful == true){
                                $orden = substr($json_r->Data, 3);  
                                $data_update = array(
                                                          'orden' => array('value'=>$orden)
                                                        );

                                  $update = $this->general->update($data_update, $id_insertd);
                                  if($update){
                                                  $busqueda = $this->general->get(null,array('id'=>$id_insertd),"*");
                                                  $response['status'] = TRUE;
                                                  $response['message'] = "";
                                                  $response['data'] = $busqueda[0];
                                                  return $this->set_response($response, REST_Controller::HTTP_OK);
                                  }else{
                                                  $response['status'] = FALSE;
                                                  $response['message'] = 'No se pudo procesar el registro, inténtalo más tarde 1.';
                                                  $response['data'] = json_decode ("{}");
                                                  return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                  }

                     }else{
                              $response['status'] = FALSE;
                              $response['message'] = $json_r->Message;
                              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                     }





                /*
                $busqueda = $this->general->get(null,array('id'=>$id_insertd),"*");
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $busqueda[0];
                return $this->set_response($response, REST_Controller::HTTP_OK);
                */
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }
        
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
         return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }


    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    
  }

  public function detalle_pedidos_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=>json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $id = (int) $id;
      $data = array(
          'id_pedido' => array('value'=>$this->post('id_pedido')),
          'id_producto' => array('value'=>$this->post('id_producto')),
          'id_carrito' => array('value'=>$this->post('id_carrito')),
          'orden' => array('value'=>$this->post('orden')),
          'producto' => array('value'=>$this->post('producto')),
          'cantidad' => array('value'=>$this->post('cantidad')),   
          'precio' => array('value'=>$this->post('precio')),
          'precio_total' => array('value'=>$this->post('precio_total')),
          'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );

      if( $data['id_pedido']['value'] != "" && $data['id_producto']['value'] != "" && $data['id_carrito']['value'] != "" && $data['orden']['value'] != "" && $data['producto']['value'] != "" && $data['cantidad']['value'] != "" && $data['precio']['value'] != "" && $data['precio_total']['value'] != "" ){
            $this->general->table = "tab_detalles_pedidos";   
            $id_insertd = $this->general->insert($data);
            if($id_insertd){
                $busqueda = $this->general->get(null,array('id'=>$id_insertd),"*");
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $busqueda[0];
                return $this->set_response($response, REST_Controller::HTTP_OK);
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }
        
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
         return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }


    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    
  }

  public function calificar_usuario_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

       $usuario = $response_token['data'];
       $idusuario = $usuario->id;
       $idusuario = (int) $idusuario;

      $data = array(
          'id' => array('value'=>$this->put('id')),
          'comentarios_usuario' => array('value'=>$this->put('comentarios_usuario')),
          'calificacion_usuario' => array('value'=>$this->put('calificacion_usuario'))
      );
      
        if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {

          $search = $this->general->get(null,array('id'=>$data['id']['value']));
          
          if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
             $update = $this->general->update($data, $data["id"]["value"]);
             if($update){
                       $busqueda = $this->general->get(null,array('id'=>$data["id"]["value"]),"*");
                        $response['status'] = TRUE;
                        $response['message'] = '';
                        $response['data'] = $busqueda[0];
                        return $this->set_response($response, REST_Controller::HTTP_OK);
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }

          }else{
            $response['status'] = FALSE;
            $response['message'] = 'El id no esta registrado';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => null
      ]; 
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA'; 
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  //repartidor
  public function pedidos_repartidor_finalizados_get(){
    
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $limit = $this->get('limit');
      $offset = $this->get('offset');

        if ($limit == "" || (int) $limit <= 0 || $offset == ""){
          return $this->response([
            'status' => FALSE,
            'message' => 'Parametros limit & offset requeridos',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }


        if ($id <= 0){
          return $this->response([
            'status' => FALSE,
            'message' => 'Pedido could not be found',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }


        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.
        $pedido =  $this->general->getPedidosRepartidorFinalizado($id, $limit, $offset);

        if (!empty($pedido)){
              return $this->response([
                         'status' => TRUE,
                         'message' => '',
                         'data' => $pedido
                    ], REST_Controller::HTTP_OK); 
        }else{
            $this->set_response([
                'status' => TRUE,
                'message' => 'Sin pedidos',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

      }    

      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  public function pedidos_repartidor_ruta_get(){
    
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $limit = $this->get('limit');
      $offset = $this->get('offset');

        if ($limit == "" || (int) $limit <= 0 || $offset == ""){
          return $this->response([
            'status' => FALSE,
            'message' => 'Parametros limit & offset requeridos',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }


        if ($id <= 0){
          return $this->response([
            'status' => FALSE,
            'message' => 'Pedido could not be found',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }


        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.
        $pedido =  $this->general->getPedidosRepartidorRuta($id);

        if (!empty($pedido)){
              return $this->response([
                         'status' => TRUE,
                         'message' => '',
                         'data' => $pedido
                    ], REST_Controller::HTTP_OK); 
        }else{
            $this->set_response([
                'status' => TRUE,
                'message' => 'Sin pedidos',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

      }    

      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }
  
  public function pedidos_repartidor_get(){
    
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> []);
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $limit = $this->get('limit');
      $offset = $this->get('offset');

        if ($limit == "" || (int) $limit <= 0 || $offset == ""){
          return $this->response([
            'status' => FALSE,
            'message' => 'Parametros limit & offset requeridos',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }

        if ($id <= 0){
          return $this->response([
            'status' => FALSE,
            'message' => 'Pedido could not be found',
            'data' => []
           ], REST_Controller::HTTP_BAD_REQUEST); 
        }


        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.
        $pedido =  $this->general->getPedidosRepartidor($id);

        if (!empty($pedido)){
              return $this->response([
                         'status' => TRUE,
                         'message' => '',
                         'data' => $pedido
                    ], REST_Controller::HTTP_OK); 
        }else{
            $this->set_response([
                'status' => TRUE,
                'message' => 'Sin pedidos',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

      }    

      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  public function calificar_repartidor_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

       $usuario = $response_token['data'];
       $idrepartidor = $usuario->id;
       $idrepartidor = (int) $idrepartidor;

      $data = array(
          'id' => array('value'=>$this->put('id')),
          'comentarios_repartidor' => array('value'=>$this->put('comentarios_repartidor')),
          'calificacion_repartidor' => array('value'=>$this->put('calificacion_repartidor'))
      );
      
        if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {

          $search = $this->general->get(null,array('id'=>$data['id']['value'], 'id_repartidor'=>$idrepartidor ));
          
          if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
             $update = $this->general->update($data, $data["id"]["value"]);
             if($update){
                $r = $this->general->get(null,array('id'=>$data["id"]["value"]),"*");
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $r[0];
                return $this->set_response($response, REST_Controller::HTTP_OK);
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                $response['data'] = json_decode ("{}");
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }

          }else{
            $response['status'] = FALSE;
            $response['message'] = 'El id no esta registrado';
            $response['data'] = json_decode ("{}");
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          $response['data'] = json_decode ("{}");
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => json_decode ("{}")
      ]; 
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA'; 
    $response['data'] = json_decode ("{}");
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  public function pedidos_repartidor_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

       $usuario = $response_token['data'];
       $idusuario = $usuario->id;

      $data = array(
          'id' => array('value'=>$this->put('id')),
          'status_pedido' => array('value'=>$this->put('status_pedido')),
          'status_pago' => array('value'=>1)
      );
      
        if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {

          $search = $this->general->get(null,array('id'=>$data['id']['value'], 'id_repartidor'=>$idusuario ));
          
          if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){

            if( $this->actualizar_status_repartidor($idusuario, 1, $search[0]->id_usuario, $search[0]->id, $search[0]->orden)){
                      $this->general->table = "tab_pedidos";
                      $update = $this->general->update($data, $data["id"]["value"]);
                      if($update){
                                  $response['status'] = TRUE;
                                  $response['message'] = '';
                                  $response['data'] = $search[0];
                                  return $this->set_response($response, REST_Controller::HTTP_OK);
                        }else{
                                  $response['status'] = FALSE;
                                  $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                  return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                        }
            }else{
                          $response['status'] = FALSE;
                          $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);

            }
             

          }else{
            $response['status'] = FALSE;
            $response['message'] = 'El id no esta registrado';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     
    
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => null
      ]; 
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA'; 
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  public function pedidos_repartidor_imagen_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

       $usuario = $response_token['data'];
       $idusuario = $usuario->id;

      $data = array(
          'id' => array('value'=>$this->post('id')),
          'status_pedido' => array('value'=> 3),
          'status_pago' => array('value'=> 1),
          'receta' => array('value'=>'')
      );
      
        if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {

          $search = $this->general->get(null,array('id'=>$data['id']['value'], 'id_repartidor'=>$idusuario ));
          
          if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
                  
                  if( isset($_FILES['receta']) && $_FILES['receta']['error']==0 && $_FILES['receta']['size']>0 ){
                        $this->load->library('upload');
                        $this->params_imgs['file_name']='receta_'.time();
                        $this->upload->initialize($this->params_imgs);
                        if ( ! $this->upload->do_upload('receta') ){
                                  $data['receta']['value'] = '';
                        }else{
                                  $file_info = $this->upload->data();
                                  $data['receta']['value'] = $file_info['file_name'];
                        }
                  }else{
                         $response['status'] = FALSE;
                         $response['message'] = "Ocurrio un error al subir la imagen";
                         $response['data'] = json_decode ("{}");
                         return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                  }
    
                  if( $this->actualizar_status_repartidor($idusuario, 1, $search[0]->id_usuario, $search[0]->id, $search[0]->orden)){
                            $this->general->table = "tab_pedidos";
                            $update = $this->general->update($data, $data["id"]["value"]);
                            if($update){
                                    $response['status'] = TRUE;
                                    $response['message'] = '';
                                    $response['data'] = $search[0];
                                    return $this->set_response($response, REST_Controller::HTTP_OK);
                            }else{
                                    $response['status'] = FALSE;
                                    $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                    $response['data'] = json_decode ("{}");
                                    return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                            }
                  }else{
                              $response['status'] = FALSE;
                              $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                              $response['data'] = json_decode ("{}");
                              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                  }
                 
      
          }else{
            $response['status'] = FALSE;
            $response['message'] = 'El id no esta registrado';
            $response['data'] = json_decode ("{}");
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          $response['data'] = json_decode ("{}");
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     
    
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => json_decode ("{}")
      ]; 
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA'; 
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  public function iniciar_ruta_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

       $usuario = $response_token['data'];
       $idrepartidor = $usuario->id;
       $idrepartidor = (int) $idrepartidor;

      $data = array(
          'id' => array('value'=>$this->put('id')),
          'status_pedido' => array('value'=>2)
      );
      
        if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {

                      //$search = $this->general->get(null,array('id'=>$data['id']['value'], 'id_repartidor'=>$idrepartidor ));
                      $search =  $this->general->get(null,array('id_repartidor'=>$idrepartidor, 'status_pedido'=>2, 'id !='=> $data["id"]["value"] ), "id, id_repartidor");

                      if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
                                $response['status'] = FALSE;
                                $response['message'] = 'No se puede iniciar la ruta, tiene otro pedido en ruta en estos momentos.';
                                $response['data'] = json_decode ("{}");
                                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                      }else{
                           $repartidor_status = $this->actualizar_status_repartidor2($idrepartidor, 2);
                           if( $repartidor_status){

                                      $this->general->table = "tab_pedidos";
                                      $update = $this->general->update($data, $data["id"]["value"]);
                                      if($update){
                                              $r = $this->general->get(null,array('id'=>$data["id"]["value"]),"*");
                                              $response['status'] = TRUE;
                                              $response['message'] = '';
                                              $response['data'] = $r[0];
                                              return $this->set_response($response, REST_Controller::HTTP_OK);
                                      }else{
                                              $response['status'] = FALSE;
                                              $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                              $response['data'] = json_decode ("{}");
                                              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                      }

                           }else{
                                      $response['status'] = FALSE;
                                      $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                      $response['data'] = json_decode ("{}");
                                      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                           }
                            
                           
                      }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          $response['data'] = json_decode ("{}");
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => json_decode ("{}")
      ]; 
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA'; 
    $response['data'] = json_decode ("{}");
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  public function actualizar_status_repartidor($id, $status, $id_usuario, $id_pedido, $orden){
          $this->general->table = "reg_repartidores";
          $a = $this->general->update(
                                      array('status_rep' => array('value'=> $status)),
                                      $id);
          if($a){
                  $this->general->table = "reg_usuarios";
                  $users = $this->general->get($id_usuario,null,"id, token_firebase");
                  $decive = $users[0]->token_firebase;
                  $tipo = substr($decive, 0, 4); 
                  $msj = "Tu pedido ha sido entregado. Ayudanos calificando tu experiencia";
                  if($tipo == "and-"){
                    $token = str_replace("and-", "", $decive);
                    $resu = $this->firebase->sendNotificationAnd($token, $msj, $id_pedido);
                  }else{
                    $token = str_replace("ios-", "", $decive);
                    $resu = $this->firebase->sendNotification($token, $msj, $id_pedido);
                  }
                        
                 return true;             
          }else{
               return false;
          }

  }

  public function actualizar_status_repartidor2($id, $status){
    $this->general->table = "reg_repartidores";
    $a = $this->general->update(
                                array('status_rep' => array('value'=> $status)),
                                $id);
    if($a){
           return true;             
    }else{
         return false;
    }

}

  private function _orden($d, $id, $id_direccion, $monto, $tarjeta, $mes_tarjeta, $anio_tarjeta){
      //{"Nombre": "", "NumeroTarjeta": "","TarejtaMes":"","TarejtaAnno":"","CodigoSeguridad":"", "TipoTarjeta":1, "Monto":4533.3,"MontoEnvio":0,"GastoID":0,"FacturaElectronica":false,"TipoPagoID":2}
        $servicio = "checkout?";
        $parametros =  "connection=".
                       "&pUbicacion=".urlencode($id_direccion).
                       "&pPago=".urlencode('{"Nombre": "", "NumeroTarjeta": "'.$tarjeta.'","TarjetaMes":'.$mes_tarjeta.',"TarjetaAnno":'.$anio_tarjeta.',"CodigoSeguridad":0, "TipoTarjeta":1, "Monto": '.$monto.', "MontoEnvio":0,"GastoID":0,"FacturaElectronica":false, "TipoPagoID":'.$d['tipo_pago']['value'].'}').
                       "&pSocioId=".urlencode($id).
                       "&pTipoEntrega=0".
                       "&pSucursalRecogeId=0";
        $url = $servicio.$parametros;
        return _curl_ldcom_pruebas($url, "8085", "POST", null);
  }

  public function send_notificacion_get(){

        $msj = $this->get('msj');
        $token = $this->get('token');
        if( $msj != "" && $token != "" ){
              $resu = $this->firebase->sendNotification($token, $msj, "192");
              return $this->set_response([
                'status' => TRUE,
                'message' => 'Se ha enviada la notificacion',
                'data' =>  $resu
              ], REST_Controller::HTTP_OK); 
        }else{
                return $this->set_response([
                  'status' => FALSE,
                  'message' => 'Parametros incompletos',
                  'data' => ""
              ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        

  }

}