<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class Chats extends REST_Controller {

  function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['chats_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['chats_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "tab_chats";
        $this->general->id = "id";
        $this->load->library('Authorization');
  }

  public function chats_get(){

    $response_token = $this->authorization->verificaToken();
  
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
       $usuario = $response_token['data'];
       $limit = $this->get('limit');
       $offset = $this->get('offset');
       $id =  $usuario->id;
       $chats = $this->general->get(null,null,"id, id_usuario, mensaje, fecha, respuesta");
       
       // If the id parameter doesn't exist return all the users
       
        if ($id === NULL){
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($chats){
              $response['status'] = TRUE;
              $response['message'] = '';
              $response['data'] = $chats;
              return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }else{
                // Set the response and exit
                return $this->response([
                    'status' => FALSE,
                    'message' => 'No chat were found',
                    'data' => []
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }

        $id = (int) $id;
        if ($id <= 0){
            $response['status'] = FALSE;
            $response['message'] = '';
            $response['data'] = [];
            // Invalid id, set the response and exit.
            return $this->response($response, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
        }

        if ($limit === "" || $offset === "" || $limit === NULL || $offset === NULL){
              return $this->response([
                              'status' => FALSE,
                              'message' => 'Could not be found (Required parameters limit & offset)',
                              'data' => []
                             ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code               
        }

        // Usually a model is to be used for this.
        $chat =  $this->general->get_chat($id, "id, id_usuario, mensaje, fecha, respuesta", $limit, $offset);

        if (!empty($chat)){
            $response['status'] = TRUE;
            $response['message'] = '';
            $response['data'] = $chat;
            return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
          return  $this->set_response([
                'status' => FALSE,
                'message' => 'chat could not be found',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }

    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
          $response = [
              'status' => FALSE,
              'message' => $response_token['message'],
              'data' => []
          ]; 
          return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    $response['data'] = [];
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    
  }

  public function chats_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
    $response_token = $this->authorization->verificaToken();
  
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id =  $usuario->id;
      $data = array(
          'id_usuario' => array('value'=>$id),
          'mensaje' => array('value'=>$this->post('mensaje')),
          'respuesta' => array('value'=> -1),
          'status_chat' => array('value'=> 2),
      );

      if( $data['id_usuario']['value'] != "" && $data['mensaje']['value'] != ""){
        if (filter_var($data['id_usuario']['value'], FILTER_VALIDATE_INT)) {
          $this->general->table = "reg_usuarios";

          $search = $this->general->get(null,array('id'=>$data['id_usuario']['value']));
          
          if( (isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ) || $data['id_usuario']['value'] === "-1" || $data['id_usuario']['value'] === -1 ){
           
            $this->general->table = "tab_chats";
            $id_insertd = $this->general->insert($data);
            if($id_insertd){
                $busqueda = $this->general->get(null,array('id'=>$id_insertd),"id, id_usuario, mensaje, respuesta, fecha");
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $busqueda[0];
                return $this->set_response($response, REST_Controller::HTTP_OK);
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }


          }else{
            $response['status'] = FALSE;
            $response['message'] = 'El idusuario no esta registrado';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El idusuario no es valido.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }
    } 
  
    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => null
        ]; 
        return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

}