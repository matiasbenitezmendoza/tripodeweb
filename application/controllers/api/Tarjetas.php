<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/openpay/Openpay.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Tarjetas extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['tarjetas_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['tarjetas_openpay_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['tarjetas_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['pago_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['tarjetas_delete']['limit'] = 10000; // 100 requests per hour per user/key

        $this->load->model("general");
        $this->general->table = "tab_tarjetas";
        $this->general->id = "id";
        $this->load->library('Authorization');

    }

  public function tarjetas_get(){
      $response = array('status' => FALSE, 'message'=>'');
  
      $response_token = $this->authorization->verificaToken();
  
      if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id = $usuario->id;
        $id = (int) $id;
        
        if($id != ''){
              $datos =  $this->general->get(null,array('id_usuario'=>$id));
              $response['status'] = TRUE;
              $response['message'] = '';
              $response['data'] = $datos;
              return $this->set_response($response, REST_Controller::HTTP_OK);
        }else{
             $response['status'] = FALSE;
             $response['message'] = 'Id no valido.';
             $response['data'] = [];
             return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }    
  
      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => []
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = [];
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  
  
  } 

  public function tarjetas_openpay_get(){
      $response = array('status' => FALSE, 'message'=>'');
  
      $response_token = $this->authorization->verificaToken();
  
      if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id = $usuario->id;
        $id = (int) $id;
        
        if(  $id != ''){
              $this->general->table = "reg_usuarios";
              $user =  $this->general->get(null,array('id'=>$id),"id_openpay");
              $id_openpay = $user[0]->id_openpay; //id openpay de la tabla reg_usuarios

              $cards = $this->lista_tarjetas_openpay($id_openpay);
              if( $cards != false){
                             $response['status'] = TRUE;
                             $response['message'] = '';
                             $d = array();
                             for ($i = 0; $i < count($cards); $i++) {     
                                $item = array(
                                  'id' =>         $cards[$i]->id,
                                  'numero' =>     $cards[$i]->card_number,
                                  'tipo' =>       $cards[$i]->type,
                                  'marca' =>       $cards[$i]->brand,
                                  'anio_exp' =>   $cards[$i]->expiration_year,   
                                  'mes_exp' =>    $cards[$i]->expiration_month,
                                  'id_cliente' => $cards[$i]->customer_id
                                );
                                $d[] = $item;
                             }
                             $response['data'] = $d;
                             return $this->set_response($response, REST_Controller::HTTP_OK);
              }
              else{
                             $response['status'] = FALSE;
                             $response['message'] = 'No se puede procesar el registro, ocurrio un problema.';
                             return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }
  
        }else{
          $response['status'] = FALSE;
          $response['message'] = 'Parametros incompletos.';
  
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }    
  
      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => null
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  
  
  } 
    
  public function tarjetas_post(){
    $response = array('status' => FALSE, 'message'=>'');

    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $id = (int) $id;
      $data = array(
          'id_usuario' => array('value'=>$id),
          'id_op' => array('value'=>$this->post('token_id')),
          'id_sesion' => array('value'=>$this->post('device_session_id')),
          'nombre' => array('value'=>$this->post('nombre')),   
          'numero' => array('value'=>$this->post('numero')),
          'tipo' => array('value'=>$this->post('tipo')),
          'mes_ven' => array('value'=>$this->post('mes_ven')),
          'anio_ven' => array('value'=>$this->post('anio_ven')),
          'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );

      if( $data['id_usuario']['value'] != "" && $data['id_op']['value'] != "" && $data['nombre']['value'] != "" && $data['numero']['value'] != '' && $data['mes_ven']['value'] != '' && $data['anio_ven']['value'] != ''){
            $this->general->table = "reg_usuarios";
            $user =  $this->general->get(null,array('id'=>$id),"id_openpay");
            $id_openpay = $user[0]->id_openpay; //id openpay de la tabla reg_usuarios

            if( $id_openpay == "" || $id_openpay == NULL ){
                 $response['status'] = FALSE;
                 $response['message'] = 'No se puede agregar la tarjeta, debe dar de alta sus datos en el perfil.';
                 return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }


            $id_card = $this->agregar_tarjeta_openpay($id_openpay, $data['id_op']['value'], $data['id_sesion']['value'] );

            if( $id_card != false){
                    $this->general->table = "tab_tarjetas";
                    $data['id_op']['value'] = $id_card;
                    $id_insertd = $this->general->insert($data);
                    if($id_insertd){                
                           $busqueda = $this->general->get(null,array('id'=>$id_insertd),"*");  
                           $response['status'] = TRUE;
                           $response['message'] = "Se agrego correctamente la tarjeta";
                           $response['data'] = $busqueda[0];
                           return $this->set_response($response, REST_Controller::HTTP_OK);
                    }else{
                           $response['status'] = FALSE;
                           $response['data'] = $id_card;
                           $response['message'] = 'No se puede procesar el registro, inténtalo más tarde.';
                           return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }
            }
            else{
                           $response['status'] = FALSE;
                           $response['message'] = 'Su tarjeta no puede ser procesada, revise que sus datos sean correctos';
                           return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }

      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }
    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


  }

  public function pago_get(){
    $response = array('status' => FALSE, 'message'=>'');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id =  $usuario->id;
        $id_tarjeta = $this->get('id'); 
        $cantidad = $this->get('cantidad'); 

        $tarjeta = $this->general->get(null,array('id'=>$id_tarjeta));
        $this->general->table = "reg_usuarios";
        $usuario = $this->general->get(null,array('id'=>$id));

        if( isset($tarjeta) && is_array($tarjeta) && count($tarjeta)>0 && isset($tarjeta[0]->id) ){
            if( isset($usuario) && is_array($usuario) && count($usuario)>0 && isset($usuario[0]->id) ){
                  try {
                       Openpay::setProductionMode(true);
                       $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
                       $id_openpay = $usuario[0]->id_openpay; 
                       $customer = $openpay->customers->get($id_openpay);

                       $chargeData = array(
                             'method' => 'card',
                             'source_id' => $tarjeta[0]->id_op,
                             'amount' => (float) $cantidad,
                             'currency' => 'MXN',
                           //'order_id' => 'oid-00052d2',
                             'description' => "Farmacia Tripode",
                             'device_session_id' => $tarjeta[0]->id_sesion,
                           //'customer' => $customer
                        );
                        
                        $charge = $customer->charges->create($chargeData);
                     
                        $response['status'] = TRUE;
                        $response['data'] = $charge->id;
                        $response['message'] = "Payment Made";
                        return $this->set_response($response,  REST_Controller::HTTP_OK);
                  } catch (Exception $e) {
                         $response['status'] = FALSE;
                         $response['data'] = 0;
                         $response['message'] =  $e->getMessage();
                         return $this->set_response($response,   REST_Controller::HTTP_BAD_REQUEST);
                  }
            }

        }
    }    
    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


  }

  public function tarjetas_delete(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
    $id =  $this->delete('id');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id_usuario =  $usuario->id;
      if( $id != "" ){
        if (filter_var($id, FILTER_VALIDATE_INT)) {
           $this->general->table = "reg_usuarios";
           $user =  $this->general->get(null,array('id'=>$id_usuario),"id_openpay");
           $id_openpay = $user[0]->id_openpay; //id openpay de la tabla reg_usuarios

           $this->general->table = "tab_tarjetas";
           $search = $this->general->get(null,array('id'=>$id));

           if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
              $id_card = $search[0]->id_op;

              $card_delete = $this->eliminar_tarjeta_openpay($id_openpay, $id_card);
              if($card_delete != false){
                  $delete = $this->general->delete($id);
                    if($delete){
                        $message = [
                             'status' => TRUE,
                             'id' => $card_delete,
                             'message' => 'Deleted the resource'
                        ];
                        return $this->set_response($message, REST_Controller::HTTP_OK);
                    }else{
                         $response['status'] = FALSE;
                         $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                         return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }
              }
              else{
                $response['status'] = FALSE;
                $response['message'] = $card_delete;
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }
             
          }else{
            $response['status'] = FALSE;
            $response['message'] = 'No se encontro el registro';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }else{
        $response['status'] = FALSE;
        $response['message'] = $id;
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }

    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


  }

  public function agregar_tarjeta_openpay($id, $token, $sesion){
    try {
      Openpay::setProductionMode(true);
      $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
      $cardData = array(
        'token_id' => $token,
        'device_session_id' => $sesion
        );
      $customer = $openpay->customers->get($id);
      $card = $customer->cards->add($cardData);
      return $card->id;
    } 
    catch (Exception $e) {
      return false;
    }


  }

  public function lista_tarjetas_openpay($id){
    try {
      Openpay::setProductionMode(true);
      $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
      $customer = $openpay->customers->get($id);

      $findData = array(
        'creation[gte]' => '2019-01-01',
        'creation[lte]' => '2019-12-31',
        'offset' => 0,
        'limit' => 1000);
      
      $cardList = $customer->cards->getList($findData);

      return $cardList;
    }    
    catch (Exception $e) {
      return false;
    }

  }

  public function eliminar_tarjeta_openpay($id, $idcard){
    try {
      Openpay::setProductionMode(true);
      $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
      $customer = $openpay->customers->get($id);
      $card = $customer->cards->get($idcard);
      $card->delete();
      return true;
    }
    catch (Exception $e) {
        return false;
    }

  }


}
