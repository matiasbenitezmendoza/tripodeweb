<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Temas extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['temas_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "cat_temas";
        $this->general->id = "id";
        $this->load->library('Authorization');

    }

  public function temas_get(){
      
    $id = $this->get('id');
    $response_token = $this->authorization->verificaToken();
  
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      
        $registros = $this->general->get(null,null,"id as id_tema, nombre, descripcion, concat ('https://www.farmaciastripode.com/admin/imgs/temas/',imagen) as imagen");
       
        if ($id === NULL){
            if ($registros){
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $registros;
                return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }else{
                return $this->response([
                    'status' => FALSE,
                    'message' => 'No registry were found',
                    'data' => []
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }

        $id = (int) $id;
        if ($id <= 0){
            $response['status'] = FALSE;
            $response['message'] = '';
            $response['data'] = [];
            // Invalid id, set the response and exit.
            return $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $registro =  $this->general->get(null,array('id'=>$id ), "id as id_tema, nombre, descripcion, concat ('https://www.farmaciastripode.com/admin/imgs/temas/',imagen) as imagen");

        if (!empty($registro)){
            $response['status'] = TRUE;
            $response['message'] = '';
            $response['data'] = $registro;
            return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
          return $this->set_response([
                'status' => FALSE,
                'message' => 'registry could not be found',
                'data' => []
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
          $response = [
              'status' => FALSE,
              'message' => $response_token['message'],
              'data' => []
          ]; 
          return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    $response['data'] = [];
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    
  }

    
}
