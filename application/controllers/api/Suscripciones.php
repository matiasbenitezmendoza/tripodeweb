<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Suscripciones extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['suscripciones_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['suscripciones_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['suscripciones_put']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "tab_suscripciones";
        $this->general->id = "id";
        $this->load->library('Authorization');

    }
  
    public function suscripciones_get(){
  
      $response_token = $this->authorization->verificaToken();
    
      if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
          $usuario = $response_token['data'];
          $id =  $usuario->id;
          $registros = $this->general->get(null,null,"id, id_usuario, novedades, notificaciones, temas");
  
          if ($id === NULL){
              if ($registros){
                  $response['status'] = TRUE;
                  $response['message'] = '';
                  $response['data'] = $registros[0];
                  return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
              }else{
                  return $this->response([
                      'status' => FALSE,
                      'message' => 'No registry were found',
                      'data' =>  json_decode ("{}")
                  ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
              }
          }
  
          $id = (int) $id;
          if ($id <= 0){
              $response['status'] = FALSE;
              $response['message'] = '';
              $response['data'] =    json_decode ("{}");
              // Invalid id, set the response and exit.
              return $this->response($response, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
          }
  
          $registro =  $this->general->get(null,array('id_usuario'=>$id ), "id, id_usuario, novedades, notificaciones, temas");
  
          if (!empty($registro)){
            $response['status'] = TRUE;
            $response['message'] = '';
            $response['data'] = $registro[0];
            return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
          }else{
            return $this->set_response([
                  'status' => FALSE,
                  'message' => 'registry could not be found',
                  'data' =>  json_decode ("{}")
              ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
          }
      }    
  
      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => json_decode ("{}")
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      $response['data'] = json_decode ("{}");
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
  
    public function suscripciones_post(){
      $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
      $response_token = $this->authorization->verificaToken();
  
      if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id = $usuario->id;
        $id = (int) $id;
        $data = array(
            'id_usuario' => array('value'=>$id),
            'novedades' => array('value'=>$this->post('novedades')),
            'notificaciones' => array('value'=>$this->post('notificaciones')),   
            'temas' => array('value'=>$this->post('temas')),
            'fecha' => array('value'=>date("Y-m-d H:i:s"))
        );
  
        if( $data['id_usuario']['value'] != "" && $data['novedades']['value'] != "" &&  $data['notificaciones']['value'] != ''){
        // if (filter_var($data['id_usuario']['value'], FILTER_VALIDATE_INT)) {
            //$this->general->table = "reg_usuarios";
            $search = $this->general->get(null,array('id_usuario'=>$data['id_usuario']['value']));
            if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
      
              $response['status'] = FALSE;
              $response['message'] = 'Solo puede haber un registro de suscripciones por usuario';
              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
             
            }else{
                 $id_insertd = $this->general->insert($data);
                 if($id_insertd){
                   $busqueda = $this->general->get(null,array('id'=>$id_insertd),"id, id_usuario, novedades, notificaciones, temas");
                   $response['status'] = TRUE;
                   $response['message'] = '';
                   $response['data'] = $busqueda[0];
                   return $this->set_response($response, REST_Controller::HTTP_OK);
                 }else{
                   $response['status'] = FALSE;
                   $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                   return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                 }
            }
  
        //  }else {
          //  $response['status'] = FALSE;
          //  $response['message'] = 'El id_usuario no es valido.';
          //  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        //  }
        }else{
          $response['status'] = FALSE;
          $response['message'] = 'Parametros incompletos.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }    
  
      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
              $response = [
                  'status' => FALSE,
                  'message' => $response_token['message'],
                  'data' => null
              ]; 
              return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  
  
    }
  
    public function suscripciones_put(){
      $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
      $response_token = $this->authorization->verificaToken();
  
      if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id = $usuario->id;
        $id = (int) $id;
        $data = array(
            'id_usuario' => array('value'=>$id),
            'novedades' => array('value'=>$this->put('novedades')),
            'notificaciones' => array('value'=>$this->put('notificaciones')),
            'temas' => array('value'=>$this->put('temas')),
            'fecha' => array('value'=>date("Y-m-d H:i:s"))
        );
        
        if( $data['id_usuario']['value'] != "" && $data['novedades']['value'] != "" && $data['notificaciones']['value'] != ""){
          if (filter_var($data['id_usuario']['value'], FILTER_VALIDATE_INT)) {
  
            $search = $this->general->get(null,array('id_usuario'=>$data['id_usuario']['value']));
            
            if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
               $update = $this->general->update($data, $search[0]->id);
               if($update){
                  $busqueda = $this->general->get(null,array('id_usuario'=>$data["id_usuario"]["value"]),"id, id_usuario, novedades, notificaciones, temas");
                  $response['status'] = TRUE;
                  $response['message'] = '';
                  $response['data'] = $busqueda[0];
                  return $this->set_response($response, REST_Controller::HTTP_OK);
                }else{
                  $response['status'] = FALSE;
                  $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                  return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                }
  
            }else{
              $response['status'] = FALSE;
              $response['message'] = 'El id_usuario no esta registrado';
              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }
  
          }else {
            $response['status'] = FALSE;
            $response['message'] = 'El id no es valido.';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }
        }else{
          $response['status'] = FALSE;
          $response['message'] = 'Parametros incompletos.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
  
      }
  
      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => null
        ]; 
        return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA'; 
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  
    }

}
