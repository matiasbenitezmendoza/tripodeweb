<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/openpay/Openpay.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Usuarios extends REST_Controller {

  function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        $this->methods['usuarios_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['usuarios_openpay_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['usuarios_recuperar_password_get']['limit'] = 50000;
        $this->methods['usuarios_post']['limit'] = 10000;
        $this->methods['usuarios_put']['limit'] = 5000;
        $this->methods['usuarios_password_put']['limit'] = 5000;
        $this->load->model("general");
        $this->general->table = "reg_usuarios";
        $this->general->id = "id";
        $this->load->library('Authorization');
        $this->load->helper('file');
  }

  public function usuarios_get(){
      // Users from a data store e.g. database
    $response_token = $this->authorization->verificaToken();
    $version = $this->post('version');
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
     $usuario = $response_token['data'];
     $id =  $usuario->id;
     $users = $this->general->get(null,null,"id, nombre, apellido_paterno, apellido_materno, email, telefono, padecimientos");

      // If the id parameter doesn't exist return all the users

      if ($id === NULL){
          // Check if the users data store contains users (in case the database result returns NULL)
          if ($users){
             $response['status'] = TRUE;
             $response['message'] = '';
             $response['data'] = $users;
             return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
          }else{
              // Set the response and exit
              return $this->response([
                  'status' => FALSE,
                  'message' => 'No users were found',
                  'data' =>  json_decode ("{}")
              ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
          }
      }

      $id = (int) $id;
      if ($id <= 0){
             $response['status'] = FALSE;
             $response['message'] = '';
             $response['data'] = json_decode ("{}");

          // Invalid id, set the response and exit.
          return $this->response($response, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
      }

      // Get the user from the array, using the id as key for retrieval.
      // Usually a model is to be used for this.
      $user =  $this->general->get(null,array('id'=>$id),"id, nombre, apellido_paterno, apellido_materno, email, telefono, padecimientos");

      if (!empty($user)){
          $response['status'] = TRUE;
          $response['message'] = '';
          $response['data'] = $user;
          return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }else{
        return $this->set_response([
              'status' => FALSE,
              'message' => 'User could not be found',
              'data' =>  json_decode ("{}")
          ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
      }
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => json_decode ("{}")
        ];
        return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    $response['data'] = json_decode ("{}");
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


  }

  public function token_firebase_get(){
    // Users from a data store e.g. database
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));

    $response_token = $this->authorization->verificaToken();
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id =  $usuario->id;
        $id_usuario_ldcom = $usuario->id_usuario_ldcom;

        $data = array(
          'id' => array('value'=>$id),
          'token_firebase' => array('value'=> $this->get('token'))
        );

        if( $data['id']['value'] != "" && $data['token_firebase']['value'] != ""  ){

                    //ver si existe el registro
                    $search = $this->general->get(null,array('id'=>$data['id']['value']));
                    if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
                                    if( $this->general->update($data, $data['id']['value']) ){
                                                              $busqueda = $this->general->get(null,array('id'=>$data['id']['value']));
                                                              $response['status'] = TRUE;
                                                              $response['message'] = 'Se actualizaron sus datos con exito';
                                                              $response['data'] = $busqueda[0];
                                                              return $this->set_response($response, REST_Controller::HTTP_OK);
                                    }
                                    else{
                                        $response['status'] = FALSE;
                                        $response['message'] = 'No se puede procesar el registro, inténtalo más tarde.';
                                        return  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                    }
                    }
                    else{
                          $response['status'] = FALSE;
                          $response['message'] = 'No se encontro el registro';
                          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }

        }
        else{
          $response['status'] = FALSE;
          $response['message'] = 'Parametros incompletos.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => json_decode ("{}")
        ];
        return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    $response['data'] = json_decode ("{}");
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


}

  public function usuarios_openpay_get(){

    try {
      Openpay::setProductionMode(true);
      $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');

      $id = array(
          'creation[gte]' => '2019-01-01',
          'creation[lte]' => '2019-12-31',
          'offset' => 0,
          'limit' => 50);
      $customer = $openpay->customers->getList($id);
      $d = array();
      for ($i = 0; $i < count($customer); $i++) {
        $item = array(
          'id' =>         $customer[$i]->id,
          'name' =>     $customer[$i]->name,
          'last_name' =>       $customer[$i]->last_name,
          'email' =>       $customer[$i]->email,
          'phone_number' =>   $customer[$i]->phone_number,
          'requires_account' =>   $customer[$i]->requires_account
        );
        $d[] = $item;
     }
      $response['data'] = $d;
      $response['status'] = TRUE;
      return $this->set_response($response, REST_Controller::HTTP_OK);

    } catch (OpenpayApiTransactionError $e) {
      $response['status'] = FALSE;
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
    } catch (OpenpayApiRequestError $e) {
      $response['status'] = FALSE;
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);

    } catch (OpenpayApiConnectionError $e) {
      $response['status'] = FALSE;
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);

    } catch (OpenpayApiAuthError $e) {
      $response['status'] = FALSE;
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);

    } catch (OpenpayApiError $e) {
      $response['status'] = FALSE;
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);

    } catch (Exception $e) {
      $response['status'] = FALSE;
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
    }


  }

  public function usuarios_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));
    $data = array(
        'id_openpay' => array('value'=>''),
        'nombre' => array('value'=>$this->post('nombre')),
        'apellido_paterno' => array('value'=>$this->post('apellido_paterno')),
        'apellido_materno' => array('value'=>$this->post('apellido_materno')),
        'fecha_nacimiento' => array('value'=>$this->post('fecha_nacimiento')),
        'email' => array('value'=>$this->post('email')),
        'password' => array('value'=>$this->post('password')),
        'telefono' => array('value'=>$this->post('telefono')),
        'padecimientos' => array('value'=>$this->post('padecimientos')),
        'id_ldcom' => array('value'=>"desvinculado"),
        'fecha' => array('value'=>date("Y-m-d H:i:s"))
    );

    $data2 = array(
          'id_usuario' => array('value'=>0),
          'nombre' => array('value'=>'Direccion 1'),
          'calle' => array('value'=>$this->post('calle')),
          'numero_exterior' => array('value'=>$this->post('numero_exterior')),
          'numero_interior' => array('value'=>$this->post('numero_interior')),
          'residencial' => array('value'=>$this->post('residencial')),
          'codigo_postal' => array('value'=>$this->post('codigo_postal')),
          'estado' => array('value'=>$this->post('estado')),
          'colonia' => array('value'=>$this->post('colonia')),
          'ciudad' => array('value'=>$this->post('ciudad')),
          'referencia' => array('value'=>$this->post('referencia')),
          'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );


    if( $data['nombre']['value'] != "" && $data['apellido_paterno']['value'] != "" && $data['apellido_materno']['value'] != "" && $data['password']['value'] != '' && $data['email']['value'] != '' && $data['fecha_nacimiento']['value'] != "" && $data2['nombre']['value'] != "" && $data2['calle']['value'] != "" && $data2['estado']['value'] != '' && $data2['colonia']['value'] != '' && $data2['numero_exterior']['value'] != '' && $data2['codigo_postal']['value'] != ''){
      if (filter_var($data['email']['value'], FILTER_VALIDATE_EMAIL)) {
        $search = $this->general->get(null,array('email'=>$data['email']['value']));
        if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
                $response['status'] = FALSE;
                $response['message'] = 'El correo ya esta resgistrado.';
                return  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
        else{
               //registro openpay
               $id_openpay = $this->_registrar_openpay($data);
               
               if($id_openpay != false){

                          //registro ldcom
                          $rldcom = $this->_registrar_ld_com($data);
                          $registro_ldcom = json_decode($rldcom);
                          if($registro_ldcom->IsSuccessful == true){
                                  //registro en la bd
                                  $data['id_openpay']['value'] = $id_openpay;
                                  $data['password']['value'] = md5($data['password']['value']);
                                  $id_insertd = $this->general->insert($data);
                                  if( $id_insertd ){
                                     
                                          //registro de la direccion
                                          $data2['id_usuario']['value'] = $id_insertd;
                                          $this->general->table = "tab_direcciones";
                                          if($this->general->insert($data2)){
                                                    $this->general->table = "reg_usuarios";
                                                    $nuevousuario = $this->general->get(null,array('id'=>$id_insertd),"id,id_openpay, nombre, apellido_paterno, apellido_materno, email, telefono, padecimientos");
                                                    $response['status'] = TRUE;
                                                    $response['message'] = "Registro guardado";
                                                    $response['data'] =  $nuevousuario[0];
                                                    return $this->set_response($response, REST_Controller::HTTP_OK);
                                          }else{
                                                    $response['status'] = FALSE;
                                                    $response['message'] = 'No se puede guardar la direccion del usuario, inténtalo más tarde dir.';
                                                    return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                          }
                                  }else{
                                      $response['status'] = FALSE;
                                      $response['message'] = 'No se puede procesar el registro, inténtalo más tarde reg.';
                                      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                  }
                          }
                          else{
                                $response['status'] = FALSE;
                                $response['message'] = $registro_ldcom->Message;
                                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                          }
                                                  
                }
                else{
                              $response['status'] = FALSE;
                              $response['message'] = 'No se puede procesar el registro, inténtalo más tarde op.';
                              return  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                }
        }

      }else {
        $response['status'] = FALSE;
        $response['message'] = 'El email no es valido.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }
    }else{
      $response['status'] = FALSE;
      $response['message'] = 'Parametros incompletos.';
      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function _usuarios_facebook_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));
    $data = array(
        'id_openpay' => array('value'=>''),
        'id_facebook' => array('value'=>$this->post('id_facebook')),
        'token_facebook' => array('value'=>$this->post('token_facebook')),
        'nombre' => array('value'=>$this->post('nombre')),
        'apellido_paterno' => array('value'=>$this->post('apellido_paterno')),
        'apellido_materno' => array('value'=>$this->post('apellido_materno')),
        'fecha_nacimiento' => array('value'=>$this->post('fecha_nacimiento')),
        'password' => array('value'=>"Fb5"._RandomString(12)),
        'id_ldcom' => array('value'=>"desvinculado"),
        'email' => array('value'=>$this->post('email')),
        'telefono' => array('value'=>$this->post('telefono')),
        'fecha' => array('value'=>date("Y-m-d H:i:s"))
    );

    $version = $this->post('version');

    if( $data['id_facebook']['value'] != "" && $data['token_facebook']['value'] != ""){

        $search = [];
        if( $data['email']['value'] != ""){
          $search = $this->general->get(null,array('email'=>$data['email']['value']));
        }
        else{
          $search = $this->general->get(null,array('id_facebook'=>$data['id_facebook']['value']));
        }

        if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){

                                    $usuario = $search[0];                                   
                                    $usuario_ld_com = $this->_login_ld_com($usuario->email, $usuario->password);
                                    if ($usuario_ld_com == false) {
                                        $response = [
                                            'status' => FALSE,
                                            'message' => 'Por favor verifique la activacion de su cuenta en su email.',
                                            'data' => $usuario_ld_com
                                        ];
                                        return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
                                    }
                                                        
                                    if($usuario->id_ldcom == "desvinculado" || $usuario->id_ldcom == ""){
                                            $actualizar_usuario = $this->_actualizar_usuario_ld_com($usuario, $usuario_ld_com->Id);
                                            $actualizar_id = $this->general->update_user($usuario->id, $usuario_ld_com->Id, $data['token_facebook'],$data['id_facebook']);   
                                            if($actualizar_usuario !=false  && $actualizar_id != false ){
                                              
                                            }else{
                                                $response = [
                                                    'status' => FALSE,
                                                    'message' => 'Ocurrio un problema, intente iniciar sesion nuevamente',
                                                    'data' => json_decode ("{}")
                                                ];
                                                return $this->response($response, REST_Controller::HTTP_OK);
                                            }
                                    }
          
                            
                                    $busqueda =  $this->general->get(null,array('id'=>$search[0]->id),"id, id_openpay, id_facebook, token_facebook, nombre, apellido_paterno, apellido_materno, fecha_nacimiento, email, telefono, padecimientos, telefono as version, telefono as id_usuario_ldcom");
                                    $user = $busqueda[0];

                                    $user->version = $version;
                                    $user->id_usuario_ldcom = $usuario_ld_com->Id;
                            
                                    $user_token = $this->authorization->generateToken($user);

                                    $response = [
                                        'status' => TRUE,
                                        'message' => 'Inicio de sesion correcto.',
                                        'data' => $user,
                                        'token' => $user_token
                                    ];
                            
                                    return $this->response($response, REST_Controller::HTTP_OK);
        }
        else{
                //registro_openpay
                $id_openpay = $this->_registrar_openpay($data);
                if($id_openpay != false){

                                      //registro ldcom
                                      $registro_ldcom = $this->_registrar_ld_com($data);
                                      if($registro_ldcom != false){
                                                  $data['id_openpay']['value'] = $id_openpay;
                                                  //registro bd
                                                  $id_insertd = $this->general->insert($data);
                                                  if( $id_insertd ){
                                                          $response['status'] = FALSE;
                                                          $response['message'] = 'Por favor confirme la activacion de su cuenta en su email.';
                                                          $response['data'] = json_decode ("{}");
                                                          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                                  }else{
                                                          $response['status'] = FALSE;
                                                          $response['message'] = 'No se puede procesar el registro, inténtalo más tarde.';
                                                          $response['data'] = json_decode ("{}");
                                                          $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                                  }
                                      }
                                      else{
                                        $response['status'] = FALSE;
                                        $response['message'] = 'No se puede procesar el registro, inténtalo más tarde. LDOOM';
                                        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                      }

                }
                else{
                        $response['status'] = FALSE;
                        $response['message'] = 'No se puede guardar el usuario, inténtalo más tarde.';
                        $response['data'] = json_decode ("{}");
                        $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                }
        }


    }else{
            $response['status'] = FALSE;
            $response['message'] = 'Parametros incompletos.';
            $response['data'] = json_decode ("{}");
            $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
    }

  }

  public function usuarios_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));

    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id =  $usuario->id;
      $id_usuario_ldcom = $usuario->id_usuario_ldcom;
      $version =  $usuario->version;

      $data = array(
        'id' => array('value'=>$id),
        'nombre' => array('value'=> $this->put('nombre')),
        'apellido_paterno' => array('value'=>$this->put('apellido_paterno')),
        'apellido_materno' => array('value'=>$this->put('apellido_materno')),
        'fecha_nacimiento' => array('value'=>$this->put('fecha_nacimiento')),
        'email' => array('value'=>$this->put('email')),
        'padecimientos' => array('value'=>$this->put('padecimientos')),
        'telefono' => array('value'=>$this->put('telefono')),
        'password' => array('value'=> $this->put('password'))
      );

      if( $data['id']['value'] != "" && $data['nombre']['value'] != "" && $data['apellido_paterno']['value'] != "" && $data['apellido_materno']['value'] != "" && $data['fecha_nacimiento']['value'] != '' && $data['email']['value'] != '' && $data['telefono']['value'] != '' ){

         if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {
                   //ver si existe el registro
                   $search = $this->general->get(null,array('id'=>$data['id']['value']));
                   if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){

                         $search_email = $this->general->get(null,array('email'=>$data['email']['value'], 'id !='=>$data['id']['value'] ));
                         if( isset($search_email) && is_array($search_email) && count($search_email)>0 && isset($search_email[0]->id) ){
                                    $response['status'] = FALSE;
                                    $response['message'] = 'El correo ya se encuentra registrado';
                                    return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                         }
                         else{
                                     
                                    
                                    if( $data['password']['value'] != ""){
                                          $actualizar_pass = $this->_actualizar_password_ld_com($id_usuario_ldcom, $data['password']['value']);
                                          $data['password']['value'] = md5($data['password']['value']);
                                          if($actualizar_pass == false ){
                                                  $response = [
                                                      'status' => FALSE,
                                                      'message' => 'Ocurrio un problema al cambiar su contraseña',
                                                      'data' => json_decode ("{}")
                                                  ];
                                                  return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
                                         }
                                    }else{
                                         unset($data['password']);
                                    }
                                    
                                    if( $this->general->update($data, $data['id']['value']) ){

                                              $updateop = $this->_actualizar_user_openpay($search[0]->id_openpay, $data);
                                              if($updateop != false){
                                                            $busqueda =  $this->general->get(null,array('email'=>$data['email']['value']),"id, id_openpay, nombre, apellido_paterno, apellido_materno, fecha_nacimiento, email, telefono, padecimientos, fecha, status, telefono as version, telefono as id_usuario_ldcom, fecha as fecha_activacion");
                                                            $user = $busqueda[0];
                                                            $actualizar_usuario = $this->_actualizar_usuario_ld_com($user, $id_usuario_ldcom);
                                                                   
                                                            if($actualizar_usuario == false ){
                                                                      $response = [
                                                                          'status' => FALSE,
                                                                          'message' => 'Ocurrio un problema, intentelo nuevamente',
                                                                          'data' => json_decode ("{}")
                                                                      ];
                                                                      return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
                                                            }
                                                            $user->version = $version;
                                                            $user->id_usuario_ldcom = $id_usuario_ldcom;
                                                            $response['status'] = TRUE;
                                                            $response['message'] = 'Se actualizaron sus datos con exito';
                                                            $response['token'] = $this->authorization->generateToken($user);
                                                            $response['data'] = $user;
                                                            return $this->set_response($response, REST_Controller::HTTP_OK);
                                              }else{
                                                            $response['status'] = FALSE;
                                                            $response['message'] = 'Ocurrio un problema con su actualizacion';
                                                            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                              }
                                   }
                                   else{
                                       $response['status'] = FALSE;
                                       $response['message'] = 'No se puede procesar el registro, inténtalo más tarde.';
                                       return  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                   }
                         }

                   }
                   else{
                         $response['status'] = FALSE;
                         $response['message'] = 'No se encontro el registro';
                         return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                   }

         }
         else {
                 $response['status'] = FALSE;
                 $response['message'] = 'El id no es valido.';
                 return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
         }
      }
      else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => null
        ];
        return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);



  }

  public function _usuarios_password_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));

    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id =  $usuario->id;
      $id_usuario_ldcom = $usuario->id_usuario_ldcom;


      $data = array(
        'id' => array('value'=>$id),
        'password' => array('value'=> $this->put('password')),
        'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );

      if( $data['password']['value'] != '' ){

         if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {
                   //ver si existe el registro
                   $search = $this->general->get(null,array('id'=>$data['id']['value']));
                   if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){

                                    $change = $this->_actualizar_password_ld_com($id_usuario_ldcom, $data['password']['value']);
                                    if ($change == false) {
                                                  $response = [
                                                      'status' => FALSE,
                                                      'message' => 'Ocurrio un problema con su cambio de contraseña.',
                                                      'data' => $usuario_ld_com
                                                  ];
                                                  return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
                                    }

                                    $data['password']['value'] = md5($data['password']['value']);
                                    if( $this->general->update($data, $data['id']['value']) ){
                                                $busqueda = $this->general->get(null,array('id'=>$data['id']['value']),"id, id_openpay, nombre, apellido_paterno, apellido_materno, fecha_nacimiento, email, telefono, padecimientos");
                                                $response['status'] = TRUE;
                                                $response['message'] = '';
                                                $response['data'] =  $busqueda[0];
                                                return $this->set_response($response, REST_Controller::HTTP_OK);
                                    }
                                    else{
                                                $response['status'] = FALSE;
                                                $response['message'] = 'No se pudo procesar el registro, inténtalo más tarde.';
                                                return  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                    }



                   }
                   else{
                         $response['status'] = FALSE;
                         $response['message'] = 'No se encontro el registro';
                         return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                   }

         }
         else {
                 $response['status'] = FALSE;
                 $response['message'] = 'El id no es valido.';
                 return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
         }
      }
      else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => null
        ];
        return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);



  }

  public function usuarios_recuperar_password_get(){
       $email = $this->get('email');
       if($email != ""){     
              $recuperar = $this->_reset_password_ld_com($email);
              if($recuperar != false){
                    return $this->set_response([
                      'status' => TRUE,
                      'message' => 'Se ha enviado un correo para recuperar su contraseña.',
                      'data' => null
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
              }
              
              return $this->set_response([
                'status' => FALSE,
                'message' => 'Error al enviar correo',
                'data' => null
               ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                     
       }
       else{
          return $this->set_response([
            'status' => FALSE,
            'message' => 'El correo no puede estar vacio',
            'data' => null
           ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
       }



  }

  private function _registrar_openpay($data){

    try {
      Openpay::setProductionMode(true);
      $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
      $customerData = array(
        'name' =>  $data['nombre']['value'],
        'last_name' => $data['apellido_paterno']['value'].' '.$data['apellido_materno']['value'],
        'email' => $data['email']['value'],
        'requires_account' => false,
        'phone_number' => $data['telefono']['value']
       );
       $customer = $openpay->customers->add($customerData);
       return $customer->id;

    }
    catch (Exception $e) {
      return false;
    }
  }

  private function _actualizar_user_openpay($id, $data){

    try {
       Openpay::setProductionMode(true);
       $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
       $customer = $openpay->customers->get($id);

       $customer->name = $data['nombre']['value'];
       $customer->last_name = $data['apellido_paterno']['value'].' '.$data['apellido_materno']['value'];
       $customer->email = $data['email']['value'];
       $customer->phone_number = $data['telefono']['value'];
       $customer->requires_account = false;
       $customer->save();
       return true;

    }
    catch (Exception $e) {
      return false;
    }


  }

  private function _registrar_ld_com($d){
    //signup?connection=&name=Pedro Lopez&password=Pedro11&email=romedy10@gmail.com
    $servicio = "signup?";
    $parametros =  "connection=".
                   "&name=".urlencode($d['nombre']['value']." ".$d['apellido_paterno']['value']." ".$d['apellido_materno']['value']).
                   "&password=".urlencode($d['password']['value']).
                   "&email=".urlencode($d['email']['value']);
    $url = $servicio.$parametros;
    return _curl_ldcom_pruebas($url, "8085", "POST", null);
  }

  private function _login_ld_com($email, $password){
        $servicio = "login?";
        $parametros = "connection=&password=".$password."&email=".$email;
        $url = $servicio.$parametros;
        return _curl_ldcom($url, "8085", "GET", null);
  }

  private function _add_direcciones_ld_com($d, $id){
    //addresses?connection=&PhoneNumber=&IsMainAddress=&PostalCode=&UserId=&References=&Street=&ExtNumber=&IntNumebr=&Name=&Level1=&Level2=&Level3=
    $servicio = "addresses?";
    $parametros =  "connection=".
                   "&PhoneNumber=".
                   "&IsMainAddress=true".
                   "&PostalCode=".urlencode($d->codigo_postal).
                   "&UserId=".urlencode($id).
                   "&References=".urlencode($d->residencial).
                   "&Street=".urlencode($d->calle).
                   "&ExtNumber=".urlencode($d->numero_exterior).
                   "&IntNumber=".urlencode($d->numero_interior).
                   "&Name=".urlencode($d->nombre).
                   "&Level1=".urlencode($d->estado).
                   "&Level2=".urlencode($d->ciudad).
                   "&Level3=".urlencode($d->colonia);
    $url = $servicio.$parametros;
    return _curl_ldcom($url, "8085", "POST", null);
  }

  private function _actualizar_usuario_ld_com($d, $id){
    //updatesocio?pId=44&pName=Juan e&pEmail=&pBirth=1979-06-29T00:00:00&pFilter1=2&pFilter2=2&pFilter3=2&pFilter4=3&pFilter5=4&pTelephone=6666666666
    $servicio = "updatesocio?";
    $parametros =       "pId=".urlencode($id).
                        "&pName=".urlencode($d->nombre." ".$d->apellido_paterno." ".$d->apellido_materno).
                        "&pEmail=".urlencode($d->email).
                        "&pBirth=".urlencode($d->fecha_nacimiento).
                        "&pFilter1=0".
                        "&pFilter2=0".
                        "&pFilter3=0".
                        "&pFilter4=0".
                        "&pFilter5=0".
                        "&pTelephone=".urlencode($d->telefono);
    $url = $servicio.$parametros;
    return _curl_ldcom($url, "8085", "POST", null); 
  }

  private function _actualizar_password_ld_com($id, $pass){
    //changepassword/44?connection=&password=Juan11
    $servicio = "changepassword/".$id."?";
    $parametros =       "connection=".
                        "&password=".urlencode($pass);
    $url = $servicio.$parametros;
    return _curl_ldcom($url, "8085", "POST", null); 
  }

  private function _reset_password_ld_com($email){
    //resetpassword?connection=&email=mbenitez@webandando.com
    $servicio = "resetpassword?";
    $parametros =       "connection=".
                        "&email=".urlencode($email);
    $url = $servicio.$parametros;
    return _curl_ldcom($url, "8085", "POST", null); 
  }

  public function eliminar_user_openpay_get(){
    $id = $this->get('id');
    try {
       Openpay::setProductionMode(true);
       $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
       $customer = $openpay->customers->get($id);
       $customer->delete();

       return $this->set_response([
        'status' => TRUE
      ], REST_Controller::HTTP_OK);
    }
    catch (Exception $e) {
       return $this->set_response([
        'status' => TRUE
      ], REST_Controller::HTTP_OK);
    }


  }

  public function add_openpay_get(){
    $data = array(
      'nombre' => array('value'=>$this->get('nombre')),
      'apellido_paterno' => array('value'=>$this->get('apellido_paterno')),
      'apellido_materno' => array('value'=>$this->get('apellido_materno')),
      'email' => array('value'=>$this->get('email')),
      'telefono' => array('value'=>$this->get('telefono'))
   );

    try {
      Openpay::setProductionMode(true);
      $openpay = Openpay::getInstance('mfv3k7ynfmotpjhdkc0h', 'sk_9faca3c79b8e4905924ebe88849de233');
      $customerData = array(
        'name' =>  $data['nombre']['value'],
        'last_name' => $data['apellido_paterno']['value'].' '.$data['apellido_materno']['value'],
        'email' => $data['email']['value'],
        'requires_account' => false,
        'phone_number' => $data['telefono']['value']
       );
       $customer = $openpay->customers->add($customerData);
       return $this->set_response([
        'status' => TRUE,
        'id' => $customer->id
       ], REST_Controller::HTTP_OK);
      // return $customer->id;
      
    }
    catch (Exception $e) {
      return $this->set_response([
        'status' => FALSE
      ], REST_Controller::HTTP_OK);
    }
  }




}
