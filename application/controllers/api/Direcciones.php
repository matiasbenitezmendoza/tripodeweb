<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Direcciones extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['direcciones_cp_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['direcciones_estados_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['direcciones_municipios_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['direcciones_colonias_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['direcciones_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['direcciones_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['direcciones_put']['limit'] = 5000; // 50 requests per hour per user/key
        $this->methods['direcciones_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "tab_direcciones";
        $this->general->id = "id";
        $this->load->library('Authorization');
        $this->url_api =  "http://pruebas.aph.mx:8085/FRAGARE/RestApi_eCommerce/eCommerce/";

        $this->llaves = array(
                                'Consecutivo' => array('value'=>'id'),
                                'UserId' => array('value'=>'id_usuario'),
                                'Name' => array('value'=>'nombre'),
                                'Street' => array('value'=>'calle'),
                                'NumExt' => array('value'=>'numero_exterior'),
                                'NumInt' => array('value'=>'numero_interior'),
                                'References' => array('value'=>'residencial'),
                                'PostalCode' => array('value'=>'codigo_postal'),
                                'Nivel1Id' => array('value'=>'estado'),
                                'Nivel2Id' => array('value'=>'ciudad'),
                                'Nivel3Id' => array('value'=>'colonia'),
                                'Telephone' => array('value'=>'referencia')
                              );
    }
  


  public function direcciones_get(){

    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id =  $usuario->id;
        $id_ldcom =  $usuario->id_usuario_ldcom;


        $registros = $this->_direcciones_get_ld_com($id_ldcom);
        if ($registros != false){
                    $datos = _cambiar_llaves($this->llaves, $registros);

                    foreach($datos as $clave => $valor) {
                                $myObj = json_decode ("{}");
                                $id_estado = $valor->estado;
                                $id_municipio = $valor->ciudad;
                                $id_colonia = $valor->colonia;

                                $estados = $this->_direcciones_estados();
                                $municipios = $this->_direcciones_municipios($id_estado);
                                $colonias = $this->_direcciones_colonias($id_estado, $id_municipio);

                                foreach( $estados as $key => $field ){
                                          if($field->Id == $id_estado){
                                                $valor->estado = $field->Name;
                                          }
                                }

                                foreach( $municipios as $key => $field ){
                                          if($field->Id == $id_municipio){
                                                $valor->ciudad = $field->Name;
                                          }
                                }

                                foreach( $colonias as $key => $field ){
                                          if($field->Id == $id_colonia){
                                                $valor->colonia = $field->Name;
                                          }
                                }

                                if($valor->residencial == "0")
                                	   $valor->residencial = "";

                                if($valor->referencia == "0")
                                	   $valor->referencia = "";


                    }

                    $response['status'] = TRUE;
                    $response['message'] = '';
                    $response['data'] = $datos;
                    return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
                return $this->set_response([
                      'status' => TRUE,
                      'message' => 'Addresses could not be found',
                      'data' => []
                      ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => []
            ];
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    $response['data'] = [];
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  public function direcciones_post(){

    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}") );
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $id_usuario_ldcom = $usuario->id_usuario_ldcom;
      $id = (int) $id;
      $data = array(
          'id_usuario_ldcom' => array('value'=>$id_usuario_ldcom),
          'nombre' => array('value'=>$this->post('nombre')),
          'calle' => array('value'=>$this->post('calle')),
          'numero_exterior' => array('value'=>$this->post('numero_exterior')),
          'numero_interior' => array('value'=>$this->post('numero_interior')),
          'residencial' => array('value'=>$this->post('residencial')),
          'codigo_postal' => array('value'=>$this->post('codigo_postal')),
          'pNivel1' => array('value'=>$this->post('pNivel1')),
          'pNivel2' => array('value'=>$this->post('pNivel2')),
          'pNivel3' => array('value'=>$this->post('pNivel3')),
      );

      if( $data['id_usuario_ldcom']['value'] != "" && $data['nombre']['value'] != "" && $data['calle']['value'] != ""  && $data['numero_exterior']['value'] != '' && $data['codigo_postal']['value'] != ''  ){
        
            $resp_ldcom = $this->_direcciones_add_ld_com($data);

            if($resp_ldcom == false){
                      $response['status'] = FALSE;
                      $response['message'] = "No se pudo agregar su direccion, intentelo de nuevo.";
                      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }

            if($resp_ldcom == "El nombre de la dirección de entrega ingresada ya existe. Ingrese otro nombre."){
                      $response['status'] = FALSE;
                      $response['message'] = "El nombre de la dirección ya existe. Ingrese otro nombre.";
                      return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }

            $response['status'] = TRUE;
            $response['message'] = 'Se agrego correctamente la direccion';
            return $this->set_response($response, REST_Controller::HTTP_OK);


      }else{
            $response['status'] = FALSE;
            $response['message'] = 'Parametros incompletos.';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ];
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


  }

  public function direcciones_put(){
    
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}") );
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id_usuario_ldcom = $usuario->id_usuario_ldcom;

      $data = array(
          'id' => array('value'=>$this->put('id')),
          'id_usuario_ldcom' => array('value'=>$id_usuario_ldcom),
          'nombre' => array('value'=>$this->put('nombre')),
          'calle' => array('value'=>$this->put('calle')),
          'numero_exterior' => array('value'=>$this->put('numero_exterior')),
          'numero_interior' => array('value'=>$this->put('numero_interior')),
          'residencial' => array('value'=>$this->put('residencial')),
          'codigo_postal' => array('value'=>$this->put('codigo_postal')),
          'pNivel1' => array('value'=>$this->put('pNivel1')),
          'pNivel2' => array('value'=>$this->put('pNivel2')),
          'pNivel3' => array('value'=>$this->put('pNivel3'))
      );

   
      if( $data['id']['value'] != "" && $data['nombre']['value'] != "" && $data['calle']['value'] != "" && $data['numero_exterior']['value'] != '' && $data['numero_interior']['value'] != '' && $data['codigo_postal']['value'] != '' ){

                    $resp_ldcom = $this->_direcciones_update_ld_com($data);
                  
                    if($resp_ldcom === "El nombre de la dirección de entrega ingresada ya existe. Ingrese otro nombre."){
                          $response['status'] = FALSE;
                          $response['message'] = "El nombre de la dirección ya existe. Ingrese otro nombre.";
                          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }

                    if($resp_ldcom === false){
                              $response['status'] = FALSE;
                              $response['message'] = "No se pudo actualizar la direccion, verifique que el nombre de la direccion no exista.";
                              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }

                    $response['status'] = TRUE;
                    $response['message'] = 'Se actualizo el registro.';
                    return $this->set_response($response, REST_Controller::HTTP_OK);

      
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => null
      ];
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    
  }

  public function direcciones_delete(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos',  'data'=> json_decode ("{}") );
    $id =  $this->delete('id');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id_usuario_ldcom = $usuario->id_usuario_ldcom;

      $data = array(
        'id' => array('value'=>intval( $id)),
        'id_usuario_ldcom' => array('value'=>$id_usuario_ldcom)
      );
     
      if (filter_var($id, FILTER_VALIDATE_INT)) {
                              $resp_ldcom = $this->_direcciones_delete_ld_com($data);
                               
                              if($resp_ldcom === false){
                                          $response['status'] = FALSE;
                                          $response['message'] = "No se pudo eliminar su direccion, intentelo de nuevo.";
                                          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                              }

                              $response['status'] = TRUE;
                              $response['message'] = 'Se elimino el registro.';
                              return $this->set_response($response, REST_Controller::HTTP_OK);

        }else {
                  $response['status'] = FALSE;
                  $response['message'] = 'El id no es valido.';
                  return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ];
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  /*Funciones para LD COM*/

  public function direcciones_estados_get(){    
      $servicio = "listlevel1?";
      $parametros = "connection=";
      $url = $servicio.$parametros;
      $respuesta =  _curl_ldcom($url, "8085", "GET", null);

      if($respuesta == false){
           $response['status'] = FALSE;
           $response['message'] = "Ocurrio un problema al obtener los datos.";
           $response['data'] = [];
           return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST); // OK (200) being the HTTP response code
      }
      else{
          $response['status'] = TRUE;
          $response['message'] = '';
          $response['data'] = $respuesta;
          return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }
  }

  public function direcciones_municipios_get(){
      $id_estado = $this->get('Id');
      if ($id_estado != NULL &&  !empty($id_estado) &&  !empty($id_estado) ){
              $servicio = "listlevel2/".$id_estado."?";
              $parametros = "connection=";
              $url = $servicio.$parametros;
              $respuesta =  _curl_ldcom($url, "8085", "GET", null);
          
              if($respuesta == false){
                    $response['status'] = FALSE;
                    $response['message'] = "Ocurrio un problema al obtener los datos.";
                    $response['data'] = [];
                    return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST); // OK (200) being the HTTP response code
              }
              else{
                    $response['status'] = TRUE;
                    $response['message'] = '';
                    $response['data'] = $respuesta;
                    return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
              }
      }else{
              $response['status'] = FALSE;
              $response['message'] = 'Id requerido';
              return $this->response($response, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
      }
  }

  public function direcciones_colonias_get(){
      $id_estado = $this->get('Id');
      $id_municipio = $this->get('Id_municipio');

      if ($id_estado != NULL &&  !empty($id_estado) && $id_municipio != NULL &&  !empty($id_municipio) ){
              
              $servicio = "listlevel3/".$id_estado."/".$id_municipio."?";
              $parametros = "connection=";
              $url = $servicio.$parametros;
              $respuesta =  _curl_ldcom($url, "8085", "GET", null);
          
              if($respuesta == false){
                    $response['status'] = FALSE;
                    $response['message'] = "Ocurrio un problema al obtener los datos.";
                    $response['data'] = [];
                    return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST); // OK (200) being the HTTP response code
              }
              else{
                    $response['status'] = TRUE;
                    $response['message'] = '';
                    $response['data'] = $respuesta;
                    return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
              }
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros Id & Id_municipio requeridos';
        return $this->response($response, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
      }
  }

  private function _direcciones_estados(){    
	    $servicio = "listlevel1?";
	    $parametros = "connection=";
	    $url = $servicio.$parametros;
	    $respuesta =  _curl_ldcom($url, "8085", "GET", null);

	    if($respuesta != false){
	         return $respuesta;
	    }
	    else{
	       return false;
	    }
  }

  private function _direcciones_municipios($id_estado){
      if ($id_estado != NULL &&  !empty($id_estado) &&  !empty($id_estado) ){
      	      //listlevel2/lebel1id?connection=
              $servicio = "listlevel2/".$id_estado."?";
              $parametros = "connection=";
              $url = $servicio.$parametros;
              $respuesta =  _curl_ldcom($url, "8085", "GET", null);
          
                  if($respuesta != false){
                        return $respuesta;
                  }
                  else{
                      return false;
                  }
      }else{
            return false;
      }
  }

  private function _direcciones_colonias($id_estado, $id_municipio){
      if ($id_estado != NULL &&  !empty($id_estado) && $id_municipio != NULL &&  !empty($id_municipio) ){
      	      //listlevel3/level1id/level2id?connection=
              $servicio = "listlevel3/".$id_estado."/".$id_municipio."?";
              $parametros = "connection=";
              $url = $servicio.$parametros;
              $respuesta =  _curl_ldcom($url, "8085", "GET", null);
                if($respuesta != false){
                      return $respuesta;
                }
                else{
                    return false;
                }
      }else{
        return false;
      }
  }

  private function _direcciones_get_ld_com($id){
  	//addresses/user/16?connection=
     $servicio = "addresses/user/".$id."?";
     $parametros = "connection=";
     $url = $servicio.$parametros;
     return _curl_ldcom($url, "8085", "GET", null);
  }

  private function _direcciones_add_ld_com($d){
        //addresses?connection=&PhoneNumber=&IsMainAddress=&PostalCode=&UserId=&References=&Street=&ExtNumber=&IntNumebr=&Name=&Level1=&Level2=&Level3=
        //residencia -> referencia
        $servicio = "addresses?";
        $parametros =  "connection=".
                       "&PhoneNumber=".
                       "&IsMainAddress=true".
                       "&PostalCode=".urlencode($d['codigo_postal']['value']).
                       "&UserId=".urlencode($d['id_usuario_ldcom']['value']).
                       "&References=".urlencode($d['residencial']['value']).
                       "&Street=".urlencode($d['calle']['value']).
                       "&ExtNumber=".urlencode($d['numero_exterior']['value']).
                       "&IntNumber=".urlencode($d['numero_interior']['value']).
                       "&Name=".urlencode($d['nombre']['value']).
                       "&Level1=".urlencode($d['pNivel1']['value']).
                       "&Level2=".urlencode($d['pNivel2']['value']).
                       "&Level3=".urlencode($d['pNivel3']['value']);
        $url = $servicio.$parametros;
        return _curl_ldcom($url, "8085", "POST", null);
  }

  private function _direcciones_update_ld_com($d){
        //addresses?connection=&AddressId=&PhoneNumber=&IsMainAddress=&PostalCode=&UserId=&References=&Street=&ExtNumber=&IntNumebr=&Name=&Level1=&Level2=&Level3=
        $servicio = "addresses?";
        $parametros = "connection=".
                       "&AddressId=".urlencode($d['id']['value']).
                       "&PhoneNumber=".
                       "&IsMainAddress=true".
                       "&PostalCode=".urlencode($d['codigo_postal']['value']).
                       "&UserId=".urlencode($d['id_usuario_ldcom']['value']).
                       "&References=".urlencode($d['residencial']['value']).
                       "&Street=".urlencode($d['calle']['value']).
                       "&ExtNumber=".urlencode($d['numero_exterior']['value']).
                       "&IntNumber=".urlencode($d['numero_interior']['value']).
                       "&Name=".urlencode($d['nombre']['value']).
                       "&Level1=".urlencode($d['pNivel1']['value']).
                       "&Level2=".urlencode($d['pNivel2']['value']).
                       "&Level3=".urlencode($d['pNivel3']['value']);
                       
        $url = $servicio.$parametros;
        return _curl_ldcom($url, "8085", "POST", null);
  }

  private function _direcciones_delete_ld_com($d){
        //addresses/addressid/userid?connection
        $servicio = "addresses/".urlencode($d['id']['value'])."/".urlencode($d['id_usuario_ldcom']['value'])."?";
        $parametros = "connection=";
        $url = $servicio.$parametros;
        return _curl_ldcom($url, "8085", "DELETE", null);
  }


}
