<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "registro_usuarios";
        $this->general->id = "id";
    }

    public function users_get(){
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL){
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users){
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }else{
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        $id = (int) $id;
        if ($id <= 0){
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.
        $user = NULL;

        if (!empty($users)){
            foreach ($users as $key => $value){
                if (isset($value['id']) && $value['id'] === $id){
                    $user = $value;
                }
            }
        }

        if (!empty($user)){
            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function users_post(){
      $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
      $data = array(
          'nombre' => array('value'=>$this->post('nombre')),
          'apellidos' => array('value'=>$this->post('apellidos')),
          'email' => array('value'=>$this->post('email')),
          'passwd' => array('value'=>$this->post('password')),
          'imagen' => array('value'=>$this->post('imagen')),
          'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );

      if( $data['nombre']['value'] != "" && $data['apellidos']['value'] != '' && $data['passwd']['value'] != '' && $data['email']['value'] != '' ){
        if (filter_var($data['email']['value'], FILTER_VALIDATE_EMAIL)) {
          $search = $this->general->get(null,array('email'=>$data['email']['value']));
          if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
            $response['status'] = FALSE;
            $response['message'] = 'El correo ya esta resgistrado.';
            $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }else{
            $data['passwd']['value'] = md5($data['passwd']['value']);

            if( isset($_FILES['imagen']) && $_FILES['imagen']['error']==0 && $_FILES['imagen']['size']>0 ){
              $fileName   = $_FILES['imagen']['name'];
              $arr_tmp = explode(".",$fileName);
              $extension = end($arr_tmp);
              $nombre_img = strtotime(date("Y-m-d H:i:s")).'.'.$extension;
              $moved = move_uploaded_file($_FILES["imagen"]["tmp_name"], "./profile/".$nombre_img );
              $data['imagen']['value'] = $nombre_img;
            }else {
              $data['imagen']['value'] = '';
            }

            if( $this->general->insert($data) ){
              $response['status'] = TRUE;
              $response['message'] = '';
              $response['data'] = $this->general->get(null,array('email'=>$data['email']['value']),"id as idusuario,nombre,apellidos,email,concat('http://codeandote.com/wa/dogit/profile/',imagen) as imagen,token");
              $this->set_response($response, REST_Controller::HTTP_OK);
            }else{
              $response['status'] = FALSE;
              $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
              $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }
          }
        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El email no es valido.';
          $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }
    }

/*
*
* No se ha podido enviar la imagen por este metodo para el registro de usuarios con imagen
*
    public function users_put(){
        $response = array('status' => FALSE, 'message'=>'Parametros incompletos');
        $data = array(
            'nombre' => array('value'=>$this->put('nombre')),
            'apellidos' => array('value'=>$this->put('apellidos')),
            'email' => array('value'=>$this->put('email')),
            'passwd' => array('value'=>$this->put('password')),
            'imagen' => array('value'=>$this->put('imagen')),
            'fecha' => array('value'=>date("Y-m-d H:i:s"))
        );

        if( $data['nombre']['value'] != "" && $data['apellidos']['value'] != '' && $data['passwd']['value'] != '' && $data['email']['value'] != '' ){
          if (filter_var($data['email']['value'], FILTER_VALIDATE_EMAIL)) {
            $search = $this->general->get(null,array('email'=>$data['email']['value']));
            if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
              $response['status'] = FALSE;
              $response['message'] = 'El correo ya esta resgistrado.';
              $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
            }else{
              $data['passwd']['value'] = md5($data['passwd']['value']);

              if( isset($_FILES['image']) && $_FILES['image']['error']==0 && $_FILES['image']['size']>0 ){
                $fileName   = $_FILES['image']['name'];
                $extension = end(explode(".", $fileName));
                $nombre_img = strtotime(date("Y-m-d H:i:s")).'.'.$extension;
                $moved = move_uploaded_file($_FILES["image"]["tmp_name"], "./profile/".$nombre_img );
                $data['imagen']['value'] = $nombre_img;
              }else {
                $data['imagen']['value'] = '';
              }

              if( $this->general->insert($data) ){
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $this->general->get(null,array('email'=>$data['email']['value']));
                $this->set_response($response, REST_Controller::HTTP_OK);
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }
            }
          }else {
            $response['status'] = FALSE;
            $response['message'] = 'El email no es valido.';
            $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }
        }else{
          $response['status'] = FALSE;
          $response['message'] = 'Parametros incompletos.';
          $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
*/

}
