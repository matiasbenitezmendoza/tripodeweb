<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Loginrepartidores extends REST_Controller {

    function __construct(){
      parent::__construct();
      $this->methods['login_post']['limit'] = 10000;
      $this->load->model("RepartidoresModel");
    }


    public function login_post() {
        $data = [
            'user' => strip_tags(trim($this->post('user'))),
            'pass' => md5(strip_tags(trim($this->post('pass')))),
            'version' => $this->post('version')
        ];

        if ( empty($data['user']) || empty($data['pass'])) {
            $response = [
                'status' => FALSE,
                'message' => 'INGRESA TU USUARIO Y CONTRASEÑA',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        }

        if ( empty($data['version']) ){
            $response = [
                'status' => FALSE,
                'message' => 'VERSION DE APLICACIÓN NO VALIDA',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        }

        $usuario = $this->RepartidoresModel->findOneWhere($data['user']);    
        if (empty($usuario)) {
            $response = [
                'status' => FALSE,
                'message' => 'USUARIO NO ENCONTRADO',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_NOT_FOUND);
        }

        if ($usuario->password != $data['pass']) {
            $response = [
                'status' => FALSE,
                'message' => 'CONTRASEÑA INCORRECTA',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $usuario->version = $data['version'];
        $usuario->password = '';

        $this->load->library('Authorization');
        $user_token = $this->authorization->generateToken($usuario);

        $response = [
            'status' => TRUE,
            'message' => 'ok',
            'data' => $user_token
        ];

        return $this->response($response, REST_Controller::HTTP_OK);
    }

}
