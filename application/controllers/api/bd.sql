-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `reg_usuarios`

CREATE TABLE `reg_usuarios`
(
 `id`               int NOT NULL AUTO_INCREMENT ,
 `nombre`           varchar(150) NOT NULL ,
 `apellido_paterno` varchar(100) NOT NULL ,
 `apellido_materno` varchar(100) NOT NULL ,
 `fecha_nacimiento` date NOT NULL ,
 `email`            varchar(100) NOT NULL ,
 `password`         varchar(255) NOT NULL ,
 `telefono`         varchar(45) NOT NULL ,
 `padecimientos`    longtext ,
 `fecha`            datetime NOT NULL ,
 `status`           int NOT NULL ,
PRIMARY KEY (`id`)
);



-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `tab_direcciones`

CREATE TABLE `tab_direcciones`
(
 `id`              int NOT NULL AUTO_INCREMENT ,
 `id_usuario`       int NOT NULL ,
 `nombre`          varchar(200) NOT NULL ,
 `calle`           text NOT NULL ,
 `numero_exterior` int NOT NULL ,
 `numero_interior` int ,
 `codigo_postal`   int NOT NULL ,
 `residencial`     text ,
 `estado`          varchar(60) NOT NULL ,
 `colonia`         varchar(250) NOT NULL ,
 `ciudad`          varchar(150) NOT NULL ,
 `referencia`      text NOT NULL ,
 `fecha`           datetime NOT NULL ,
 `status`          int NOT NULL ,
PRIMARY KEY (`id`),
KEY `fkIdx_73` (`id_usuario`),
CONSTRAINT `FK_73` FOREIGN KEY `fkIdx_73` (`id_usuario`) REFERENCES `reg_usuarios` (`id`)
);

-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `tab_facturacion`

CREATE TABLE `tab_facturacion`
(
 `id`              int NOT NULL AUTO_INCREMENT ,
 `id_usuario`      int NOT NULL ,
 `rfc`             varchar(45) NOT NULL ,
 `razon_social`    varchar(250) NOT NULL ,
 `calle`           text NOT NULL ,
 `numero_exterior` int NOT NULL ,
 `numero_interior` int ,
 `codigo_postal`   int NOT NULL ,
 `residencial`     text ,
 `estado`          varchar(60) NOT NULL ,
 `colonia`         varchar(250) NOT NULL ,
 `ciudad`          varchar(45) NOT NULL ,
 `referencia`      text NOT NULL ,
 `fecha`           datetime NOT NULL ,
 `status`          int NOT NULL ,
PRIMARY KEY (`id`),
KEY `fkIdx_87` (`id_usuario`),
CONSTRAINT `FK_87` FOREIGN KEY `fkIdx_87` (`id_usuario`) REFERENCES `reg_usuarios` (`id`)
);





-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `tab_suscripciones`

CREATE TABLE `tab_suscripciones`
(
 `id`             int NOT NULL AUTO_INCREMENT ,
 `id_usuario`     int NOT NULL ,
 `novedades`      int NOT NULL ,
 `notificaciones` int NOT NULL ,
 `temas`          longtext NOT NULL ,
 `fecha`          datetime NOT NULL ,
 `status`         int NOT NULL ,
PRIMARY KEY (`id`),
KEY `fkIdx_82` (`id_usuario`),
CONSTRAINT `FK_82` FOREIGN KEY `fkIdx_82` (`id_usuario`) REFERENCES `reg_usuarios` (`id`)
);





-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `reg_repartidores`

CREATE TABLE `reg_repartidores`
(
 `id`               int NOT NULL AUTO_INCREMENT ,
 `nombre`           varchar(150) NOT NULL ,
 `apellido_paterno` varchar(100) NOT NULL ,
 `apellido_materno` varchar(100) NOT NULL ,
 `imagen`           varchar(250) ,
 `email`            varchar(100) NOT NULL ,
 `telefono`            varchar(45) NOT NULL ,
 `fecha`            datetime NOT NULL ,
 `status`           int NOT NULL ,
PRIMARY KEY (`id`)
);

-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `tab_chats`

CREATE TABLE `tab_chats`
(
 `id`        int NOT NULL AUTO_INCREMENT ,
 `idusuario` int NOT NULL ,
 `mensaje`   longtext NOT NULL ,
 `respuesta` int NOT NULL ,
 `fecha`     datetime NOT NULL ,
 `status`    int NOT NULL ,
PRIMARY KEY (`id`)
);



-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `cat_padecimientos`

CREATE TABLE `cat_padecimientos`
(
 `id`          int NOT NULL AUTO_INCREMENT ,
 `nombre`      varchar(200) NOT NULL ,
 `descripcion` text NOT NULL ,
 `imagen`      varchar(250) ,
 `fecha`       datetime NOT NULL ,
 `status`      int NOT NULL ,
PRIMARY KEY (`id`)
);

-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `cat_temas`

CREATE TABLE `cat_temas`
(
 `id`          int NOT NULL AUTO_INCREMENT ,
 `nombre`      varchar(200) NOT NULL ,
 `descripcion` text NOT NULL ,
 `imagen`      varchar(250) ,
 `fecha`       datetime NOT NULL ,
 `status`      int NOT NULL ,
PRIMARY KEY (`id`)
);


-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;


-- ************************************** `tab_pedidos`

CREATE TABLE `tab_pedidos`
(
 `id`                      int NOT NULL AUTO_INCREMENT ,
 `id_usuario`              int NOT NULL ,
 `id_repartidor`           int NOT NULL ,
 `fecha_pedido`            datetime NOT NULL ,
 `tipo_envio`              varchar(45) NOT NULL ,
 `status_pedido`           varchar(45) NOT NULL ,
 `tipo_pago`               varchar(45) NOT NULL ,
 `status_pago`             varchar(45) NOT NULL ,
 `comentarios_usuario`     longtext ,
 `comentarios_repartidor`  longtext ,
 `calificacion_usuario`    int ,
 `calificacion_repartidor` int ,
 `calle`                   text ,
 `numero_exterior`         int ,
 `numero_interior`         int ,
 `residencial`             text ,
 `codigo_postal`           int ,
 `estado`                  varchar(60) ,
 `colonia`                 varchar(250) ,
 `ciudad`                  varchar(150) ,
 `referencia`              text ,
 `rfc`                     varchar(45) ,
 `razon_social`            varchar(250) ,
 `calle_fac`               text ,
 `numero_exterior_fac`     int ,
 `numero_interior_fac`     int ,
 `residencial_fac`         text ,
 `codigo_postal_fac`       int ,
 `estado_fac`              varchar(60) ,
 `colonia_fac`             varchar(250) ,
 `ciudad_fac`              varchar(150) ,
 `referencia_fac`          text ,
 `fecha`                   datetime NOT NULL ,
 `status`                  int NOT NULL ,
PRIMARY KEY (`id`),
KEY `fkIdx_186` (`id_usuario`),
CONSTRAINT `FK_186` FOREIGN KEY `fkIdx_186` (`id_usuario`) REFERENCES `reg_usuarios` (`id`),
KEY `fkIdx_189` (`id_repartidor`),
CONSTRAINT `FK_189` FOREIGN KEY `fkIdx_189` (`id_repartidor`) REFERENCES `reg_repartidores` (`id`)
);





