<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Login extends REST_Controller {

    function __construct(){
      parent::__construct();
      $this->methods['login_post']['limit'] = 10000;
      $this->load->model("UserModel");
    }
    
    public function login_post() {
        $user = $this->post('user');
        $pass = $this->post('pass');

        $data = [
            'user' => strip_tags(trim( $user )),
            'pass' => md5(strip_tags(trim( $pass ))),
            'version' => $this->post('version')
        ];

        if ( empty($data['user']) || empty($data['pass'])) {
            $response = [
                'status' => FALSE,
                'message' => 'INGRESA TU USUARIO Y CONTRASEÑA',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        }

        if ( empty($data['version']) ){
            $response = [
                'status' => FALSE,
                'message' => 'VERSION DE APLICACIÓN NO VALIDA',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        }

        $usuario = $this->UserModel->findOneWhere($data['user']);    
        if (empty($usuario)) {
            $response = [
                'status' => FALSE,
                'message' => 'USUARIO NO ENCONTRADO',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_NOT_FOUND);
        }

        /*
        if ($usuario->password != $data['pass']) {
            $response = [
                'status' => FALSE,
                'message' => 'CONTRASEÑA INCORRECTA',
                'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }*/

        $datos_login = $this->_login_ld_com($user, $pass);
        if(	isset($datos_login->IsSuccessful) ){
            if($datos_login->IsSuccessful != true || $datos_login->Data == null  || $datos_login->Data == "null"){
                    $response = [
                                'status' => FALSE,
                                'message' => $datos_login->Message,
                                'data' => null
                    ];
                    return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
            }
        }

        if( !isset($datos_login->Data) ){
            $response = [
                        'status' => FALSE,
                        'message' => $datos_login->Message,
                        'data' => null
            ];
            return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $usuario_ld_com = $datos_login->Data;
        /*if ($usuario_ld_com == false) {
            $response = [
                'status' => FALSE,
                'message' => 'CONTRASEÑA INCORRECTA',
                'data' => $usuario_ld_com
            ];
            return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
        }*/

        $direccion = $this->UserModel->direccion($usuario->id);

        if($usuario->id_ldcom == "desvinculado" || $usuario->id_ldcom == ""){
                $actualizar_usuario = $this->_actualizar_usuario_ld_com($usuario, $usuario_ld_com->Id);
                $direccion_ld_com = $this->_add_direcciones_ld_com($direccion, $usuario_ld_com->Id);
                $actualizar_id = $this->UserModel->update($usuario->id, $usuario_ld_com->Id);   
                if($actualizar_usuario !=false && $direccion_ld_com != false && $actualizar_id != false ){
                   
                }else{
                    $response = [
                        'status' => FALSE,
                        'message' => 'Ocurrio un problema, intenta iniciar sesion nuevamente',
                        'data' => null
                    ];
                   return $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
                }
        }
        
        $usuario->version = $data['version'];
        $usuario->password = '';
        $usuario->id_ldcom = '';
        $usuario->id_usuario_ldcom = $usuario_ld_com->Id;
        $usuario->fecha_activacion =  $usuario->fecha;

        $this->load->library('Authorization');
        $user_token = $this->authorization->generateToken($usuario);

        $response = [
            'status' => TRUE,
            'message' => 'ok',
            'data' => $user_token
        ];

        return $this->response($response, REST_Controller::HTTP_OK);
    }

    /*Funciones para LD COM*/
    private function _login_ld_com($email, $password){
            $servicio = "login?";
            $parametros = "connection=&password=".$password."&email=".$email;
            $url = $servicio.$parametros;
            return _curl_ldcom_r($url, "8085", "GET", null);
    }


    private function _add_direcciones_ld_com($d, $id){
        //addresses?connection=&PhoneNumber=&IsMainAddress=&PostalCode=&UserId=&References=&Street=&ExtNumber=&IntNumebr=&Name=&Level1=&Level2=&Level3=
        $servicio = "addresses?";
        $parametros =  "connection=".
                       "&PhoneNumber=".
                       "&IsMainAddress=true".
                       "&PostalCode=".urlencode($d->codigo_postal).
                       "&UserId=".urlencode($id).
                       "&References=".urlencode($d->residencial).
                       "&Street=".urlencode($d->calle).
                       "&ExtNumber=".urlencode($d->numero_exterior).
                       "&IntNumber=".urlencode($d->numero_interior).
                       "&Name=".urlencode($d->nombre).
                       "&Level1=".urlencode($d->estado).
                       "&Level2=".urlencode($d->ciudad).
                       "&Level3=".urlencode($d->colonia);
        $url = $servicio.$parametros;
        return _curl_ldcom($url, "8085", "POST", null);
    }

    private function _actualizar_usuario_ld_com($d, $id){
        //updatesocio?pId=44&pName=Juan e&pEmail=&pBirth=1979-06-29T00:00:00&pFilter1=2&pFilter2=2&pFilter3=2&pFilter4=3&pFilter5=4&pTelephone=6666666666
        //updatesocio?pId=44&pName=Ped   &pEmail=romedy10%40gmail.com&pBirth=1994-12-12&pFilter1=&pFilter2=&pFilter3=&pFilter4=&pFilter5=&pTelephone=19512414105"

        $servicio = "updatesocio?";
        $parametros =       "pId=".urlencode($id).
                            "&pName=".urlencode($d->nombre." ".$d->apellido_paterno." ".$d->apellido_materno).
                            "&pEmail=".urlencode($d->email).
                            "&pBirth=".urlencode($d->fecha_nacimiento).
                            "&pFilter1=0".
                            "&pFilter2=0".
                            "&pFilter3=0".
                            "&pFilter4=0".
                            "&pFilter5=0".
                            "&pTelephone=".urlencode($d->telefono);
        $url = $servicio.$parametros;
        return _curl_ldcom($url, "8085", "POST", null); 
    }


}
