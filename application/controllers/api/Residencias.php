<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Residencias extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['residencias_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "tab_residencias";
        $this->general->id = "id";
        $this->load->library('Authorization');

    }

    public function residencias_get(){
      
    
        $registros = $this->general->get(null,null,"id, nombre, latitud, longitud");
       
        if ($registros){
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $registros;
                return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
                return $this->response([
                    'status' => FALSE,
                    'message' => 'No registry were found',
                    'data' => []
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        
    }

    public function ubicacion_get(){

        $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data' => json_decode ("{}"));
        $response_token = $this->authorization->verificaToken();
    
        if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
          $usuario = $response_token['data'];
          $id = $usuario->id;
          $id = (int) $id;
          $residencial = $this->get('residencial');    
          $latlng =  $this->general->get(null, array('nombre'=>$residencial ),"latitud, longitud");
    
            if (!empty($latlng)){
                    return $this->response([
                             'status' => TRUE,
                             'message' => '',
                             'data' => $latlng[0]
                        ], REST_Controller::HTTP_OK); 
            }else{
                return $this->set_response([
                    'status' => TRUE,
                    'message' => 'No se encontro los datos de la residencial',
                    'data' =>  json_decode ("{}")
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
    
          }    
    
          if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
                  $response = [
                      'status' => FALSE,
                      'message' => $response_token['message'],
                      'data' =>  json_decode ("{}")
                  ]; 
                  return $this->response($response, $response_token['code']);
          }
          $response['status'] = FALSE;
          $response['message'] = 'ACCION NO PERMITIDA';
          $response['data'] =  json_decode ("{}");
          return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
    
}
