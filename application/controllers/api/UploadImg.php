<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class UploadImg extends REST_Controller {

    function __construct(){
      parent::__construct();
    }

    public function save_put() {
        $idevaluacion = $this->post('idevaluacion');
        $tipo = $this->post('tipo');

        $tipo_firma = "firma";
        $tipo_evaluacion = "evaluacion";
        $tipo_visita = "visita";

        $_RESPONSE = [
            'status' => FALSE,
            'message' => '',
            'data' => null,
        ];

        if (isset($_FILES['imagen']) && $_FILES['imagen']['error'] == UPLOAD_ERR_OK 
            && ($_FILES['imagen']['type'] == "image/*" || $_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/png" )) {        
                
                $imgType = ".jpg"; 

                if ($_FILES['imagen']['type'] == "image/png") {
                    $imgType = ".png"; 
                }
                
                $pleca = DIRECTORY_SEPARATOR;
                $baseDir = dirname(__FILE__);
                $imgDir = "farmbenavides/" . $tipo . "/";
                // NOMBRE DE IMAGEN 
                //$imgName = $_POST['cliente'] ."_". uniqid() . $imgType; 
                $imgName = $idevaluacion . '_' . $tipo . '_' . $uniqid() . $imgType;
                $respuesta_data['type'] = $_FILES['imagen']['type'];
                $respuesta_data['nameImage'] = $_FILES['imagen']['name'];
                
                $imageServerPath = $baseDir .	$pleca . $imgDir . $imgName; 			

                if (move_uploaded_file($_FILES['imagen']['tmp_name'], $imageServerPath)) {
                    $imageUrl = $imgDir . $imgName;
                    $respuesta_data['imagen'] = $imageUrl;
                } else {
                    //response error 500
                    $_RESPONSE['message'] = "ERROR AL MOVER EL ARCHIVO";
                    $this->set_response($_RESPONSE, REST_Controller::HTTP_BAD_REQUEST);
                }
                
                // update info img
                $this->set_response($_RESPONSE, REST_Controller::HTTP_OK);
        } else {
            // response imagen no valida 
            $_RESPONSE['message'] = "IMAGEN NO VALIDA";
            $this->set_response($_RESPONSE, REST_Controller::HTTP_BAD_REQUEST);

        }
    }

}