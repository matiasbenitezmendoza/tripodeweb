<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Facturacion extends REST_Controller {

  function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['facturacion_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['facturacion_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['facturacion_put']['limit'] = 5000; // 50 requests per hour per user/key
        $this->methods['facturacion_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "tab_facturacion";
        $this->general->id = "id";
        $this->load->library('Authorization');

  }


  public function facturacion_get(){

    $response_token = $this->authorization->verificaToken();
  
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id =  $usuario->id;
        $registros = $this->general->get(null,null,"*");

        if ($id === NULL){
            if ($registros){
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $registros;
                return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }else{
                return $this->response([
                    'status' => FALSE,
                    'message' => 'No registry were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        $id = (int) $id;
        if ($id <= 0){
                $response['status'] = FALSE;
                $response['message'] = '';
            // Invalid id, set the response and exit.
            return $this->response($response, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $registro =  $this->general->get(null,array('id_usuario'=>$id ), "*");

        if (!empty($registro)){
          $response['status'] = TRUE;
          $response['message'] = '';
          $response['data'] = $registro;
          return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
          return $this->set_response([
                'status' => FALSE,
                'message' => 'registry could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  }

  public function facturacion_post(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
      $usuario = $response_token['data'];
      $id = $usuario->id;
      $id = (int) $id;

      $cfdi = $this->post('cfdi');

      $data = array(
          'id_usuario' => array('value'=>$id),
          'rfc' => array('value'=>$this->post('rfc')),
          'razon_social' => array('value'=>$this->post('razon_social')),
          'calle' => array('value'=>$this->post('calle')),   
          'numero_exterior' => array('value'=>$this->post('numero_exterior')),
          'numero_interior' => array('value'=>$this->post('numero_interior')),
          'residencial' => array('value'=>$this->post('residencial')),
          'codigo_postal' => array('value'=>$this->post('codigo_postal')),
          'estado' => array('value'=>$this->post('estado')),
          'colonia' => array('value'=>$this->post('colonia')),
          'municipio' => array('value'=>$this->post('municipio')),
          'referencia' => array('value'=>$this->post('referencia')),
          'cfdi_name' => array('value'=>$this->post('cfdi_name')),
          'cfdi' => array('value'=>$cfdi),
          'email' => array('value'=>$usuario->email),
          'fecha' => array('value'=>date("Y-m-d H:i:s"))
      );


      $data2 = array(
        'cfdi' => array('value'=>$cfdi),
        'email' => array('value'=>$usuario->email),
        'pNivel1' => array('value'=>$this->post('pNivel1')),
        'pNivel2' => array('value'=>$this->post('pNivel2')),
        'pNivel3' => array('value'=>$this->post('pNivel3'))
      );

      if( $data['id_usuario']['value'] != "" && $data['rfc']['value'] != "" && $data['razon_social']['value'] != "" && $data['numero_exterior']['value'] != "" && $data['calle']['value'] != "" && $data['estado']['value'] != '' && $data['colonia']['value'] != '' && $data['codigo_postal']['value'] != '' && $data['municipio']['value'] != '' ){


                    $resp_ldcom = $this->_facturacion_add_ld_com($data,$data2);                      
                    if($resp_ldcom === false){
                                $response['status'] = FALSE;
                                $response['message'] = "No se pudo agregar sus datos, intentelo de nuevo.";
                                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }

                    $search = $this->general->get(null,array('rfc'=>$data['rfc']['value']));                    
                      
                    if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
                                  $update = $this->general->update($data, $search[0]->id);
                                  if($update){
                                            $busqueda = $this->general->get(null,array('id'=>$search[0]->id),"*");
                                            $response['status'] = TRUE;
                                            $response['message'] = '';
                                            $response['data'] = $busqueda[0];
                                            return $this->set_response($response, REST_Controller::HTTP_OK);
                                  }else{
                                            $response['status'] = FALSE;
                                            $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                  }


                    }else{
                                  $id_insertd = $this->general->insert($data);
                                  if($id_insertd){
                                              $busqueda = $this->general->get(null,array('id'=>$id_insertd),"*");
                                              $response['status'] = TRUE;
                                              $response['message'] = '';
                                              $response['data'] = $busqueda[0];
                                              return $this->set_response($response, REST_Controller::HTTP_OK);
                                  }else{
                                              $response['status'] = FALSE;
                                              $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                              return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                  }
                    }

      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }
    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


  }

  public function facturacion_put(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}") );
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

        $usuario = $response_token['data'];
        $cfdi = $this->put('cfdi');

        $data = array(
            'id' => array('value'=>$this->put('id')),
            'rfc' => array('value'=>$this->put('rfc')),
            'razon_social' => array('value'=>$this->put('razon_social')),
            'calle' => array('value'=>$this->put('calle')),
            'numero_exterior' => array('value'=>$this->put('numero_exterior')),
            'numero_interior' => array('value'=>$this->put('numero_interior')),
            'residencial' => array('value'=>$this->put('residencial')),
            'codigo_postal' => array('value'=>$this->put('codigo_postal')),
            'estado' => array('value'=>$this->put('estado')),
            'colonia' => array('value'=>$this->put('colonia')),
            'municipio' => array('value'=>$this->put('municipio')),
            'referencia' => array('value'=>$this->put('referencia')),
            'cfdi_name' => array('value'=>$this->put('cfdi_name')),
            'cfdi' => array('value'=>$cfdi),
            'email' => array('value'=>$usuario->email),
            'fecha' => array('value'=>date("Y-m-d H:i:s"))
        );

        $data2 = array(
          'cfdi' => array('value'=>$cfdi),
          'email' => array('value'=>$usuario->email),
          'pNivel1' => array('value'=>$this->put('pNivel1')),
          'pNivel2' => array('value'=>$this->put('pNivel2')),
          'pNivel3' => array('value'=>$this->put('pNivel3'))
        );

      
      if( $data['id']['value'] != "" && $data['rfc']['value'] != "" && $data['razon_social']['value'] != "" && $data['numero_exterior']['value'] != "" && $data['calle']['value'] != "" && $data['estado']['value'] != '' && $data['colonia']['value'] != '' && $data['codigo_postal']['value'] != '' && $data['municipio']['value'] != '' ){
        if (filter_var($data['id']['value'], FILTER_VALIDATE_INT)) {

          $search = $this->general->get(null,array('id'=>$data['id']['value']));
          if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
            

                        
                        $resp_ldcom = $this->_facturacion_add_ld_com($data,$data2); 
                        if($resp_ldcom === false){
                                    $response['status'] = FALSE;
                                    $response['message'] = "No se pudo agregar sus datos, intentelo de nuevo.";
                                    return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                        }

                        $search_rfc = $this->general->get(null,array('rfc'=>$data['rfc']['value'], 'id !='=>$data['id']['value'] ));
                        if( isset($search_rfc) && is_array($search_rfc) && count($search_rfc)>0 && isset($search_rfc[0]->id) ){
                                 $this->general->delete($search_rfc[0]->id);
                        }

                        $update = $this->general->update($data, $data["id"]["value"]);
                        if($update){
                                  $busqueda = $this->general->get(null,array('id'=>$data["id"]["value"]),"*");
                                  $response['status'] = TRUE;
                                  $response['message'] = '';
                                  $response['data'] = $busqueda[0];
                                  return $this->set_response($response, REST_Controller::HTTP_OK);
                        }else{
                                  $response['status'] = FALSE;
                                  $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                                  return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                        }

          }else{
            $response['status'] = FALSE;
            $response['message'] = 'El id no esta registrado';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }

    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
      $response = [
          'status' => FALSE,
          'message' => $response_token['message'],
          'data' => null
      ]; 
      return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA'; 
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  public function facturacion_delete(){
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}") );
    $id =  $this->delete('id');
    $response_token = $this->authorization->verificaToken();

    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

      if( $id != "" ){
        if (filter_var($id, FILTER_VALIDATE_INT)) {
           $search = $this->general->get(null,array('id'=>$id));
           if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){  
              $delete = $this->general->delete($id);
              if($delete){
                  $message = [
                    'status' => TRUE,
                    'id' => $id,
                    'message' => 'Deleted the resource'
                  ];
                   return $this->set_response($message, REST_Controller::HTTP_OK);
              }else{
                $response['status'] = FALSE;
                $response['message'] = 'No se puedo procesar el registro, inténtalo más tarde.';
                return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
              }
          }else{
            $response['status'] = FALSE;
            $response['message'] = 'No se encontro el registro';
            return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
          }

        }else {
          $response['status'] = FALSE;
          $response['message'] = 'El id no es valido.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
      }else{
        $response['status'] = FALSE;
        $response['message'] = 'Parametros incompletos.';
        return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
      }

    }    

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }


  private function _facturacion_add_ld_com($d, $d2){
    //billingdata?connection=&UserId=BEMM09039400C&Name=Juan perez lopez&CFDIId=1&PhoneNumber=13243132&CellPhoneNumber=1234321&Address=Francisco I madero
    //&PostalCode=12341&Email=eznayderjb@gmail.com&Street=Francisco I madero&ExtNumber=2&IntNumber=2&Level1Id=1&Level2Id=1&Level3Id=1
    //billingdata?connection=&UserId=ssss&Name=Pedro&CFDIId=1&PhoneNumber=&CellPhoneNumber=&Address=&
    //PostalCode=12341&Email=eznayderjb2@gmail.com&Street=Francisco I madero&ExtNumber=2&IntNumber=2&Level1Id=1&Level2Id=1&Level3Id=1
    $servicio = "billingdata?";
    $parametros =  "connection=".
                   "&UserId=".urlencode($d['rfc']['value']).
                   "&Name=".urlencode($d['razon_social']['value']).
                   "&CFDIId=".urlencode($d2['cfdi']['value']).
                   "&PhoneNumber=".
                   "&CellPhoneNumber=".
                   "&Address=".urlencode($d['residencial']['value']).
                   "&PostalCode=".urlencode($d['codigo_postal']['value']).
                   "&Email=".urlencode($d2['email']['value']).
                   "&Street=".urlencode($d['calle']['value']).
                   "&ExtNumber=".urlencode($d['numero_exterior']['value']).
                   "&IntNumber=".urlencode($d['numero_interior']['value']).
                   "&Level1Id=".urlencode($d2['pNivel1']['value']).
                   "&Level2Id=".urlencode($d2['pNivel2']['value']).
                   "&Level3Id=".urlencode($d2['pNivel3']['value']);
                   
    $url = $servicio.$parametros;
    return _curl_ldcom($url, "8085", "POST", null);
}
    
}
