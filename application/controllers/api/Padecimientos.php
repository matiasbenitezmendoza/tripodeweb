<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Padecimientos extends REST_Controller {

    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['padecimientos_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->load->model("general");
        $this->general->table = "cat_padecimientos";
        $this->general->id = "id";
        $this->load->library('Authorization');

    }

  public function padecimientos_get(){
      
      
        $registros = $this->general->get(null,null,"id, nombre, descripcion, concat ('https://www.farmaciastripode.com/admin/imgs/padecimientos/',imagen) as imagen");
       
        if ($registros){
                $response['status'] = TRUE;
                $response['message'] = '';
                $response['data'] = $registros;
                return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
                return $this->response([
                    'status' => FALSE,
                    'message' => 'No registry were found',
                    'data' => []
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        

  }

    
}
