<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Repartidores extends REST_Controller {

  function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['repartidores_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['repartidores_disponibles_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['repartidores_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['repartidores_put']['limit'] = 5000; // 50 requests per hour per user/key
        $this->methods['repartidores_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model("general");
        // $this->general->table = "registro_usuarios";
        $this->general->table = "reg_repartidores";
        $this->general->id = "id";
        $this->load->library('Authorization');

  }

  public function repartidores_get(){
        
      $id = $this->get('id');
      $response_token = $this->authorization->verificaToken();
    
      if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        
          if ($id === NULL){
                  return $this->response([
                      'status' => FALSE,
                      'message' => 'Id requerido',
                      'data' => json_decode ("{}")
                  ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
          }
  
          $id = (int) $id;
          if ($id <= 0){
              $response['status'] = FALSE;
              $response['message'] = '';
              $response['message'] = json_decode ("{}");
              return $this->response($response, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
          }
  
          $registro =  $this->general->get(null,array('id'=>$id ), "id, nombre, apellido_paterno, apellido_materno, email, concat ('https://www.farmaciastripode.com/admin/imgs/repartidores/',imagen) as imagen");
  
          if (!empty($registro)){
              $response['status'] = TRUE;
              $response['message'] = '';
              $response['data'] = $registro[0];
              return $this->set_response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
          }else{
            return $this->set_response([
                  'status' => FALSE,
                  'message' => 'registry could not be found',
                  'data' => json_decode ("{}")
              ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
          }
      }    
  
      if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
            $response = [
                'status' => FALSE,
                'message' => $response_token['message'],
                'data' => null
            ]; 
            return $this->response($response, $response_token['code']);
      }
      $response['status'] = FALSE;
      $response['message'] = 'ACCION NO PERMITIDA';
      return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
  
  }

  public function repartidores_disponibles_get(){
        
    $response_token = $this->authorization->verificaToken();
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {

        $registros = $this->general->get(null,array('status_rep !='=> 2 ),"id as id_repartidor, nombre, apellido_paterno, apellido_materno, email, concat ('https://www.farmaciastripode.com/admin/imgs/repartidores/',imagen) as imagen");

            if ($registros){
                 $response['status'] = TRUE;
                 $response['message'] = '';
                 $response['data'] = $registros;
                 return $this->response($response, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }else{
                return $this->response([
                       'status' => FALSE,
                       'message' => 'No registry were found'
                       ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
          $response = [
              'status' => FALSE,
              'message' => $response_token['message'],
              'data' => null
          ]; 
          return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);

  }

  public function token_firebase_get(){
    // Users from a data store e.g. database
    $response = array('status' => FALSE, 'message'=>'Parametros incompletos', 'data'=> json_decode ("{}"));

    $response_token = $this->authorization->verificaToken();
    if(!empty ($response_token['status']) && $response_token['status'] == TRUE && !empty($response_token['data'])) {
        $usuario = $response_token['data'];
        $id =  $usuario->id;

        $data = array(
          'id' => array('value'=>$id),
          'token_firebase' => array('value'=> $this->get('token'))
        );

        if( $data['id']['value'] != "" && $data['token_firebase']['value'] != ""  ){

                    //ver si existe el registro
                    $search = $this->general->get(null,array('id'=>$data['id']['value']));
                    if( isset($search) && is_array($search) && count($search)>0 && isset($search[0]->id) ){
                                    if( $this->general->update($data, $data['id']['value']) ){
                                                              $busqueda = $this->general->get(null,array('id'=>$data['id']['value']));
                                                              $response['status'] = TRUE;
                                                              $response['message'] = 'Se actualizaron sus datos con exito';
                                                              $response['data'] = $busqueda[0];
                                                              return $this->set_response($response, REST_Controller::HTTP_OK);
                                    }
                                    else{
                                        $response['status'] = FALSE;
                                        $response['message'] = 'No se puede procesar el registro, inténtalo más tarde.';
                                        return  $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                                    }
                    }
                    else{
                          $response['status'] = FALSE;
                          $response['message'] = 'No se encontro el registro';
                          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }

        }
        else{
          $response['status'] = FALSE;
          $response['message'] = 'Parametros incompletos.';
          return $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    if (!empty($response_token) && !empty($response_token['code']) && !empty($response_token['message'])) {
        $response = [
            'status' => FALSE,
            'message' => $response_token['message'],
            'data' => json_decode ("{}")
        ];
        return $this->response($response, $response_token['code']);
    }
    $response['status'] = FALSE;
    $response['message'] = 'ACCION NO PERMITIDA';
    $response['data'] = json_decode ("{}");
    return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);


}
  
}