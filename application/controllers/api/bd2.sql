

CREATE TABLE `cat_padecimientos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(250) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `cat_temas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `reg_repartidores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `imagen` varchar(250) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `status_rep` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `reg_usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `padecimientos` longtext,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tab_chats` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `mensaje` longtext NOT NULL,
  `respuesta` int(11) NOT NULL,
  `status_chat` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tab_direcciones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `calle` text NOT NULL,
  `numero_exterior` int(11) NOT NULL,
  `numero_interior` int(11) DEFAULT NULL,
  `codigo_postal` int(11) NOT NULL,
  `residencial` text,
  `estado` varchar(60) NOT NULL,
  `colonia` varchar(250) NOT NULL,
  `ciudad` varchar(150) NOT NULL,
  `referencia` text NOT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tab_facturacion` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `rfc` varchar(45) NOT NULL,
  `razon_social` varchar(250) NOT NULL,
  `calle` text NOT NULL,
  `numero_exterior` int(11) NOT NULL,
  `numero_interior` int(11) DEFAULT NULL,
  `codigo_postal` int(11) NOT NULL,
  `residencial` text,
  `estado` varchar(60) NOT NULL,
  `colonia` varchar(250) NOT NULL,
  `municipio` varchar(45) NOT NULL,
  `referencia` text,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tab_pedidos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_repartidor` int(11) NOT NULL,
  `orden` varchar(250) NOT NULL,
  `fecha_pedido` datetime NOT NULL,
  `status_pedido` varchar(45) NOT NULL,
  `comentarios_usuario` longtext,
  `comentarios_repartidor` longtext,
  `calificacion_usuario` int(11) DEFAULT NULL,
  `calificacion_repartidor` int(11) DEFAULT NULL,
  `tipo_pago` varchar(45) NOT NULL,
  `status_pago` varchar(45) NOT NULL,
  `calle` text,
  `numero_exterior` int(11) DEFAULT NULL,
  `numero_interior` int(11) DEFAULT NULL,
  `residencial` text,
  `codigo_postal` int(11) DEFAULT NULL,
  `estado` varchar(60) DEFAULT NULL,
  `colonia` varchar(250) DEFAULT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `referencia` text,
  `rfc` varchar(45) DEFAULT NULL,
  `razon_social` varchar(250) DEFAULT NULL,
  `calle_fac` text,
  `numero_exterior_fac` int(11) DEFAULT NULL,
  `numero_interior_fac` int(11) DEFAULT NULL,
  `codigo_postal_fac` int(11) DEFAULT NULL,
  `estado_fac` varchar(60) DEFAULT NULL,
  `colonia_fac` varchar(250) DEFAULT NULL,
  `municipio_fac` varchar(150) DEFAULT NULL,
  `tipo_envio` varchar(45) NOT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tab_suscripciones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `novedades` int(11) NOT NULL,
  `notificaciones` int(11) NOT NULL,
  `temas` longtext NOT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(500) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `tel_fijo` varchar(20) DEFAULT NULL,
  `tel_movil` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `users` (`id`, `email`, `first_name`, `last_name`, `imagen`, `password`, `type`, `fecha`, `status`, `tel_fijo`, `tel_movil`) VALUES
(1, 'matias@hotmail.com', 'Matias', 'BM', 'imagen_1550000551.jpeg', '090c36e3bb39377468363197afb3e91b', 'Administrador', '2019-02-12 19:02:47', 1, '532332', '43323431234');


ALTER TABLE `cat_padecimientos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `cat_temas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `reg_repartidores`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `reg_usuarios`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `tab_chats`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tab_direcciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_73` (`id_usuario`);

ALTER TABLE `tab_facturacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_87` (`id_usuario`);

ALTER TABLE `tab_pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_186` (`id_usuario`);

ALTER TABLE `tab_suscripciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkIdx_82` (`id_usuario`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `cat_padecimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `cat_temas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `reg_repartidores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `reg_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `tab_chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `tab_direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `tab_facturacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `tab_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `tab_suscripciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


ALTER TABLE `tab_direcciones`
  ADD CONSTRAINT `FK_73` FOREIGN KEY (`id_usuario`) REFERENCES `reg_usuarios` (`id`);

ALTER TABLE `tab_facturacion`
  ADD CONSTRAINT `FK_87` FOREIGN KEY (`id_usuario`) REFERENCES `reg_usuarios` (`id`);

ALTER TABLE `tab_pedidos`
  ADD CONSTRAINT `FK_186` FOREIGN KEY (`id_usuario`) REFERENCES `reg_usuarios` (`id`);

ALTER TABLE `tab_suscripciones`
  ADD CONSTRAINT `FK_82` FOREIGN KEY (`id_usuario`) REFERENCES `reg_usuarios` (`id`);