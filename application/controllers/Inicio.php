<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	var $data = array('page'=>'contactos');
	var $fields = array();
	var $fields_user = array(
					'id' => array('value'=>'','validate' => array(
								                     'label'   => 'id',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'first_name' => array('value'=>'','validate' => array(
								                     'label'   => 'first_name',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'last_name' => array('value'=>'','validate' => array(
								                     'label'   => 'last_name',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'email' => array('value'=>'','validate' => array(
								                     'label'   => 'email',
								                     'rules'   => 'trim|required|xss_clean|validate_email'
								                  )),
					'password' => array('value'=>'','validate' => array(
								                     'label'   => 'password',
								                     'rules'   => 'trim|required|xss_clean'
								                  ))
				);
	var $fields_address = array(
					'idusuario' => array('value'=>'','validate' => array(
								                     'label'   => 'idusuario',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'iddireccion' => array('value'=>'','validate' => array(
								                     'label'   => 'iddireccion',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'calle' => array('value'=>'','validate' => array(
								                     'label'   => 'calle',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'exterior' => array('value'=>'','validate' => array(
								                     'label'   => 'exterior',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'interior' => array('value'=>'','validate' => array(
								                     'label'   => 'interior',
								                     'rules'   => 'trim|xss_clean|validate_email'
								                  )),
					'colonia' => array('value'=>'','validate' => array(
								                     'label'   => 'colonia',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'ciudad' => array('value'=>'','validate' => array(
								                     'label'   => 'ciudad',
								                     'rules'   => 'trim|required|xss_clean'
								                  )),
					'estado' => array('value'=>'','validate' => array(
								                     'label'   => 'estado',
								                     'rules'   => 'trim|xss_clean'
								                  )),
					'telefono' => array('value'=>'','validate' => array(
								                     'label'   => 'telefono',
								                     'rules'   => 'trim|xss_clean|validate_email'
								                  )),
					'codigo_postal' => array('value'=>'','validate' => array(
								                     'label'   => 'codigo_postal',
								                     'rules'   => 'trim|xss_clean|validate_email'
								                  )),
					'fecha' => array('value'=>'','validate' => array(
								                     'label'   => 'fecha',
								                     'rules'   => 'trim|xss_clean|validate_email'
								                  ))
				);

	function __construct(){
      parent::__construct();
			$user_data = $this->facebook->getMe();
			if(isset($user_data)){
				$this->data['img_profile'] = $this->facebook->getPicture();
				$this->data['user_data'] = $user_data;
				$this->data['id_mp_user'] = $this->mercadopagofunctions->get_user_mp($user_data['email']);
				$this->general->table = "users";
				$this->general->id = "email";
				$this->data['users'] = $this->general->get($user_data['email']);
			}
			$this->data['accessToken'] = $this->mercadopagofunctions->return_access_token();
			$this->data['publicKey'] = $this->mercadopagofunctions->return_public_key();

  }

	public function index(){
		$this->data['page'] = 'inicio';
		$this->template->content->view('inicio', $this->data);
		$this->template->publish('template_home');
	}

	public function aviso(){
		$this->data['page'] = 'aviso';
		$this->template->content->view('aviso', $this->data);
		$this->template->publish('template_site');
	}

	public function terminos(){
		$this->data['page'] = 'terminos';
		$this->template->content->view('terminos', $this->data);
		$this->template->publish('template_site');
	}

	public function log_out(){
		$this->session->unset_userdata('user_data');
		$this->session->unset_userdata('fb_img');
		$this->session->unset_userdata('code');
		$this->session->unset_userdata('state');
		$this->session->unset_userdata('items_cart');
		$this->data['user_data'] = "";
		redirect('/');
	}

	public function store($order = "desc"){
		$this->general->table = "tab_productos";
		$this->general->id = "idproducto";
		$this->data['page'] = 'store';
		$this->general->per_page = "9";
		//$this->data['productos'] = $this->general->get(null,null,"*","",true,$order);
		$this->data['productos'] = $this->general->getPerPage(null,null);

		$this->general->table = "tab_categorias";
		$this->general->id = "idcategoria";
		$this->data['categorias'] = $this->general->get(null,null,"*","",true,$order);

		$this->template->content->view('store', $this->data);
		$this->template->publish('template_site');
	}

	public function user_info(){
		$user_data = $this->facebook->getMe();
		if($user_data != ""){
			$id_mp_user = $this->mercadopagofunctions->get_user_mp($user_data['email']);

			$this->general->table = "users";
			$this->general->id = "email";
			$users = $this->general->get($user_data['email']);

			$this->general->table = "tab_direcciones";
			$this->general->id = "idusuario";
			$this->data['addresses'] = $this->general->get($users[0]->id);

			$this->general->table = "tab_compras";
			$this->general->id = "comprador";
			$this->data['purchases'] = $this->general->getCompra(null,array('comprador'=>$users[0]->id));

			$this->mercadopagofunctions->get_cards_info($user_data['email'],$id_mp_user);
			$this->template->content->view('informacion_usuario', $this->data);
			$this->template->publish('template_site');
		}else{
			redirect("/");
		}

	}

	public function insert_address(){
		$this->fields = $this->fields_address;
		$this->general->table = "tab_direcciones";
		$this->config_validates();
		//echo $this->fields['idusuario']['value'];
		$this->general->insert($this->fields);

		redirect('/inicio/user_info');
	}

	public function edit_addres(){
		$this->fields = $this->fields_address;
		$this->general->table = "tab_direcciones";
		$this->general->id = "iddireccion";
		$this->config_validates();
		$id = $this->fields['iddireccion']['value'];
		$this->general->update($this->fields,$id);

		redirect('/inicio/user_info');
	}

	public function delete_address(){
		$this->general->table = "tab_direcciones";
		$this->general->id = "iddireccion";
		$id_address = $_POST["id_address"];
		$res = $this->general->delete($id_address);
	}


	public function delete_card_user(){
		$id_card = $_POST["id_card"];
		$id_mp_user = $_POST["id_mp_user"];
		$res = $this->mercadopagofunctions->delete_card($id_mp_user,$id_card);
		$res = json_encode($res);
	}

	public function add_card_user(){
		$id_card = $_POST["id_card"];
		$id_mp_user = $_POST["id_mp_user"];
		$res = $this->mercadopagofunctions->add_tokencard_user($id_card,$id_mp_user);
		$res = json_encode($res);
	}

	function config_validates(){
		$config = array();

		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}

		$this->form_validation->set_rules($config);

		foreach( $this->fields as $key=>$field )
				$this->fields[$key]['value'] = $this->input->post($key,true);
	}

	public function pay(){
		$this->template->content->view('pago', $this->data);
		$this->template->publish('template_site');
	}

}
