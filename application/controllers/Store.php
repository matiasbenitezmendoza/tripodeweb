<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	var $data = array('page'=>'store');
	var $filtro = array();

	function __construct(){
      parent::__construct();
			$this->ci = &get_instance();
			$this->general->table = "tab_productos";
			$this->general->id = "idproducto";
			$this->general->per_page = "9";

			$user_data = $this->facebook->getMe();
			if(isset($user_data)){
				$this->data['img_profile'] = $this->facebook->getPicture();
				$this->data['user_data'] = $user_data;
				$this->data['id_mp_user'] = $this->mercadopagofunctions->get_user_mp($user_data['email']);
			}

  }

	public function index(){
		$pagina = "1";
		$this->data['productos'] = $this->general->getPerPage($pagina,$this->filtro);

		$this->general->table = "tab_categorias";
		$this->general->id = "idcategoria";
		$this->data['categorias'] = $this->general->get(null,null,"*","",true,"");
		$this->data['total_paginas'] = $this->general->getTotalPages($this->filtro);
		$this->data['pagina_actual'] = $pagina;

		$user_data = $this->facebook->getMe();
		if ($user_data != "") {
			$id_mp_user = $this->mercadopagofunctions->get_user_mp($user_data['email']);
			if ($id_mp_user == "No existe el usuario") {
				$id_mp_user = $this->mercadopagofunctions->create_user_mp($user_data['email'],$user_data['first_name'],$user_data['last_name']);
				$this->insert_user($user_data,$id_mp_user);
			}else{
				$this->insert_user($user_data,$id_mp_user);
			}
		}else{

		}
		$this->template->content->view('store', $this->data);
		$this->template->publish('template_site');

	}

	public function pagina( $pagina = 1 ){
		//$this->filtro['categorias'] = "Fashion";
		$this->data['productos'] = $this->general->getPerPage($pagina,$this->filtro);
		$this->data['total_paginas'] = $this->general->getTotalPages($this->filtro);
		$this->data['pagina_actual'] = $pagina;

		$this->general->table = "tab_categorias";
		$this->general->id = "idcategoria";
		$this->data['categorias'] = $this->general->get(null,null,"*","",true,"");

		$this->template->content->view('store', $this->data);
		$this->template->publish('template_site');
	}

	public function search($order ="", $type_order="", $pagina = 1 ){
		$search = $this->input->post('f_search');
		$category = $this->input->post('categorias');
		$this->filtro['nombre'] = $search;
		//$this->filtro['categorias'] = $search;
		//$this->filtro['categorias'] =$category[0];
		//$this->filtro['descripcion_corta'] = $search;
		$this->data['productos'] = $this->general->getPerPage($pagina,$this->filtro,$type_order,$order);
		//$this->data['productos'] = getTalentos($page,null,"precio desc",$envio="not_free",$category,$search);
		$this->data['total_paginas'] = $this->general->getTotalPages($this->filtro);
		$this->data['pagina_actual'] = $pagina;

		$this->general->table = "tab_categorias";
		$this->general->id = "idcategoria";
		$this->data['categorias'] = $this->general->get(null,null,"*","",true,"");

		$this->template->content->view('store', $this->data);
		$this->template->publish('template_site');
	}

	public function product_detail($id = null){
		$this->data['productos'] = $this->general->getProducts($id,null);

		$this->template->content->view('product_detail', $this->data);
		$this->template->publish('template_site');
	}

	public function insert_user($user_data, $id_mp_user){
		$this->general->table = "users";
		$this->general->id = "email";
		$users = $this->general->get($user_data['email']);
		if( !isset($users[0]->email) ){
			$data = array(
				'id' => array('value'=>''),
				'email' => array('value'=>$user_data['email']),
				'first_name' => array('value'=>$user_data['first_name']),
				'last_name' => array('value'=>$user_data['last_name']),
				'imagen' => array('value'=>''),
				'password' => array('value'=>md5($user_data['id'])),
				'type' => array('value'=>'User'),
				'fecha' => array('value'=>''),
				'status' => array('value'=>'1'),
				'tel_fijo' => array('value'=>''),
				'tel_movil' => array('value'=>''),
				'talentos' => array('value'=>'1'),
				'id_mp' => array('value'=>$id_mp_user),
				'cards' => array('value'=>''),
				'direcciones' => array('value'=>'')
			);
			$this->general->insert($data);
		}else{
			$this->general->update(array('id_mp'=>array('value'=>$id_mp_user)),$user_data['email']);
		}
	}

	public function checkout($id = null){
		$this->data['publicKey'] = $this->mercadopagofunctions->return_public_key();
		$store = $this->session->userdata('items_cart');
		$this->data['store'] = $store;
		$ids = "";
		if (isset($store['item'])) {
			foreach ($store['item'] as $key => $value) {
				if ($value > 0) {
					$ids .= $key.",";
				}
			}
		}
		$this->data['productos'] = $this->general->getProducts($ids.'0',null,false);

		$this->general->table = "users";
		$this->general->id = "email";
		$users = $this->general->get($this->session->userdata('email'));

		$this->general->table = "tab_direcciones";
		$this->general->id = "idusuario";
		$this->data['addresses'] = $this->general->get($users[0]->id);

		$this->data['cards_info'] = $this->mercadopagofunctions->get_cards_info($this->session->userdata('email'),$this->session->userdata('id_mp'));

		$this->template->content->view('checkout', $this->data);
		$this->template->publish('template_site');
	}

	public function payment($id_dir = "",$total = ""){
	 $store = $this->session->userdata('items_cart');
	 $this->data['store'] = $store;
	 $this->general->table = "tab_productos";
	 $ids = "";
	 if (isset($store['item'])) {
		 foreach ($store['item'] as $key => $value) {
			 if ($value > 0) {
				 $ids .= $key.",";
			 }
		 }
	 }
	 $productos = $this->general->getProducts($ids.'0',null,false);

	 $this->general->table = "tab_direcciones";
	 $this->general->id = "iddireccion";
	 $direcciones = $this->general->get($id_dir);

	 $detalle = $this->mercadopagofunctions->pay_payment($direcciones,$productos,$store,$total);
	 if( $detalle->status == "approved" ){
		$direccion =  $direcciones[0]->calle." ".$direcciones[0]->exterior." ".$direcciones[0]->interior." ".$direcciones[0]->colonia." ".$direcciones[0]->ciudad." ".$direcciones[0]->estado." ".$direcciones[0]->codigo_postal;
		$this->general->table = "tab_compras";
		 foreach($productos as $key => $producto){
			 $cantidad = $store['item'][$producto->idproducto];
			 $costo = ($producto->precio * $cantidad)+$producto->envio;
			 $data = array(
				 'comprador' => array('value'=>$direcciones[0]->idusuario),
				 'producto' => array('value'=>$producto->idproducto),
				 'direccion' => array('value'=>$direccion),
				 'envio' => array('value'=>$producto->envio),
				 'precio' => array('value'=>$producto->precio),
				 'cantidad' => array('value'=>$cantidad),
				 'costo' => array('value'=>$costo),
				 'estatus' => array('value'=>'pendiente de envio'),
				 'fecha' => array('value'=>''),
				 'status' => array('value'=>'1'),
				 'detalle' => array('value'=>$detalle->toJson()),
				 'pago' => array('value'=>'0')
			 );
			 $this->shopcart->delete_item_cart($producto->idproducto);
			 $this->general->insert($data);
			 $this->data['respuesta'] = "Pagado";
		 }
	 }else{
		 echo "not approved";
		 echo $detalle->status;
	 }

	 $this->template->content->view('payment', $this->data);
	 $this->template->publish('template_site');
 }

	public function add_cart(){
		$id_item = $_POST["id_item"];
		$quantity = $_POST["quantity"];
		$this->shopcart->add_item_cart($id_item, $quantity);
	}

	public function delete_cart(){
		$id_item = $_POST["id_item"];
		$this->shopcart->delete_item_cart($id_item);
	}

	public function get_total_price(){
		$productos = $_POST["$productos"];
		$store = $_POST["$store"];
		$subtotal =  $this->shopcart->get_total($productos,$store);
		echo $subtotal;
	}

	public function total_items_cart(){
		echo	$this->shopcart->total_item();
	}

}
