var maps = (function(){

var marker;
var map;
var estilo = [
    {
        "stylers": [
            {
                "hue": "#007fff"
            },
            {
                "saturation": 89
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
             ];

var _initMap = function(){
  var fieldlat = document.getElementById("latitud").value;
  var fieldlong =document.getElementById("longitud").value;
  var Latlng = new google.maps.LatLng(19.4335908, -99.1324941);
  
  if(fieldlat !== "" && fieldlong !== ""){
       Latlng = new google.maps.LatLng(fieldlat, fieldlong);
  }

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    styles: estilo,
    center: Latlng
  });

  marker = new google.maps.Marker({
    draggable: true,
    position: Latlng,
    map: map,
    title: "Arrastra el Mouse en la ubicacion de la nueva Sucursal o da dobleclick en la ubicacion" 
  });

  google.maps.event.addListener(marker, 'dragend', function (event) {
       document.getElementById("latitud").value = event.latLng.lat();
       document.getElementById("longitud").value = event.latLng.lng();
  });


  map.addListener('dblclick', function(e) {
               marker.setMap(null);
               var nuevaLatlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
               marker = new google.maps.Marker({
                   draggable: true,
                   position: nuevaLatlng,
                   map: map,
                   title: "Arrastra el Mouse en la ubicacion de la nueva Sucursal o da dobleclick en la ubicacion"
              });
              document.getElementById("latitud").value = e.latLng.lat();
              document.getElementById("longitud").value = e.latLng.lng();
              
              google.maps.event.addListener(marker, 'dragend', function (event) {
                        document.getElementById("latitud").value = event.latLng.lat();
                        document.getElementById("longitud").value = event.latLng.lng();
              });
  });

};

return{
            "initMap" : _initMap
};
                
})();
 
